// images
import blogImg1 from "../images/facilities/bg-1.jpg";
import blogImg2 from "../images/facilities/bg-2.jpg";
import blogImg3 from "../images/facilities/bg-3.jpg";

import blogSingleImg1 from "../images/blog/img-4.jpg";
import blogSingleImg2 from "../images/blog/img-5.jpg";
import blogSingleImg3 from "../images/blog/img-6.jpg";

const facilities = [
  {
    id: "1",
    title: "Jasa Layanan Service Kendaraan",
    screens: blogImg1,
    description:
      "Tanpa perlu kerepotan mempersiapkan berbagai perlengkapan alat dapur. Kami sudah memfasilitasi dapur Anda...",
    author: "Loura Sweety",
    create_at: "25 Sep 2022",
    blogSingleImg: blogSingleImg1,
    comment: "35",
    blClass: "format-standard-image",
  },
  {
    id: "2",
    title: "Sewa Alat Bengkel",
    screens: blogImg2,
    description:
      "Brand Anda akan kami daftarkan di seluruh Food Delivery Channel, seperti Go Food, Grab Food, Traveloka Eats, ...",
    author: "David Luis",
    create_at: "23 Sep 2022",
    blogSingleImg: blogSingleImg2,
    comment: "80",
    blClass: "format-standard-image",
  },
  {
    id: "3",
    title: "Jual Sparepart Kendaraan",
    screens: blogImg3,
    description:
      "Satu dapur dapat digunakan untuk beberapa brand Anda. Kebersihan akan selalu terjamin di area dapur Anda...",
    author: "Jenefer Willy",
    create_at: "21 Sep 2022",
    blogSingleImg: blogSingleImg3,
    comment: "95",
    blClass: "format-video",
  },
];
export default facilities;
