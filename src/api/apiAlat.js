import axios from "axios";

const apiUrl = "https://stag-msib-01.bisa.ai:8080/backend_cloud_workshop";

export const getAlat = async () => {
  try {
    const response = await axios.get(`${apiUrl}/alat/get_alat`);
    return response.data.data;
  } catch (error) {
    console.error(error);
  }
};
