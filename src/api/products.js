const products = [
  {
    id_lokasi: "1",
    subUrl: "bengkel",
    title: "Sewa Bengkel",
  },
  {
    id_lokasi: "2",
    subUrl: "alat",
    title: "Sewa Alat",
  },
  {
    id_lokasi: "3",
    subUrl: "service",
    title: "Service Kendaraan",
  },
  {
    id_lokasi: "4",
    subUrl: "sparepart",
    title: "Sparepart Kendaraan",
  },
];

export default products;
