const posProducts = [
  {
    deskripsi_produk: "Lele goreng + Sambal",
    foto_produk: "2022-10-25_203147_80564_foto_produk.png",
    harga: 13000,
    id_brand: 2,
    id_produk: 5,
    is_delete: 0,
    nama_produk: "Pecel Lele",
  },
  {
    deskripsi_produk: "Ayam goreng + Sambal",
    foto_produk: "2022-10-25_203147_80564_foto_produk.png",
    harga: 13000,
    id_brand: 2,
    id_produk: 4,
    is_delete: 0,
    nama_produk: "Pecel Ayam",
  },
  {
    deskripsi_produk: "makanan",
    foto_produk: "2022-10-25_203147_80564_foto_produk.png",
    harga: 20000,
    id_brand: 1,
    id_produk: 3,
    is_delete: 0,
    nama_produk: "ayam geprek B",
  },
  {
    deskripsi_produk: "Teh Manis",
    foto_produk: "2022-10-25_203147_80564_foto_produk.png",
    harga: 3000,
    id_brand: 1,
    id_produk: 2,
    is_delete: 0,
    nama_produk: "Teh Manis",
  },
  {
    deskripsi_produk: "makanan",
    foto_produk: "2022-10-25_203147_80564_foto_produk.png",
    harga: 10000,
    id_brand: 1,
    id_produk: 1,
    is_delete: 0,
    nama_produk: "ayam geprek",
  },
];

export default posProducts;
