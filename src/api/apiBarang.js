import axios from "axios";

const apiUrl = "https://stag-msib-01.bisa.ai:8080/backend_cloud_workshop";

export const getBarang = async () => {
  try {
    const response = await axios.get(`${apiUrl}/barang/get_barang`);
    return response.data.data;
  } catch (error) {
    console.error(error);
  }
};
