import simg1 from "../images/service/img-1.jpg";
import simg2 from "../images/service/img-2.jpg";
import simg3 from "../images/service/img-3.jpg";
import simg4 from "../images/service/img-4.jpg";
import simg5 from "../images/service/img-5.jpg";
import simg6 from "../images/service/img-6.jpg";
import simg7 from "../images/service/img-7.jpg";

import singleImg1 from "../images/service-single/1.jpg";
import singleImg2 from "../images/service-single/2.jpg";
import singleImg3 from "../images/service-single/3.jpg";
import singleImg4 from "../images/service-single/4.jpg";
import singleImg5 from "../images/service-single/5.jpg";
import singleImg6 from "../images/service-single/6.jpg";
import singleImg7 from "../images/service-single/7.jpg";

const Services = [
  {
    id: "1",
    fIcon: "ti-location-pin",
    title: "Lokasi Strategis",
    description:
      "Lokasi yang ramai penduduk dekat dengan jalan raya dan mudah diakses.",
    simg1: simg1,
    ssImg: singleImg1,
  },
  {
    id: "2",
    fIcon: "flaticon-food-tray",
    title: "Pemesanan lebih mudah dan cepat",
    description:
      "Kelola pesanan sesuai order dari seluruh channel penjualan hanya lewat satu aplikasi. Anda juga dapat melihat seluruh transaksi dari periode terpilih.",
    simg1: simg1,
    ssImg: singleImg1,
  },
  {
    id: "3",
    fIcon: "flaticon-expand-arrows",
    title: "Harga Kompetitif",
    description:
      "Buka cabang baru di lokasi strategis dan jangkau market lebih luas dengan investasi minimal.",
    simg1: simg1,
    ssImg: singleImg1,
  },
  {
    id: "4",
    fIcon: "ti-money",
    title: "Sistem door to door service",
    description:
      "Tanpa biaya sewa tempat, air, listrik dan lingkungan anda dapat menjalankan bisnis kuliner dengan skema bagi hasil dengan bisa kitchen.",
    simg1: simg1,
    ssImg: singleImg1,
  },
  {
    id: "5",
    fIcon: "ti-medall-alt",
    title: "Dikelola secara professional",
    description:
      "Dapur bersama dapat membantu anda berjualan dan mengelola bisnis UMKM secara professional.",
    simg1: simg1,
    ssImg: singleImg1,
  },
  {
    id: "6",
    fIcon: "ti-world",
    title: "Menjadi sahabat dan pilihan pertama untuk Anda",
    description:
      "Dapur bersama membantu anda mendapatkan networking dari berbagai brand dan pesanan bisa kitchen.",
    simg1: simg1,
    ssImg: singleImg1,
  },
];
export default Services;
