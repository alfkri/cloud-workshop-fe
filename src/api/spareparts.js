const spareparts = [
  {
    id: 1,
    title: "Baut",
    harga: 100000,
    rating: 4.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 2,
    title: "Gear",
    harga: 50000,
    rating: 3,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 3,
    title: "CVT",
    harga: 1000000,
    rating: 4.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 4,
    title: "CVT",
    harga: 1000000,
    rating: 3.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
];

export default spareparts;
