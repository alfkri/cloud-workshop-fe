const service = [
  {
    id: 1,
    title: "Paket Service Karburator",
    harga: 1000000,
    rating: 2.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 2,
    title: "Paket Service CVT",
    harga: 5000000,
    rating: 5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 3,
    title: "Paket Service Lengkap",
    harga: 500000,
    rating: 4.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
];

export default service;
