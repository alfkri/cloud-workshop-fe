const alat = [
  {
    id: 1,
    title: "Dongkrak",
    lokasi: "Depok",
    harga: 1000000,
    rating: 2.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 2,
    title: "Pompa",
    lokasi: "Jakarta",
    harga: 5000000,
    rating: 5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 3,
    title: "Compressor",
    lokasi: "Jakarta Selatan",
    harga: 500000,
    rating: 3.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
  {
    id: 4,
    title: "Forklift",
    lokasi: "Tangerang",
    harga: 700000,
    rating: 4.5,
    imgUrl:
      "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png",
  },
];

export default alat;
