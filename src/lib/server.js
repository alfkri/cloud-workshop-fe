const SERVER_URL = "https://gate.bisa.kitchen:8080/bisa_kitchen_prod";

const SERVER_URL_DEV = "https://gate.bisa.kitchen:8080/bisa_kitchen_dev";

// MEDIA
const POS_MEDIA_URL = `${SERVER_URL}/pos/media`;
const POS_MEDIA_DEV_URL = `${SERVER_URL_DEV}/pos/media`;
// Bisa ditambahin KDS_MEDIA_URL kalau perlu
const TRANSAKSI_MEDIA_URL = `${SERVER_URL_DEV}/transaksi/media/logo_metode_pembayaran`;

export { SERVER_URL, POS_MEDIA_URL, POS_MEDIA_DEV_URL, TRANSAKSI_MEDIA_URL };
