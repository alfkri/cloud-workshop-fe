export default function authHeader() {
  const dataUser = localStorage.getItem("kitct");
  let dataToken;
  let authToken;
  if (dataUser) {
    dataToken = JSON.parse(localStorage.getItem("kitct"));
  }
  const token = dataToken?.token;
  if (token) {
    authToken = `JWT ${token}`;
    return {
      Authorization: authToken,
      "Access-Control-Allow-Origin": "*",
    };
  }
  return {};
}
