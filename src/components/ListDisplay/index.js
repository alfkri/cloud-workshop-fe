import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { Link, useNavigate, useParams } from "react-router-dom";
import { getAllDisplay } from "../../store/actions/action";
import { client } from "../../lib/client";
import { logout } from "../../store/actions/action";

const ListDisplay = ({ display, auth, logout }) => {
  // // const { id_display } = useParams();
  const push = useNavigate();
  const [isLoading, setIsLoading] = useState(true);
  const [listData, setListData] = useState([]);

  // const dispatch = useDispatch();
  // const { display } = useSelector((state) => state.display);

  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  async function getListDisplay() {
    const response = await client.get("/pos/get_display");
    if (response.status === 200) {
      if (response.status === 200) {
        setListData(response.data?.data);
      } else {
        toast.error(response.error);
      }
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getListDisplay();
  }, []);

  if (isLoading) return "Loading..";

  return (
    <div className={`wpo-destination-area`}>
      <div className="container mt-5">
        <div className="destination-wrap">
          <div className="row">
            {listData?.map(item => (
              <div className="col-lg-4 col-md-6 col-12 " key={item.id_display}>
                <Link
                  onClick={ClickHandler}
                  to={`/kds-system/menu-display/${item.id_display}/${item.nama_display}`}
                >
                  <div className="destination-item shadow">
                    {/* <div className="destination-img">
                    <img src={destination.dimg1} alt="" />
                  </div> */}
                    <div className="destination-content">
                      {/* <span className="sub">{item.id_display}</span> */}
                      <h2 className="text-danger">{item.nama_display}</h2>
                      <div className="destination-bottom">
                        {item.tipe_display === 1 ? (
                          <p>Display Umum</p>
                        ) : (
                          <p>Display Khusus</p>
                        )}
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ListDisplay;
