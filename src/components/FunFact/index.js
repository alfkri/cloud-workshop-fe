import React from "react";

const funFact = [
  {
    title: "Harga Terjangkau",
    subTitle:
      "Anda dapat mengjangkau harga yang disediakan dan standar harga yang sudah ditentukan",
  },
  {
    title: "Garansi",
    subTitle:
      "Anda akan mendapatkan tawaran garansi per tahun dengan bergabung menjadi komunitas pelanggan Cloud Bengkel",
  },
  {
    title: "Pembayaran Sistem Cepat",
    subTitle:
      "Cloud Bengkel dilengkapi dan didukung oleh kecanggihan teknologi yang dapat membantu masyarakat membayar pesanan di manapun & kapanpun",
  },
];

const FunFact = props => {
  return (
    <section className={`wpo-fun-fact-section ${props.fClass}`}>
      <div className="container-fluid">
        <div className="row">
          <div className="col col-xs-12">
            <h1>
              Kenapa Harus <span>Cloud Bengkel</span>?
            </h1>
            <div className="wpo-fun-fact-grids clearfix mt-5">
              {funFact.map((funfact, fitem) => (
                <div className="grid" key={fitem}>
                  <div className="info">
                    <h3>{funfact.title}</h3>
                    <p>{funfact.subTitle}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FunFact;
