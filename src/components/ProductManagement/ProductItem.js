import { Box, Button, Grid, Typography } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { client } from "../../lib/client";
import { POS_MEDIA_URL } from "../../lib/server";
import SimpleDeleteModal from "../Reusable/SimpleDeleteModal";

const ProductItem = ({ item, isDeleting, doneDeleting, onDelete }) => {
  const push = useNavigate();
  const { idBrand } = useParams();

  return (
    <Grid container alignItems="center" spacing={2}>
      <Grid item xs={2} sm={1}>
        <img
          width="80%"
          height="auto"
          src={`${POS_MEDIA_URL}/foto_produk/${item.foto_produk}`}
          alt={item.nama_produk}
        />
      </Grid>
      <Grid item xs={3} sm={4}>
        <Typography component="p">{item.nama_produk}</Typography>
      </Grid>
      <Grid item xs={3} sm={3}>
        <Typography component="p">Rp {item.harga?.toLocaleString()}</Typography>
      </Grid>
      <Grid item xs={4} sm={4}>
        <Box
          display="flex"
          alignItems="center"
          sx={{ gap: "1rem", flexWrap: "wrap" }}
        >
          <Button
            variant="contained"
            onClick={() => {
              push(`/pos-system/produk/${idBrand}/update/${item.id_produk}`);
            }}
          >
            <EditIcon />
          </Button>
          <SimpleDeleteModal
            text={<DeleteIcon />}
            item="Produk"
            itemName={item.nama_produk}
            idItem={item.id_produk}
            deleteHandler={onDelete}
            isDeleting={isDeleting}
            doneDeleting={doneDeleting}
          />
        </Box>
      </Grid>
    </Grid>
  );
};

export default ProductItem;
