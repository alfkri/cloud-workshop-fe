import React from "react";
import "./menuItem.css";
import { Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { POS_MEDIA_URL } from "../../lib/server";
import { connect } from "react-redux";
import { addProduct } from "../../store/actions/action";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 600,
    display: "flex",
    cursor: "pointer",
    boxShadow: theme.shadows[5],
  },
  boldText: {
    fontWeight: 700,
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: 280,
  },
  content: {
    flex: "1 0 auto",
  },
  p: {
    fontSize: 12,
    fontWeight: 700,
  },
  cover: {
    width: 250,
    height: 110,
  },
}));

const MenuItem = ({ item, addProduct }) => {
  const classes = useStyles();

  const addNewProduct = () => {
    addProduct({
      id_produk: item.id_produk,
      nama_produk: item.nama_produk,
      harga: item.harga,
      jumlah: 1,
    });
  };
  return (
    <Card id="menuItem" className={classes.root} onClick={addNewProduct}>
      <CardMedia
        className={classes.cover}
        image={`${POS_MEDIA_URL}/foto_produk/${item.foto_produk}`}
        title={item.nama_produk}
      />
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" variant="p" className={classes.boldText}>
            {item.nama_produk}
          </Typography>
          <Typography
            component="h6"
            color="textSecondary"
            className={classes.p}
          >
            Rp {item.harga?.toLocaleString()}
          </Typography>
        </CardContent>
      </div>
    </Card>
  );
};

export default connect(null, { addProduct })(MenuItem);
