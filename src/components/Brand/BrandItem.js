import { Box, Button, CardActions, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useNavigate } from "react-router-dom";

import DeleteIcon from "@material-ui/icons/Delete";

import { POS_MEDIA_URL } from "../../lib/server";
import SimpleDeleteModal from "../Reusable/SimpleDeleteModal";

const useStyles = makeStyles((theme) => ({
  boldText: {
    fontWeight: 700,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  updateButton: {
    flexGrow: 1,
  },
}));

const BrandItem = ({ item, isDeleting, doneDeleting, onDelete }) => {
  const classes = useStyles();
  const push = useNavigate();

  const updateBrandHandler = () => {
    push(`/pos-system/brand/update/${item.id_brand}`);
  };

  return (
    <Card id="productItem" variant="outlined">
      <CardMedia
        image={`${POS_MEDIA_URL}/foto_brand/${item.foto_brand}`}
        title={item.nama_brand}
        className={classes.media}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {item.nama_brand}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {item.deskripsi_brand}
        </Typography>
      </CardContent>
      <CardActions>
        <Box display="flex" width="100%" sx={{ gap: "1rem" }}>
          <Button
            size="medium"
            variant="contained"
            className={classes.updateButton}
            color="primary"
            onClick={updateBrandHandler}
          >
            Update
          </Button>
          <SimpleDeleteModal
            text={<DeleteIcon />}
            item="Brand"
            itemName={item.nama_brand}
            idItem={item.id_brand}
            deleteHandler={onDelete}
            isDeleting={isDeleting}
            doneDeleting={doneDeleting}
          />
        </Box>
      </CardActions>
    </Card>
  );
};

export default BrandItem;
