import { CardActionArea, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import { makeStyles } from "@material-ui/core/styles";
import { useNavigate } from "react-router-dom";
import { POS_MEDIA_URL } from "../../lib/server";

const useStyles = makeStyles((theme) => ({
  boldText: {
    fontWeight: 700,
  },
  media: {
    height: 140,
  },
}));

const BrandItemClickable = ({ item, imgURL }) => {
  const push = useNavigate();
  const classes = useStyles();
  const imgPath = imgURL ? imgURL : POS_MEDIA_URL;

  const onClickHandler = () => {
    push(`${item.id_brand}`);
  };

  return (
    <Card id="productItem" variant="outlined">
      <CardActionArea onClick={onClickHandler}>
        <CardMedia
          image={`${imgPath}/foto_brand/${item.foto_brand}`}
          title={item.nama_brand}
          className={classes.media}
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="h6"
            component="h2"
            align="center"
            className={classes.boldText}
          >
            {item.nama_brand}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default BrandItemClickable;
