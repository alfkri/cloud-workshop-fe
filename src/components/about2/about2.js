import React from "react";
import { Link } from "react-router-dom";
import abimg from "../../images/about/bg-about.jpg";

const About2 = (props) => {
  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  return (
    <section className="wpo-about-section section-padding">
      <div className="container">
        <div className="wpo-about-section-wrapper">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12 col-12">
              <div className="wpo-about-img">
                <img src={abimg} alt="" />
              </div>
            </div>
            <div className="col-lg-6 col-md-12 col-12">
              <div className="wpo-about-content">
                <div className="about-title">
                  {/* <span>Exclusive Offer</span> */}
                  <h2>Apa itu Cloud Bengkel?</h2>
                </div>
                <div className="wpo-about-content-inner">
                  <p>
                    Cloud Bengkel merupakan sebuah bengkel yang sudah dilengkapi
                    dengan fasilitas yang mendukung dibantu oleh kemajuan
                    teknologi. Sistem pemesanan dan pembayaran yang praktis,
                    bisa dilakukan kapanpun dan di manapun.
                  </p>
                  <p>
                    Kami berinovasi untuk memberikan keuntungan besar di pasar
                    pengiriman makanan dengan belanja modal rendah, peluang
                    ekspansi tinggi, dan mengurangi biaya overhead.
                  </p>
                  {/* <div className="about-info-wrap">
                    <div className="about-info-left">
                      <p>2 Days / 3 Night</p>
                      <ul>
                        <li>
                          <i className="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <i className="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <i className="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <i className="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <span>
                            <i className="fa fa-star" aria-hidden="true"></i>
                          </span>
                        </li>
                      </ul>
                    </div>
                    <div className="about-info-right">
                      <p>Only</p>
                      <h3>$2500</h3>
                    </div>
                  </div> */}
                  {/* <Link className="theme-btn" onClick={ClickHandler} to="/room">
                    Book Now
                  </Link> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About2;
