import { Box, Button, Grid, Typography } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { client } from "../../lib/client";
import { POS_MEDIA_URL } from "../../lib/server";
import SimpleDeleteModal from "../Reusable/SimpleDeleteModal";

const QRCodeItem = ({ item, isDeleting, doneDeleting, onDelete }) => {
  const push = useNavigate();
  // const { idBrand } = useParams();

  return (
    <Grid container alignItems="center" justifyContent="center" spacing={2}>
      <Grid item xs={10} sm={10} justifyContent="center">
        <h3 component="h4" className="text-center">
          Scan Me!
        </h3>
      </Grid>
      <Grid
        item
        xs={10}
        sm={10}
        px={5}
        justifyContent="center"
        className="text-center"
      >
        <img
          width="300px"
          height="300px"
          src={`${POS_MEDIA_URL}/foto_qrscan/${item.qrcode}`}
        />
      </Grid>
      <Grid item xs={10} sm={10} justifyContent="center">
        <Typography component="p" className="text-center">
          {item.nama}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default QRCodeItem;
