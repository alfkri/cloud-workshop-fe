import { Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { POS_MEDIA_URL } from "../../lib/server";
import { addProduct } from "../../store/actions/action";

const useStyles = makeStyles((theme) => ({
  listProduct: {
    // backgroundColor: "red",
    height: "100vh",
  },
  listOrder: {
    backgroundColor: "#2c3248",
    height: "100vh",
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const ProductItem = ({ item, addProduct }) => {
  const classes = useStyles();

  const addNewProduct = () => {
    addProduct({
      id_produk: item.id_produk,
      nama_produk: item.nama_produk,
      harga: item.harga,
      jumlah: 1,
    });
  };

  return (
    <Card id="productItem">
      <CardActionArea onClick={addNewProduct}>
        <CardMedia
          className={classes.media}
          image={`${POS_MEDIA_URL}/foto_produk/${item.foto_produk}`}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="h6"
            component="h2"
            align="center"
            className={classes.boldText}
          >
            {item.nama_produk}
          </Typography>
          <Typography variant="body2" component="p" align="center">
            Rp {item.harga?.toLocaleString()}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default connect(null, { addProduct })(ProductItem);
