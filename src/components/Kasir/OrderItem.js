import { Box, TextField, Typography } from "@material-ui/core";
import { AddBox, IndeterminateCheckBox } from "@material-ui/icons";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  incrementQTYOrder,
  decrementQTYOrder,
} from "../../store/actions/action";
import { connect } from "react-redux";

const useStyles = makeStyles(theme => ({
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  icons: {
    color: "#FFFFFF",
    cursor: "pointer",
  },
  orderItem: {
    display: "flex",

    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
  quantity: {
    marginTop: "0.5rem",
  },
}));

const OrderItem = ({ item, incrementQTYOrder, decrementQTYOrder }) => {
  const classes = useStyles();

  const incrementQtyProduct = idProduct => {
    incrementQTYOrder(idProduct);
  };

  const decrementQtyProduct = idProduct => {
    decrementQTYOrder(idProduct);
  };

  return (
    <Box
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      py={2}
      id="orderItem"
      className={classes.orderItem}
    >
      <Box>
        <Typography component="p" variant="body1" className={classes.boldText}>
          {item.nama_produk}
        </Typography>
        <Typography component="p" variant="body2">
          Rp {item.harga?.toLocaleString()}/unit
        </Typography>
      </Box>
      <Box display="flex" alignItems="center" className={classes.quantity}>
        <Box onClick={() => decrementQtyProduct(item.id_produk)}>
          <IndeterminateCheckBox className={classes.icons} fontSize="large" />
        </Box>
        <TextField
          id="outlined-basic"
          size="small"
          variant="outlined"
          value={item.jumlah}
          className={classes.input}
          inputProps={{ style: { padding: "5px", color: "white" } }}
        />
        <Box onClick={() => incrementQtyProduct(item.id_produk)}>
          <AddBox className={classes.icons} fontSize="large" />
        </Box>
      </Box>
    </Box>
  );
};

export default connect(null, { incrementQTYOrder, decrementQTYOrder })(
  OrderItem
);
