import React, { useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Box, Divider, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  boldText: {
    fontWeight: 700,
  },
  blackBoldText: {
    fontWeight: 700,
    color: "black",
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
}));

export default function QRCodeModal({
  dataQR,
  isOpen,
  handleClose,
  totalHarga,
}) {
  const classes = useStyles();

  return (
    <div>
      {/* <Button
        size="medium"
        variant="contained"
        color="secondary"
        onClick={handleClickOpen}
      >
        {text}
      </Button> */}
      <Dialog
        open={isOpen}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="sm"
        scroll="paper"
        fullWidth
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          <Typography variant="h6" component="h6" className={classes.boldText}>
            QR Code
          </Typography>
          {/* <Typography component="p">#{dataQR.id_qrcode}</Typography> */}
        </DialogTitle>
        <DialogContent>
          {/* <Box pb={2} pt={4}>
        <Typography variant="h6" component="h2">
          Order Detail
        </Typography>
      </Box> */}
          <Divider className={classes.divider} />
          {dataQR?.map((item, index) => (
            <Box key={item.id_qrcode}>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                py={2}
                id="orderItem"
              >
                <Box>
                  <Typography
                    component="p"
                    variant="body1"
                    className={classes.boldText}
                  >
                    {item.id_qrcode}
                  </Typography>
                  {/* <Typography component="p" variant="body2">
                    Rp {item.harga_produk_satuan?.toLocaleString()}/unit
                  </Typography>
                  <Typography component="p" variant="body2">
                    Jumlah: {item.jumlah}
                  </Typography>
                </Box>
                <Box>
                  <Typography component="p" variant="body2">
                    Rp {order.harga_produk_total?.toLocaleString()}
                  </Typography> */}
                </Box>
              </Box>
              {/* {index !== item.length - 1 && <Divider />} */}
            </Box>
          ))}
        </DialogContent>
        {/* <DialogActions>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            p={2}
            id="orderItem"
            width="100%"
          >
            <Typography
              component="p"
              variant="h6"
              className={classes.blackBoldText}
            >
              Total Harga
            </Typography>
            <Typography
              component="p"
              variant="h6"
              className={classes.blackBoldText}
            >
              Rp{totalHarga?.toLocaleString()}
            </Typography>
          </Box>
        </DialogActions> */}
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
