import React, { Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import Collapse from "@material-ui/core/Collapse";
import FontAwesome from "../../components/UiStyle/FontAwesome";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import { Link } from "react-router-dom";

import "./style.scss";
import { Typography } from "@material-ui/core";
import { toast } from "react-toastify";
import { client } from "../../lib/client";
import { TRANSAKSI_MEDIA_URL } from "../../lib/server";

const cardType = [
  {
    title: "DANA",
    img: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194314_01887_logo_metode_pembayaran.png`,
    service_code: "1084",
  },
  {
    title: "Bayarind",
    img: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194036_97310_logo_metode_pembayaran.png`,
    service_code: "1083",
  },
  {
    title: "Link Aja",
    img: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194402_29481_logo_metode_pembayaran.png`,
    service_code: "1077",
  },
  {
    title: "ShoopePay",
    img: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194520_33201_logo_metode_pembayaran.png`,
    service_code: "1085",
  },
];

const CheckoutSection = ({ data }) => {
  // states
  const kode_unik = Math.floor(Math.random() * (999 - 0 + 1)) + 0;
  const [tabs, setExpanded] = React.useState({
    cupon: false,
    billing_adress: false,
    payment: true,
  });
  const [forms, setForms] = React.useState({
    cupon_key: "",
    card_type: "",
    card_kode: "",
    nama_brand_dapur: "",
    waktu_awal_sewa: "",
    payment_method: "card",
  });

  async function CheckoutHandler(e) {
    e.preventDefault();
    if (
      forms.card_type !== "" &&
      forms.card_kode !== "" &&
      forms.nama_brand_dapur !== "" &&
      forms.waktu_awal_sewa !== ""
    ) {
      const response = await client.post("/dapur/insert_customer_dapur", {
        id_dpr_harga: data?.id_dpr_harga,
        nama_brand_dapur: forms.nama_brand_dapur,
        waktu_awal_sewa: forms.waktu_awal_sewa,
        kode_unik: kode_unik,
        service_code: forms.card_kode,
      });
      if (response.status === 200) {
        window.location.replace(response.data?.data?.redirect_url);
      } else {
        toast.error(response.error);
      }
    } else {
      toast.error("Form tidak valid");
    }
  }

  // tabs handler
  function faqHandler(name) {
    setExpanded({
      cupon: false,
      billing_adress: false,
      payment: true,
      [name]: !tabs[name],
    });
  }

  // forms handler
  const changeHandler = e => {
    setForms({ ...forms, [e.target.name]: e.target.value });
  };

  return (
    <Fragment>
      <Grid className="checkoutWrapper section-padding">
        <Grid className="container" container spacing={3}>
          <Grid item md={6} xs={12}>
            <div className="check-form-area">
              <Grid spacing={4}>
                <InputLabel htmlFor="nama_brand_dapur">
                  Nama Brand Dapur
                </InputLabel>
                <TextField
                  fullWidth
                  id="nama_brand_dapur"
                  type="text"
                  placeholder="Masukkan nama brand Anda"
                  value={forms.nama_brand_dapur}
                  name="nama_brand_dapur"
                  onChange={e => changeHandler(e)}
                />
                <br />
                <br />
                <InputLabel htmlFor="waktu_awal_sewa">
                  Tanggal Mulai Sewa
                </InputLabel>
                <TextField
                  fullWidth
                  type="date"
                  id="waktu_awal_sewa"
                  value={forms.waktu_awal_sewa}
                  name="waktu_awal_sewa"
                  onChange={e => changeHandler(e)}
                />
              </Grid>
              {/* <Grid className="cuponWrap checkoutCard">
                <Button
                  className="collapseBtn"
                  fullWidth
                  onClick={() => faqHandler("cupon")}
                >
                  Have a coupon ? Click here to enter your code.
                  <FontAwesome name={tabs.cupon ? "minus" : "plus"} />
                </Button>
                <Collapse in={tabs.cupon} timeout="auto" unmountOnExit>
                  <Grid className="chCardBody">
                    <p>If you have coupon code,please apply it</p>
                    <form className="cuponForm">
                      <TextField
                        fullWidth
                        type="text"
                        className="formInput radiusNone"
                        value={forms.cupon_key}
                        name="cupon_key"
                        onChange={e => changeHandler(e)}
                      />
                      <Button className="cBtn cBtnBlack">Apply</Button>
                    </form>
                  </Grid>
                </Collapse>
              </Grid> */}
              <br />
              <br />
              <Grid className="cuponWrap checkoutCard">
                <Button
                  className="collapseBtn"
                  fullWidth
                  onClick={() => faqHandler("payment")}
                >
                  Payment Method
                  <FontAwesome name={tabs.payment ? "minus" : "plus"} />
                </Button>
                <Grid className="chCardBody">
                  <Collapse in={tabs.payment} timeout="auto">
                    <Collapse
                      in={forms.payment_method === "card"}
                      timeout="auto"
                    >
                      <Grid className="cardType">
                        {cardType.map((item, i) => (
                          <Grid
                            key={i}
                            className={`cardItem ${
                              forms.card_type === item.title ? "active" : null
                            }`}
                            onClick={() =>
                              setForms({
                                ...forms,
                                card_type: item.title,
                                card_kode: item.service_code,
                              })
                            }
                          >
                            <img src={item.img} alt={item.title} />
                          </Grid>
                        ))}
                      </Grid>
                      {forms.card_type !== "" && (
                        <Typography component="p" sx={{ marginBottom: "20px" }}>
                          Anda memilih metode pembayaran menggunakan{" "}
                          <strong>{forms.card_type}</strong>. Setelah memproses
                          transaksi, Anda akan diarahkan di halaman pembayaran{" "}
                          <strong>{forms.card_type}</strong> untuk melunasi
                          penyewaan dapur dengan nominal Rp
                          {(
                            data?.harga * data?.minimum_sewa +
                            kode_unik
                          )?.toLocaleString()}
                        </Typography>
                      )}
                    </Collapse>
                    <Collapse
                      in={forms.payment_method === "card"}
                      timeout="auto"
                    >
                      <Grid className="cardType">
                        <Link
                          to="/"
                          className="cBtn cBtnLarge cBtnTheme mt-20 ml-15"
                          onClick={CheckoutHandler}
                        >
                          Proceed to Checkout
                        </Link>
                      </Grid>
                    </Collapse>
                  </Collapse>
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item md={6} xs={12}>
            <Grid className="cartStatus">
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Grid className="cartTotals">
                    <h4>Cart Total</h4>
                    <Table>
                      <TableBody>
                        <TableRow>
                          <TableCell>{data?.nama_dapur}</TableCell>
                          <TableCell align="right">
                            Rp{data?.harga?.toLocaleString()}/hari
                          </TableCell>
                        </TableRow>
                        <TableRow className="totalProduct">
                          <TableCell>Minimum Sewa</TableCell>
                          <TableCell align="right">
                            {data?.minimum_sewa}
                          </TableCell>
                        </TableRow>
                        <TableRow className="totalProduct">
                          <TableCell>Kode Unik</TableCell>
                          <TableCell align="right">{kode_unik}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Total Harga</TableCell>
                          <TableCell align="right">
                            Rp
                            {(
                              data?.harga * data?.minimum_sewa +
                              kode_unik
                            )?.toLocaleString()}
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default CheckoutSection;
