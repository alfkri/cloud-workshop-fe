import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import kitchen from "../../images/kitchen.jpg";

const Pricing = [
  {
    prImg: kitchen,
    package: "Sewa Harian",
    idDapurHarga: 1,
    price: "75.000",
    subs: "Hari",
    l1: "Ukuran tenant 3m x 3m",
    l2: "Pendaftaran ke Food Delovery Channel",
    l3: "> 10 Fasilitas Dapur",
    l4: "Minimal 6-10 hari sewa",
  },
  {
    prImg: kitchen,
    package: "Sewa Harian",
    idDapurHarga: 2,
    price: "90.000",
    subs: "Hari",
    l1: "Ukuran tenant 3m x 3m",
    l2: "Pendaftaran ke Food Delovery Channel",
    l3: "> 10 Fasilitas Dapur",
    l4: "Minimal 3-5 hari sewa",
  },
  {
    prImg: kitchen,
    package: "Sewa Harian",
    idDapurHarga: 3,
    price: "100.000",
    subs: "Hari",
    l1: "Ukuran tenant 3m x 3m",
    l2: "Pendaftaran ke Food Delovery Channel",
    l3: "> 10 Fasilitas Dapur",
    l4: "1-2 hari sewa",
  },
  {
    prImg: kitchen,
    package: "Sewa Bulanan",
    idDapurHarga: 4,
    price: "2.000.000",
    subs: "Bulan",
    l1: "Ukuran tenant 3m x 3m",
    l2: "Pendaftaran ke Food Delovery Channel",
    l3: "> 10 Fasilitas Dapur",
  },
  {
    prImg: kitchen,
    package: "Sewa Bulanan",
    idDapurHarga: 7,
    price: "2.250.000",
    subs: "Bulan",
    l1: "Ukuran tenant 4m x 3m",
    l2: "Pendaftaran ke Food Delovery Channel",
    l3: "> 10 Fasilitas Dapur",
    l4: "Bisa request 1-2 utensil yang dibutuhkan",
  },
];

const ClickHandler = () => {
  window.scrollTo(10, 0);
};

const PricingSection = ({ available, auth }) => {
  return (
    <section className="wpo-pricing-section">
      <div className="container">
        <div className="wpo-pricing-wrap">
          <div className="row">
            {Pricing.map((pricing, pitem) => (
              <div className="col col-lg-4 col-md-6 col-12 mb-4" key={pitem}>
                <div className="wpo-pricing-item">
                  <div className="wpo-pricing-top">
                    <div className="wpo-pricing-img">
                      <img src={pricing.prImg} alt="" />
                    </div>
                    <div className="wpo-pricing-text">
                      <h4>{pricing.package}</h4>
                      <h2>
                        Rp{pricing.price}
                        <span>/Per {pricing.subs}</span>
                      </h2>
                    </div>
                  </div>
                  <div className="wpo-pricing-bottom">
                    <div className="wpo-pricing-bottom-text">
                      <ul>
                        <li>{pricing.l1}</li>
                        <li>{pricing.l2}</li>
                        <li>{pricing.l3}</li>
                        <li>{pricing.l4}</li>
                      </ul>
                      <Link
                        onClick={ClickHandler}
                        className="theme-btn"
                        to={
                          available
                            ? auth.isLoggedIn
                              ? `/checkout/${pricing.idDapurHarga}`
                              : "/login"
                            : "#"
                        }
                      >
                        Sewa Kitchen
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(PricingSection);
