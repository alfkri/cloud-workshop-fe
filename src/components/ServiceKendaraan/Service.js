import React from "react";
import "./service.css";
import service from "../../api/serviceKendaraan";
import { AiFillStar } from "react-icons/ai";

const Service = () => {
  return (
    <div className="container">
      <div className="row mb-5 mt-5 p-3 d-flex justify-content-center">
        <div className="col-md-10 col-sm-12">
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              placeholder="Cari Paket Service.."
            ></input>
          </div>
        </div>
      </div>
      <div className="row mt-5">
        {service.map((item) => (
          <div className="col-md-4 col" key={item.id}>
            <div className="card card-service shadow box-shadow pointer mb-5">
              <img
                className="card-img-top"
                src={item.imgUrl}
                alt="Card image cap"
              ></img>
              <div className="card-body">
                <h5 className="card-title">{item.title}</h5>
                <p className="card-text">Rp. {item.harga}</p>
                <p className="card-text">
                  <AiFillStar style={{ marginBottom: "3px" }} size={"16px"} />{" "}
                  {item.rating} / 5
                </p>
                <button href="#" className="btn btn-warning">
                  Beli
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Service;
