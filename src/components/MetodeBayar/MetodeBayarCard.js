import { Box, Paper, Typography } from "@material-ui/core";
import { Delete, Edit } from "@material-ui/icons";
import SimpleDeleteModal from "../Reusable/SimpleDeleteModal";

const MetodeBayarCard = ({
  metode,
  id_metode,
  isDeleting,
  doneDeleting,
  onDelete,
}) => {
  return (
    <Paper>
      <Box
        p={2}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography component="p" variant="h6">
          {metode}
        </Typography>
        <Box>
          <Edit color="primary" />
          <SimpleDeleteModal
            text={<Delete />}
            item="Metode Pembayaran"
            itemName={metode}
            idItem={id_metode}
            deleteHandler={onDelete}
            isDeleting={isDeleting}
            doneDeleting={doneDeleting}
          />
        </Box>
      </Box>
    </Paper>
  );
};

export default MetodeBayarCard;
