import { Box, Button, CardActions, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useNavigate } from "react-router-dom";

import DeleteIcon from "@material-ui/icons/Delete";

import { POS_MEDIA_URL } from "../../lib/server";
import SimpleDeleteModal from "../Reusable/SimpleDeleteModal";

const useStyles = makeStyles(theme => ({
  boldText: {
    fontWeight: 700,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  updateButton: {
    flexGrow: 1,
  },
}));

const MetodeBayarItem = ({ item, isDeleting, doneDeleting, onDelete }) => {
  const classes = useStyles();
  const push = useNavigate();

  const updateMetodeBayarHandler = () => {
    push(`/pos-system/metode-bayar/update/${item.id_metode_pembayaran_pos}`);
  };

  return (
    <Card id="productItem" variant="outlined">
      <CardMedia
        image={`${POS_MEDIA_URL}/foto_pembayaran/${item.foto_pembayaran}`}
        title={item.metode_pembayaran}
        className={classes.media}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {item.metode_pembayaran}
        </Typography>
        {/* <Typography variant="body2" color="textSecondary" component="p">
          {item.deskripsi_brand}
        </Typography> */}
      </CardContent>
      <CardActions>
        <Box display="flex" width="100%" sx={{ gap: "1rem" }}>
          <Button
            size="medium"
            variant="contained"
            className={classes.updateButton}
            color="primary"
            onClick={updateMetodeBayarHandler}
          >
            Update
          </Button>
          <SimpleDeleteModal
            text={<DeleteIcon />}
            item="Metode Pembayaran"
            itemName={item.metode_pembayaran}
            idItem={item.id_metode_pembayaran_pos}
            deleteHandler={onDelete}
            isDeleting={isDeleting}
            doneDeleting={doneDeleting}
          />
        </Box>
      </CardActions>
    </Card>
  );
};

export default MetodeBayarItem;
