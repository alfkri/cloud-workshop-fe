import React from "react";
import "./bengkel.css";
import bengkel from "../../api/bengkel";
import { AiFillStar } from "react-icons/ai";
import { getBengkel } from "../../api/apiBengkel";
import { useState, useEffect } from "react";

const Bengkel = () => {
  const [bengkel, setBengkel] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const result = await getBengkel();
      setBengkel(result);
    };

    fetchData();
  }, []);

  const handleSearch = (event) => {
    setSearchKeyword(event.target.value);
  };

  const filteredBengkel = bengkel.filter((dataBengkel) =>
    dataBengkel.nama_bengkel.toLowerCase().includes(searchKeyword.toLowerCase())
  );
  return (
    <div className="container">
      <div className="row mb-5 mt-5 p-3 d-flex justify-content-center">
        <div className="col-md-10 col-sm-12">
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              placeholder="Cari Lokasi Bengkel.."
              onChange={handleSearch}
            ></input>
          </div>
        </div>
      </div>
      <div className="row mt-5">
        {filteredBengkel.map((item) => (
          <div className="col-md-4 col" key={item.id_tempat}>
            <div className="card card-bengkel shadow box-shadow pointer mb-5">
              <img
                className="card-img-top"
                src="https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png"
                alt="Gambar Bengkel"
              ></img>
              <div className="card-body">
                <h5 className="card-title">{item.nama_bengkel}</h5>
                <p className="card-text">Lokasi: {item.lokasi_bengkel}</p>
                <p className="card-text">Rp. {item.harga_sewa}</p>
                {/* <p className="card-text">
                  <AiFillStar style={{ marginBottom: "3px" }} size={"16px"} />{" "}
                  {item.rating} / 5
                </p> */}
                <button href="#" className="btn btn-warning">
                  Beli
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Bengkel;
