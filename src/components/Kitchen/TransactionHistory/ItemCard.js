import { Box, Button, Grid, Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { TRANSAKSI_MEDIA_URL } from "../../../lib/server";

const useStyles = makeStyles(theme => ({
  root: {
    overflowX: "auto",
  },
  status: {
    color: theme.palette.primary.contrastText,
  },
  title: {
    color: theme.palette.primary.dark,
    fontWeight: 600,
    marginBottom: "0.5rem",
  },
}));

const STATUS = {
  1: "Menunggu Pembayaran",
  2: "Masa Sewa Aktif",
  3: "Masa Sewa Berakhir",
  4: "Transaksi Gagal",
};

const PAYMENTMTD = {
  1084: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194314_01887_logo_metode_pembayaran.png`,
  1083: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194036_97310_logo_metode_pembayaran.png`,
  1077: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194402_29481_logo_metode_pembayaran.png`,
  1085: `${TRANSAKSI_MEDIA_URL}/2022-10-20_194520_33201_logo_metode_pembayaran.png`,
};

const ItemCard = ({ data }) => {
  const classes = useStyles();

  const bayarClickHandler = url => {
    window.open(url, "_blank");
  };

  return (
    <Paper className={classes.root}>
      <Box px={2} py={3} sx={{ minWidth: "700px" }}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Box
              p={0.5}
              sx={{
                backgroundColor: "red",
                display: "inline-block",
                borderRadius: "4px",
              }}
              mb={1}
            >
              <Typography className={classes.status}>
                {STATUS[data.status_pemesanan]}
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={3}>
            <Typography
              component="p"
              variant="body1"
              className={classes.title}
              fontWeight={600}
            >
              Nama Dapur
            </Typography>
            <Typography component="p" variant="body1">
              {data.nama_brand_dapur}
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography
              component="p"
              variant="body1"
              className={classes.title}
              fontWeight={600}
            >
              {data.status_pemesanan === 1 || data.status_pemesanan === 4
                ? "Batas Bayar"
                : "Batas Sewa"}
            </Typography>
            <Typography component="p" variant="body1">
              {data.status_pemesanan === 1 || data.status_pemesanan === 4
                ? data.waktu_akhir_pembayaran
                : data.waktu_akhir_sewa}
            </Typography>
          </Grid>
          <Grid item xs={2}>
            <Typography
              component="p"
              variant="body1"
              className={classes.title}
              fontWeight={600}
            >
              Harga
            </Typography>
            <Typography component="p" variant="body1">
              Rp{data.total_harga_pembayaran?.toLocaleString()}
            </Typography>
          </Grid>
          <Grid item xs={2}>
            <Typography
              component="p"
              variant="body1"
              className={classes.title}
              fontWeight={600}
            >
              Metode Pembayaran
            </Typography>
            {data.id_kupon !== null ? (
              <Typography component="p" variant="body1">
                Menggunakan kupon
              </Typography>
            ) : (
              <img width="70%" src={PAYMENTMTD[data.service_code]} alt="" />
            )}
          </Grid>
          <Grid item xs={2}>
            <Typography
              component="p"
              variant="body1"
              className={classes.title}
              fontWeight={600}
            >
              Aksi
            </Typography>
            <Button
              variant="contained"
              color="primary"
              disabled={data.status_pemesanan !== 1}
              onClick={() => bayarClickHandler(data.redirect_url)}
            >
              Bayar
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};

export default ItemCard;
