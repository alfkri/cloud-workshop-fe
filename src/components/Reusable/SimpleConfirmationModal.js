import React, { useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default function SimpleConfirmationModal({
  text,
  idItem,
  action,
  question,
  actionHandler,
  isProcessing,
  actionDone,
}) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (actionDone) {
      handleClose();
    }
  }, [actionDone]);

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        {text}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Konfirmasi Tindakan</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {question}
          </DialogContentText>
        </DialogContent>
        {!isProcessing && (
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => actionHandler(idItem)}
              color="primary"
              autoFocus
            >
              {action}
            </Button>
          </DialogActions>
        )}
        {isProcessing && (
          <DialogActions>
            <Button variant="contained" disabled>
              Processing...
            </Button>
          </DialogActions>
        )}
      </Dialog>
    </div>
  );
}
