import React, { useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default function SimpleDeleteModal({
  text,
  item,
  idItem,
  itemName,
  deleteHandler,
  isDeleting,
  doneDeleting,
}) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (doneDeleting) {
      handleClose();
    }
  }, [doneDeleting]);

  return (
    <div>
      <Button
        size="medium"
        variant="contained"
        color="secondary"
        onClick={handleClickOpen}
      >
        {text}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {`${item} Akan Dihapus dari Sistem`}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {item} {itemName} akan dihapus dari sistem dan tidak akan
            ditampilkan lagi. Apakah anda yakin untuk melanjutkan aksi ini?
          </DialogContentText>
        </DialogContent>
        {!isDeleting && (
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => deleteHandler(idItem)}
              color="primary"
              autoFocus
            >
              Hapus
            </Button>
          </DialogActions>
        )}
        {isDeleting && (
          <DialogActions>
            <Button variant="contained" disabled>
              Deleting...
            </Button>
          </DialogActions>
        )}
      </Dialog>
    </div>
  );
}
