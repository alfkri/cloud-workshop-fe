import { Grid, Typography } from "@material-ui/core";
import React from "react";

const OrderItem = ({ item }) => {
  return (
    <Grid container alignItems="center" spacing={2}>
      <Grid item xs={4}>
        <Typography component="p">{item.nomor_order}</Typography>
      </Grid>
      <Grid item xs={3}>
        <Typography component="p">
          Rp{item.total_harga?.toLocaleString()}
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <Typography component="p">{item.metode_pembayaran}</Typography>
      </Grid>
      <Grid item xs={3}>
        <Typography component="p">{item.waktu_order}</Typography>
      </Grid>
    </Grid>
  );
};

export default OrderItem;
