import React from "react";
import { Navigation, Pagination, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

import hero1 from "../../images/homepageSlider/hero-1.png";
import hero2 from "../../images/homepageSlider/hero-2.png";
import hero3 from "../../images/homepageSlider/hero-3.png";

const Hero = (props) => {
  return (
    <section className="wpo-hero-slider">
      <div className="swiper-container">
        <div className="swiper-wrapper">
          <Swiper
            // install Swiper modules
            modules={[Navigation, Pagination, A11y]}
            spaceBetween={0}
            slidesPerView={1}
            pagination={{ clickable: true }}
            loop={true}
            speed={1800}
            parallax={true}
            navigation
          >
            <SwiperSlide>
              <div
                className="swiper-slide"
                style={{ backgroundImage: `url(${hero2})` }}
              >
                <div className="slide-inner slide-bg-image">
                  <div className="container-fluid">
                    <div className="slide-content">
                      <div data-swiper-parallax="300" className="slide-title">
                        <h3 style={{ textAlign: "center !important" }}>
                          Cloud Bengkel di Lokasi STRATEGIS dengan Harga
                          Terjangkau!
                        </h3>
                      </div>
                      <div className="clearfix"></div>
                      <div
                        data-swiper-parallax="500"
                        className="slide-btns slide-desc"
                      >
                        {/* <a href="#" className="theme-btn">
                          Coming Soon
                        </a> */}
                        <p>
                          Sewa Cloud Bengkel, sewa alat bengkel, beli sparepart,
                          dan service dengan harga sesuai kebutuhan yang
                          terjangkau serta memiliki kualitas yang baik. Dapatkan
                          paket harga dan keuntungan lainnya hanya di Cloud
                          Bengkel.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div
                className="swiper-slide"
                style={{ backgroundImage: `url(${hero1})` }}
              >
                <div className="slide-inner slide-bg-image">
                  <div className="container-fluid">
                    <div className="slide-content">
                      <div data-swiper-parallax="300" className="slide-title">
                        <h3 style={{ textAlign: "center !important" }}>
                          Tingkatkan Kualitas Kendaraan KAMU dan Jadi Sahabat
                          Cloud Bengkel!
                        </h3>
                      </div>
                      <div className="clearfix"></div>
                      <div
                        data-swiper-parallax="500"
                        className="slide-btns slide-desc"
                      >
                        <p>
                          Cloud Bengkel tidak hanya menyewakan tempat,
                          menyewakan alat bengkel, jual sparepart dan service,
                          namun tersedia pelatihan perlatihan baik secara
                          offline maupun online agar dapat mengembangkan
                          keterampilan mekanik yang baik.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div
                className="swiper-slide"
                style={{ backgroundImage: `url(${hero3})` }}
              >
                <div className="slide-inner slide-bg-image">
                  <div className="container-fluid">
                    <div className="slide-content">
                      <div data-swiper-parallax="300" className="slide-title">
                        <h3 style={{ textAlign: "center !important" }}>
                          Tertarik Menjadi Langganan?
                        </h3>
                      </div>
                      <div className="clearfix"></div>
                      <div
                        data-swiper-parallax="500"
                        className="slide-btns slide-desc"
                      >
                        {/* <a href="#" className="theme-btn">
                          Coming Soon
                        </a> */}
                        <p>
                          Cloud Bengkel menawarkan harga terjangkau yang dapat
                          membantu anda dalam merawat kendaraan dan bergaransi.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            {/* <SwiperSlide>
              <div
                className="swiper-slide"
                style={{ backgroundImage: `url(${hero2})` }}
              >
                <div className="slide-inner slide-bg-image">
                  <div className="container-fluid">
                    <div className="slide-content">
                      <div data-swiper-parallax="300" className="slide-title">
                        <h2>Find Your Perfect Place To Stay</h2>
                      </div>
                      <div className="clearfix"></div>
                      <div data-swiper-parallax="500" className="slide-btns">
                        <a href="room.html" className="theme-btn">
                          Book Now
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div
                className="swiper-slide"
                style={{ backgroundImage: `url(${hero3})` }}
              >
                <div className="slide-inner slide-bg-image">
                  <div className="container-fluid">
                    <div className="slide-content">
                      <div data-swiper-parallax="300" className="slide-title">
                        <h2>Find Your Perfect Place To Stay</h2>
                      </div>
                      <div className="clearfix"></div>
                      <div data-swiper-parallax="500" className="slide-btns">
                        <a href="room.html" className="theme-btn">
                          Book Now
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SwiperSlide> */}
            ...
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default Hero;
