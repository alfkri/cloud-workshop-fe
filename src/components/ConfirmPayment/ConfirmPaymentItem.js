import { Box, Button, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import SimpleConfirmationModal from "../Reusable/SimpleConfirmationModal";

const useStyles = makeStyles(theme => ({
  subtitle: {
    color: "rgba(0, 0, 0, 0.54)",
  },
}));

const ConfirmPaymentItem = ({
  item,
  handleOpen,
  onConfirm,
  isProcessing,
  isDone,
}) => {
  const classes = useStyles();
  return (
    <Grid container alignItems="center" spacing={2}>
      <Grid item xs={2}>
        <Typography
          component="h6"
          variant="subtitle2"
          className={classes.subtitle}
        >
          Nomor Order
        </Typography>
        <Typography component="p">{item.nomor_order}</Typography>
      </Grid>
      <Grid item xs={3}>
        <Typography
          component="h6"
          variant="subtitle2"
          className={classes.subtitle}
        >
          Total Harga
        </Typography>
        <Typography component="p">
          Rp{item.total_harga?.toLocaleString()}
        </Typography>
      </Grid>
      {/* <Grid item xs={2}>
      <Typography
          component="h6"
          variant="subtitle2"
          className={classes.subtitle}
        >
          Metode Bayar
        </Typography>
        <Typography component="p">{item.metode_pembayaran}</Typography>
      </Grid> */}
      <Grid item xs={3}>
        <Typography
          component="h6"
          variant="subtitle2"
          className={classes.subtitle}
        >
          Waktu Order
        </Typography>
        <Typography component="p">{item.waktu_order.split("GMT")}</Typography>
      </Grid>
      <Grid item xs={3}>
        <Box display="flex" sx={{ gap: "1rem " }}>
          <Button
            variant="outlined"
            color="primary"
            onClick={() => handleOpen(item.id_order, item.total_harga)}
          >
            Detail
          </Button>
          <SimpleConfirmationModal
            text="Konfirmasi"
            question={`Pembayaran order dengan nomor ${item.nomor_order} akan disetujui dan pemesanan akan diproses oleh tim dapur. Apakah Anda yakin?`}
            idItem={item.id_produk}
            // DUMMY
            actionHandler={() => onConfirm(item.id_order_customer_qr)}
            isProcessing={isProcessing}
            actionDone={isDone}
            action="Konfirmasi"
          />
        </Box>
      </Grid>
    </Grid>
  );
};

export default ConfirmPaymentItem;
