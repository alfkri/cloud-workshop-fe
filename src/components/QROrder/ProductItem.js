import { Box, Button, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import AddCircleIcon from "@material-ui/icons/AddCircle";
import RemoveCircleRoundedIcon from "@material-ui/icons/RemoveCircleRounded";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { POS_MEDIA_URL } from "../../lib/server";

const useStyles = makeStyles((theme) => ({
  listProduct: {
    // backgroundColor: "red",
    height: "100vh",
  },
  listOrder: {
    backgroundColor: "#2c3248",
    height: "100vh",
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  productCard: {
    borderRadius: "10px",
    "&#QRProduk": {
      "& p": {
        color: "black",
      },
      "& button *": {
        color: "white",
      },
    },
  },
}));

const ProductItem = ({ item, onAdd, orderList, onInc, onDec }) => {
  const classes = useStyles();
  const orderedProduct = orderList.find(
    (product) => product.id_produk === item.id_produk
  );

  return (
    <Box p={2} bgcolor="#EFF3F7" className={classes.productCard} id="QRProduk">
      <Box display="flex">
        <Box sx={{ flexGrow: 1 }}>
          <Typography component="p" variant="h6" className={classes.boldText}>
            {item.nama_produk}
          </Typography>
          <Typography component="p" variant="caption">
            {item.deskripsi_produk}
          </Typography>
        </Box>
        <Box width="60px" height="60px" sx={{ borderRadius: "15px" }}>
          <img
            src={`${POS_MEDIA_URL}/foto_produk/${item.foto_produk}`}
            height="100%"
            width="auto"
            style={{ objectFit: "cover", borderRadius: "15px" }}
            alt={item.nama_produk}
          />
        </Box>
      </Box>
      <Box
        mt={2}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography component="p" variant="body2" style={{ fontWeight: 600 }}>
          Rp {item.harga?.toLocaleString()}
        </Typography>
        {orderedProduct ? (
          <Box display="flex" alignItems="center" style={{ gap: "5px" }}>
            <Box onClick={() => onDec(item.id_produk)} sx={{ lineHeight: 0 }}>
              <RemoveCircleRoundedIcon fontSize="small" color="primary" />
            </Box>
            <Typography component="p">{orderedProduct?.jumlah}</Typography>
            <Box onClick={() => onInc(item.id_produk)} sx={{ lineHeight: 0 }}>
              <AddCircleIcon fontSize="small" color="primary" />
            </Box>
          </Box>
        ) : (
          <Button
            variant="contained"
            size="small"
            color="primary"
            onClick={() => onAdd(item)}
          >
            Tambah
          </Button>
        )}
      </Box>
    </Box>
    // <Card id="productItem" style={{ backgroundColor: "#EFF3F7" }}>
    //   <CardActionArea onClick={addNewProduct}>
    //     <CardMedia
    //       className={classes.media}
    //       image={`${POS_MEDIA_URL}/foto_produk/${item.foto_produk}`}
    //       title="Contemplative Reptile"
    //     />
    //     <CardContent>
    //       <Typography
    //         gutterBottom
    //         variant="h6"
    //         component="h2"
    //         align="center"
    //         className={classes.boldText}
    //       >
    //         {item.nama_produk}
    //       </Typography>
    //       <Typography variant="body2" component="p" align="center">
    //         Rp {item.harga?.toLocaleString()}
    //       </Typography>
    //     </CardContent>
    //   </CardActionArea>
    // </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    orderList: state.qrOrder.order,
  };
};

export default connect(mapStateToProps, {})(ProductItem);
