import React, { useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Box, Divider, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  boldText: {
    fontWeight: 700,
  },
  blackBoldText: {
    fontWeight: 700,
    color: "black",
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
}));

export default function OrderDetailModal({
  data,
  isOpen,
  handleClose,
  totalHarga,
  isLoading,
}) {
  const classes = useStyles();

  return (
    <div>
      <Dialog
        open={isOpen}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="sm"
        scroll="paper"
        fullWidth
      >
        {isLoading ? (
          "Loading..."
        ) : (
          <>
            <DialogTitle id="customized-dialog-title" onClose={handleClose}>
              <Typography
                variant="h6"
                component="h2"
                className={classes.boldText}
              >
                Order Information
              </Typography>
              <Typography component="p">#{data[0]?.nomor_order}</Typography>
            </DialogTitle>
            <DialogContent>
              <Divider className={classes.divider} />
              {data.map((order, index) => (
                <Box key={order.nama_produk}>
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    alignItems="center"
                    py={2}
                    id="orderItem"
                  >
                    <Box>
                      <Typography
                        component="p"
                        variant="body1"
                        className={classes.boldText}
                      >
                        {order.nama_produk}
                      </Typography>
                      <Typography component="p" variant="body2">
                        Rp {order.harga_produk_satuan?.toLocaleString()}/unit
                      </Typography>
                      <Typography component="p" variant="body2">
                        Jumlah: {order.jumlah}
                      </Typography>
                    </Box>
                    <Box>
                      <Typography component="p" variant="body2">
                        Rp {order.harga_produk_total?.toLocaleString()}
                      </Typography>
                    </Box>
                  </Box>
                  {index !== order.length - 1 && <Divider />}
                </Box>
              ))}
            </DialogContent>
            <DialogActions>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                p={2}
                id="orderItem"
                width="100%"
              >
                <Typography
                  component="p"
                  variant="h6"
                  className={classes.blackBoldText}
                >
                  Total Harga
                </Typography>
                <Typography
                  component="p"
                  variant="h6"
                  className={classes.blackBoldText}
                >
                  Rp{totalHarga?.toLocaleString()}
                </Typography>
              </Box>
            </DialogActions>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Close
              </Button>
            </DialogActions>
          </>
        )}
      </Dialog>
    </div>
  );
}
