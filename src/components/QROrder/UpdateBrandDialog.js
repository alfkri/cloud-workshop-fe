import { Box, Button, Dialog, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import UpdateBrand from "../../images/QROrder/updateBrand.svg";

const useStyles = makeStyles(theme => ({
  root: {
    "& p, & h6": {
      color: "black",
      textAlign: "center",
    },
    "& h6": {
      fontWeight: 700,
    },
  },
}));

const UpdateBrandDialog = props => {
  const classes = useStyles();
  const { onClose, open } = props;

  const handleClose = selectedValue => {
    onClose(selectedValue);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="update-brand-confirm-action"
      open={open}
      maxWidth="xs"
      className={classes.root}
    >
      <Box p={4}>
        <Box sx={{ textAlign: "center" }} mb={2}>
          <img src={UpdateBrand} width="70%" alt="Confirm Update Brand" />
        </Box>
        <Typography component="h6" variant="h6">
          Mau pesan dari resto ini aja?
        </Typography>
        <Typography component="p" variant="body2">
          Kalau pesan dari resto ini, pesanan dari resto sebelumnya akan dihapus
        </Typography>
        <Box
          mt={2}
          display="flex"
          justifyContent="space-between"
          sx={{ gap: "1rem" }}
        >
          <Button
            color="primary"
            variant="outlined"
            onClick={() => handleClose(0)}
            fullWidth
          >
            Batal
          </Button>
          <Button
            color="primary"
            variant="contained"
            onClick={() => handleClose(1)}
            fullWidth
          >
            Ganti resto
          </Button>
        </Box>
      </Box>
    </Dialog>
  );
};

export default UpdateBrandDialog;
