import { Box, Button, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import AddCircleIcon from "@material-ui/icons/AddCircle";
import RemoveCircleRoundedIcon from "@material-ui/icons/RemoveCircleRounded";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { POS_MEDIA_URL } from "../../lib/server";
import {
  incrementQRItemOrderQTY,
  decrementQRItemOrderQTY,
} from "../../store/actions/action";

const useStyles = makeStyles((theme) => ({
  listProduct: {
    // backgroundColor: "red",
    height: "100vh",
  },
  listOrder: {
    backgroundColor: "#2c3248",
    height: "100vh",
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  productCard: {
    borderRadius: "10px",
    "&#QRProduk": {
      "& p": {
        color: "black",
      },
      "& button *": {
        color: "white",
      },
    },
  },
}));

const ProductItem = ({
  item,
  incrementQRItemOrderQTY,
  decrementQRItemOrderQTY,
}) => {
  const classes = useStyles();

  return (
    <Box p={1} className={classes.productCard} id="QRProduk">
      <Box display="flex">
        <Box sx={{ flexGrow: 1 }}>
          <Typography
            component="p"
            variant="body1"
            className={classes.boldText}
          >
            {item.nama_produk}
          </Typography>
          <Typography component="p" variant="subtitle2">
            Rp {item.harga?.toLocaleString()}
          </Typography>
        </Box>
        <Box width="60px" height="60px" sx={{ borderRadius: "15px" }}>
          <img
            src={`${POS_MEDIA_URL}/foto_produk/${item.foto_produk}`}
            height="100%"
            width="auto"
            style={{ objectFit: "cover", borderRadius: "15px" }}
            alt={item.nama_produk}
          />
        </Box>
      </Box>
      <Box mt={2} display="flex" justifyContent="flex-end" alignItems="center">
        <Box display="flex" alignItems="center" style={{ gap: "5px" }}>
          <Box
            onClick={() => decrementQRItemOrderQTY(item.id_produk)}
            sx={{ lineHeight: 0 }}
          >
            <RemoveCircleRoundedIcon fontSize="small" color="primary" />
          </Box>
          <Typography component="p">{item.jumlah}</Typography>
          <Box
            onClick={() => incrementQRItemOrderQTY(item.id_produk)}
            sx={{ lineHeight: 0 }}
          >
            <AddCircleIcon fontSize="small" color="primary" />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

const mapStateToProps = (state) => {
  return {
    orderList: state.qrOrder.order,
  };
};

export default connect(mapStateToProps, {
  incrementQRItemOrderQTY,
  decrementQRItemOrderQTY,
})(ProductItem);
