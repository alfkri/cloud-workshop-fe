import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import Slide from "@material-ui/core/Slide";
import { Badge, Box, Grid } from "@material-ui/core";
import { connect } from "react-redux";
import OrderItem from "./OrderItem";
import { POS_MEDIA_URL } from "../../lib/server";
import { ArrowBack } from "@material-ui/icons";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative",
    backgroundColor: "#2c3248",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
    color: "white",
  },
  orderInfo: {
    "& *": {
      color: "white",
    },
  },
  boldText: {
    fontWeight: 700,
  },
  listOrder: {
    "& p": {
      color: "black",
    },
  },
  paymentSummary: {
    "& p": {
      color: "black",
      fontSize: "10px",
    },
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const ListOrderDialog = ({
  orderList,
  totalPrice,
  brandInfo,
  totalItems,
  onChangeMetodeBayar,
  listMetodeBayar,
  metodeBayarDipilih,
  dataMetodeBayarDipilih,
  onSubmit,
  isSubmitting,
}) => {
  const classes = useStyles();
  const { qr } = useParams();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = () => {
    const data = {
      qr: qr,
    };
    onSubmit(data);
  };

  useEffect(() => {
    if (orderList.length <= 0) {
      setOpen(false);
    }
  }, [orderList]);

  return (
    <div>
      <Box
        sx={{
          position: "fixed",
          bottom: 0,
          left: 0,
          right: 0,
          zIndex: 1,
          display: `${orderList.length > 0 ? "block" : "none"}`,
          cursor: "pointer",
        }}
        pb={2}
        px={1}
        onClick={handleClickOpen}
      >
        <Box
          bgcolor="#2c3248"
          width="100%"
          sx={{
            borderRadius: "30px",
            boxShadow: "0px 4px 3px -2px rgba(0,0,0,0.35)",
          }}
          px={2.5}
          py={1}
        >
          <Box
            display="flex"
            alignItems="center"
            sx={{ gap: "10px" }}
            className={classes.orderInfo}
          >
            <Box sx={{ flexGrow: 1 }}>
              <Typography
                component="p"
                variant="body2"
                className={classes.boldText}
              >
                {totalItems} item
              </Typography>
              <Typography component="p" variant="caption">
                Dari {brandInfo.namaBrand}
              </Typography>
            </Box>
            <Typography
              component="p"
              variant="body1"
              className={classes.boldText}
            >
              {totalPrice?.toLocaleString()}
            </Typography>
            <LocalMallIcon fontSize="medium" />
          </Box>
        </Box>
      </Box>
      <Dialog
        fullScreen
        open={open}
        keepMounted
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <ArrowBack />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {brandInfo.namaBrand}
            </Typography>
          </Toolbar>
        </AppBar>
        <Box
          sx={{ backgroundColor: "#EFF3F7", paddingBottom: "120px" }}
          height="fit-content"
          // pb={4}
          className={classes.listOrder}
        >
          <Box p={2} bgcolor="white">
            <Box mb={2}>
              <Typography
                component="h6"
                variant="h6"
                className={classes.boldText}
              >
                Daftar Order
              </Typography>
            </Box>
            <Grid container spacing={1}>
              {orderList.map(item => (
                <Grid
                  item
                  xs={12}
                  sm={4}
                  md={3}
                  key={item.id_produk}
                  className={classes.productCard}
                  style={{ borderTop: "1px solid lightgray" }}
                >
                  <OrderItem item={item} />
                </Grid>
              ))}
              <Grid
                item
                xs={12}
                className={classes.productCard}
                style={{ borderTop: "1px solid lightgray" }}
              >
                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                  p={1}
                >
                  <Box>
                    <Typography
                      component="p"
                      variant="body2"
                      className={classes.boldText}
                    >
                      Mau tambah pesanan?
                    </Typography>
                    <Typography component="p" variant="caption">
                      Masih bisa lihat daftar menu lagi, loh
                    </Typography>
                  </Box>
                  <Button
                    variant="outlined"
                    color="primary"
                    size="small"
                    onClick={handleClose}
                  >
                    Tambah
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
          <Box p={2} m={2} bgcolor="white" sx={{ borderRadius: "15px" }}>
            <Typography
              component="p"
              variant="body2"
              className={classes.boldText}
            >
              Pilih Metode Pembayaran*
            </Typography>
            <Box display="flex" sx={{ gap: "10px" }} mt={2}>
              {listMetodeBayar.map(metode => (
                <Box
                  onClick={() => onChangeMetodeBayar(metode)}
                  key={metode.metode_pembayaran}
                  sx={{ cursor: "pointer" }}
                >
                  <Badge
                    key={metode.metode_pembayaran}
                    badgeContent={
                      metode.id_metode_pembayaran_pos ===
                        metodeBayarDipilih && (
                        <CheckCircleIcon color="secondary" />
                      )
                    }
                  >
                    <Box
                      sx={{
                        borderRadius: "5px",
                        color: "white",
                        border: `1px solid ${
                          metode.id_metode_pembayaran_pos === metodeBayarDipilih
                            ? "red"
                            : "black"
                        }`,
                        backgroundColor: "transparent",
                        gap: "5px",
                      }}
                      pr={2}
                      display="flex"
                      alignItems="center"
                    >
                      <img
                        src={`${POS_MEDIA_URL}/foto_pembayaran/${metode.foto_pembayaran}`}
                        alt=""
                        style={{ borderRadius: "5px 0px 0px 5px" }}
                        height="32px"
                        width="auto"
                      />{" "}
                      <Typography component="p" variant="caption">
                        {metode.metode_pembayaran}
                      </Typography>
                    </Box>
                  </Badge>
                </Box>
              ))}
            </Box>
          </Box>
          <Box p={2} bgcolor="white">
            <Box>
              <Typography
                component="h6"
                variant="h6"
                className={classes.boldText}
              >
                Ringkasan Pembayaran
              </Typography>
            </Box>
            <Box display="flex" justifyContent="space-between">
              <Typography component="p" variant="caption">
                Harga
              </Typography>
              <Typography
                component="p"
                variant="caption"
                style={{ textAlign: "right", flexGrow: 1 }}
              >
                {totalPrice?.toLocaleString(0)}
              </Typography>
            </Box>
            {/* <Box display="flex" justifyContent="space-between">
              <Typography component="p" variant="caption">
                Kode Unik Metode Pembayaran
              </Typography>
              <Typography
                component="p"
                variant="caption"
                style={{ textAlign: "right", flexGrow: 1 }}
              >
                200
              </Typography>
            </Box> */}
            <Box
              display="flex"
              justifyContent="space-between"
              mt={1}
              pt={1}
              style={{ borderTop: "1px solid lightgray" }}
            >
              <Typography
                component="p"
                variant="subtitle2"
                className={classes.boldText}
              >
                Total pembayaran
              </Typography>
              <Typography
                component="p"
                variant="caption"
                style={{ textAlign: "right", flexGrow: 1 }}
              >
                {totalPrice?.toLocaleString(0)}
              </Typography>
            </Box>
          </Box>
        </Box>
        <Box
          p={2}
          sx={{
            position: "fixed",
            bottom: 0,
            right: 0,
            left: 0,
            borderTop: "1px solid lightgrey",
          }}
          width="100%"
          bgcolor="white"
        >
          {metodeBayarDipilih && (
            <>
              <Box
                mb={1}
                display="flex"
                alignItems="center"
                sx={{ gap: "1rem" }}
              >
                <Box height="20px">
                  <img
                    src={`${POS_MEDIA_URL}/foto_pembayaran/${dataMetodeBayarDipilih?.foto_pembayaran}`}
                    alt=""
                    height="100%"
                    width="auto"
                    style={{
                      borderRadius: "5px 0px 0px 5px",
                      objectFit: "cover",
                    }}
                  />
                </Box>
                <Box
                  sx={{ flexGrow: 1, lineHeight: 0 }}
                  className={classes.paymentSummary}
                >
                  <Typography component="p" variant="caption">
                    {dataMetodeBayarDipilih.metode_pembayaran}
                  </Typography>
                  <Typography
                    component="p"
                    variant="caption"
                    className={classes.boldText}
                  >
                    {totalPrice?.toLocaleString()}
                  </Typography>
                </Box>
              </Box>
            </>
          )}
          {isSubmitting ? (
            <Button variant="contained" size="large" disabled fullWidth>
              Memproses...
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              size="large"
              onClick={() => handleSubmit()}
              fullWidth
            >
              Pesan Sekarang
            </Button>
          )}
        </Box>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    orderList: state.qrOrder.order,
    totalPrice: state.qrOrder.totalPrice,
    totalItems: state.qrOrder.totalItems,
    brandInfo: state.qrOrder.orderBrand,
  };
};
export default connect(mapStateToProps, {})(ListOrderDialog);
