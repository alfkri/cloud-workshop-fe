import React, { useState } from "react";
import { connect } from "react-redux";
import MobileMenu from "../MobileMenu";
import { logout } from "../../store/actions/action";
import { Link, useNavigate } from "react-router-dom";

const Header = props => {
  const push = useNavigate();
  const [menuActive, setMenuState] = useState(false);
  const [cartActive, setcartState] = useState(false);
  const SubmitHandler = e => {
    e.preventDefault();
  };

  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  const LogoutHandler = e => {
    e.preventDefault();
    // alert("logout");
    props.logout();
    push("/kds-system/login");
  };

  const { auth } = props;

  return (
    <header id="header" className={props.topbarBlock}>
      <div className={`wpo-site-header ${props.hclass}`}>
        <nav className="navigation navbar navbar-expand-lg navbar-light">
          <div className="container-fluid">
            <div className="row align-items-center">
              <div className="col-lg-3 col-md-3 col-3 d-lg-none dl-block">
                <div className="mobail-menu">
                  <MobileMenu />
                </div>
              </div>
              <div className="col-lg-2 col-md-6 col-6">
                <div className="navbar-header">
                  <Link
                    onClick={ClickHandler}
                    className="navbar-brand logo"
                    style={{ display: "inherit" }}
                    to="/kds-system/list-display"
                  >
                    <h2 style={{ color: "darkblue" }}>KDS</h2>
                  </Link>
                </div>
              </div>
              <div className="col-lg-10 col-md-3 col-3">
                <div
                  id="navbar"
                  className="collapse navbar-collapse navigation-holder"
                >
                  <button className="menu-close">
                    <i className="ti-close"></i>
                  </button>
                  <ul className="nav navbar-nav mb-2 mb-lg-0">
                    <li>
                      <Link
                        onClick={ClickHandler}
                        to="/kds-system/list-display"
                      >
                        List Display
                      </Link>
                    </li>
                    {auth.isLoggedIn ? (
                      <li className="menu-item-has-children">
                        <Link to="#">Welcome, Admin</Link>
                        <ul className="sub-menu">
                          <li>
                            <Link
                              onClick={LogoutHandler}
                              to="/kds-system/login"
                            >
                              Logout
                            </Link>
                          </li>
                        </ul>
                      </li>
                    ) : (
                      <li>
                        <Link onClick={ClickHandler} to="/login">
                          Login
                        </Link>
                      </li>
                    )}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth,
  };
};
export default connect(mapStateToProps, { logout })(Header);
