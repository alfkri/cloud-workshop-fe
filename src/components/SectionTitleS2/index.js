import React from "react";

const SectionTitleS2 = props => {
  return (
    <div className="wpo-section-title-s2">
      <h2>{props.MainTitle}</h2>
      <p>
        Kami menyediakan berbagai fasilitas dan kelebihan yang akan memanjakan
        bisnis kuliner Anda
      </p>
    </div>
  );
};

export default SectionTitleS2;
