import React from "react";
import { Link } from "react-router-dom";
import Logo from "../../images/logoCW1.png";
import Services from "../../api/service";
import { BsTiktok } from "react-icons/bs";

const Footer = (props) => {
  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  return (
    <footer className="wpo-site-footer">
      <div className="wpo-upper-footer">
        <div className="container">
          <div className="row">
            <div className="col col-lg-3 col-md-6 col-sm-12 col-12">
              <div className="widget about-widget">
                <div className="logo widget-title">
                  <img src={Logo} alt="footer" />
                  {/* <h1>BisaKitchen</h1> */}
                </div>
                <p>
                  Menyediakan{" "}
                  <strong>
                    jasa layanan service, sewa alat bengkel, dan jual sparepart
                  </strong>
                </p>
                <ul>
                  <li>
                    <a
                      href="https://www.instagram.com/cloud_bengkel.id/"
                      target="_blank"
                    >
                      <i className="ti-instagram"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.instagram.com/cloud_bengkel.id/"
                      target="_blank"
                    >
                      <i className="ti-youtube"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="http://www.tiktok.com/@cloud_bengkel.id"
                      target="_blank"
                    >
                      <BsTiktok size={"22px"} className="tiktok" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col col-lg-3 col-md-6 col-sm-12 col-12">
              <div className="widget link-widget s1">
                <div className="widget-title">
                  <h3>Services</h3>
                </div>
                <ul>
                  {Services.slice(0, 6).map((service, sitem) => (
                    <li className="service-item" key={sitem}>
                      <p>{service.title}</p>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            <div className="col col-lg-3 col-md-6 col-sm-12 col-12">
              <div className="widget link-widget">
                <div className="widget-title">
                  <h3>Important Link</h3>
                </div>
                <ul>
                  <li>
                    <Link onClick={ClickHandler} to="/about">
                      About Us
                    </Link>
                  </li>
                  <li>
                    <Link onClick={ClickHandler} to="/produk/bengkel">
                      Sewa Bengkel
                    </Link>
                  </li>
                  <li>
                    <Link onClick={ClickHandler} to="/produk/alat">
                      Sewa Alat
                    </Link>
                  </li>
                  <li>
                    <Link onClick={ClickHandler} to="/produk/sparepart">
                      Beli Sparepart
                    </Link>
                  </li>
                  <li>
                    <Link onClick={ClickHandler} to="/produk/service">
                      Service Kendaraan
                    </Link>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col col-lg-3 col-md-6 col-sm-12 col-12">
              <div className="widget wpo-service-link-widget">
                <div className="widget-title">
                  <h3>Contact </h3>
                </div>
                <div className="contact-ft">
                  <ul>
                    <li>
                      <i className="fi flaticon-placeholder"></i>Jl. Petogogan I
                      No.41, RT.1/RW.12, Pulo, Kec. Kby. Baru, Kota Jakarta
                      Selatan, Daerah Khusus Ibukota Jakarta, 12140.
                    </li>
                    <li>
                      <i className="fi flaticon-phone-call"></i>Coming soon
                    </li>
                    <li>
                      <i className="fi flaticon-send"></i>Coming soon
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="wpo-lower-footer">
        <div className="container">
          <div className="row">
            <div className="col col-xs-12">
              <p className="copyright">
                Copyright &copy; 2023 Cloud Bengkel by PT REZEKI MEKANIK
                INDONESIA . All Rights Reserved.
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
