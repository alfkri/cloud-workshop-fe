import React from "react";
import "./produk.css";
import alat from "../../api/alat";
import { Link } from "react-router-dom";

const Produk = () => {
  return (
    <div className="container mt-5 mb-5">
      <div className="row">
        <div className="col-lg-3 col-md-6 col">
          <div className="card card-produk">
            <Link className="img-card" to={"/produk/alat"}>
              <img src="https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png" />
            </Link>
            <div className="card-content">
              <h4 className="card-title">
                <Link to={"/produk/alat"}>Sewa Alat</Link>
              </h4>
              <p className="">
                Tutorial to make a carousel bootstrap by adding more wonderful
                effect fadein ...
              </p>
            </div>
            <div className="card-read-more">
              <Link
                to={"/produk/alat"}
                className="btn btn-link btn-primary-outline"
              >
                Lihat Selengkapnya
              </Link>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 col">
          <div className="card card-produk">
            <Link className="img-card" to={"/produk/bengkel"}>
              <img src="https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png" />
            </Link>
            <div className="card-content">
              <h4 className="card-title">
                <Link to={"/produk/bengkel"}>Sewa Bengkel</Link>
              </h4>
              <p className="">
                Tutorial to make a carousel bootstrap by adding more wonderful
                effect fadein ...
              </p>
            </div>
            <div className="card-read-more">
              <Link
                to={"/produk/bengkel"}
                className="btn btn-link btn-primary-outline"
              >
                Lihat Selengkapnya
              </Link>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 col">
          <div className="card card-produk">
            <Link className="img-card" to={"/produk/service"}>
              <img src="https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png" />
            </Link>
            <div className="card-content">
              <h4 className="card-title">
                <Link to={"/produk/service"}>Service Kendaraan</Link>
              </h4>
              <p className="">
                Tutorial to make a carousel bootstrap by adding more wonderful
                effect fadein ...
              </p>
            </div>
            <div className="card-read-more">
              <Link
                to={"/produk/service"}
                className="btn btn-link btn-primary-outline"
              >
                Lihat Selengkapnya
              </Link>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 col">
          <div className="card card-produk">
            <Link className="img-card" to={"/produk/sparepart"}>
              <img src="https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png" />
            </Link>
            <div className="card-content">
              <h4 className="card-title">
                <Link to={"/produk/sparepart"}>Beli Sparepart</Link>
              </h4>
              <p className="">
                Tutorial to make a carousel bootstrap by adding more wonderful
                effect fadein ...
              </p>
            </div>
            <div className="card-read-more">
              <Link
                to={"/produk/sparepart"}
                className="btn btn-link btn-primary-outline"
              >
                Lihat Selengkapnya
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Produk;
