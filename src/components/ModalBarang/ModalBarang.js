import React, { useState } from "react";
import styles from "./ModalBarang.module.css";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

function ModalBarang(props) {
  const { item, showModal, handleClose } = props;
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  const gambar = [
    "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_110200_course.png",
    "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023210_course.png",
    "https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023317_course.png",
  ];

  const handlePrevious = () => {
    setCurrentImageIndex((prevIndex) =>
      prevIndex === 0 ? gambar.length - 1 : prevIndex - 1
    );
  };

  const handleNext = () => {
    setCurrentImageIndex((prevIndex) =>
      prevIndex === gambar.length - 1 ? 0 : prevIndex + 1
    );
  };

  return (
    <div
      className={
        showModal ? `${styles.modalBarang} modal d-block` : "modal d-none"
      }
      tabIndex="-1"
    >
      <div className={`modal-dialog modal-lg ${styles.modalDialog}`}>
        <div className={`modal-content ${styles.modalContent}`}>
          <div className="modal-header">
            <p className={`modal-title ${styles.modalTitle}`}>
              {item.nama_barang}
            </p>
            <button type="button" className="close" onClick={handleClose}>
              <span>&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="row">
              <div className="col-md-4 mb-2">
                <div className="row">
                  <div className="d-flex justify-content-center">
                    <img
                      src={gambar[currentImageIndex]}
                      alt={item.nama_barang}
                      className={`img-fluid rounded mb-2 ${styles.imgItem}`}
                    />
                  </div>
                </div>
                {gambar.length > 1 ? (
                  <div className="d-flex justify-content-center">
                    <button
                      type="button"
                      className="btn"
                      onClick={handlePrevious}
                    >
                      <BsChevronLeft size={30} className={styles.imgBtn} />
                    </button>
                    <button type="button" className="btn" onClick={handleNext}>
                      <BsChevronRight size={30} className={styles.imgBtn} />
                    </button>
                  </div>
                ) : null}
              </div>
              <div className="col-md-6">
                <p>
                  <strong>Deskripsi: </strong>
                  {item.deskripsi_barang}
                </p>
                <p>
                  <strong>Harga: </strong>
                  Rp. {item.harga_barang}
                </p>
                <p>
                  <strong>Kondisi: </strong>
                  {item.kondisi}
                </p>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-warning">
              Beli
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalBarang;
