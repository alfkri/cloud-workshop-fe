import React from "react";
import "./sparepart.css";

import { AiFillStar } from "react-icons/ai";
import { getBarang } from "../../api/apiBarang.js";
import { useState, useEffect } from "react";
import ModalBarang from "../ModalBarang/ModalBarang";

const Sparepart = () => {
  const [barang, setBarang] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState("");
  const [selectedBarang, setSelectedBarang] = useState(null);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const result = await getBarang();
      setBarang(result);
    };

    fetchData();
  }, []);

  const handleSearch = (event) => {
    setSearchKeyword(event.target.value);
  };

  const handleModalOpen = (item) => {
    setSelectedBarang(item);
    setShowModal(true);
  };

  const handleModalClose = () => {
    setSelectedBarang(null);
    setShowModal(false);
  };

  const filteredBarang = barang.filter((dataBarang) =>
    dataBarang.nama_barang.toLowerCase().includes(searchKeyword.toLowerCase())
  );

  return (
    <div className="container">
      <div className="row mb-5 mt-5 p-3 d-flex justify-content-center">
        <div className="col-md-10 col-sm-12">
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              placeholder="Cari Barang.."
              onChange={handleSearch}
            ></input>
          </div>
        </div>
      </div>
      <div className="row mt-5">
        {filteredBarang.map((item) => (
          <div className="col-md-4 col" key={item.id_barang}>
            <div className="card card-sparepart shadow box-shadow pointer mb-5">
              <img
                className="card-img-top"
                src="https://gate.bisaai.id/dokter_mekanik_prod/course/media/2022-09-20_023125_course.png"
                alt="Gambar Bengkel"
              ></img>
              <div className="card-body">
                <h5 title={item.nama_barang} className="card-title title">
                  {item.nama_barang}
                </h5>
                <p className="card-text">Rp. {item.harga_barang}</p>
                <p className="card-text">
                  <AiFillStar style={{ marginBottom: "3px" }} size={"16px"} />{" "}
                  {item.rating} / 5
                </p>
                <div className="row">
                  <div className="col-md-3">
                    <button
                      className=" btn btn-warning"
                      onClick={() => handleModalOpen(item)}
                    >
                      Details
                    </button>
                  </div>
                  <div className="col-md-2">
                    <button className="btn btn-warning">Beli</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
      {selectedBarang ? (
        <ModalBarang
          item={selectedBarang}
          showModal={showModal}
          handleClose={handleModalClose}
        />
      ) : null}
    </div>
  );
};

export default Sparepart;
