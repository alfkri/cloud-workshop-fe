import { LOGIN, LOGOUT, LOAD_USER } from "../actions/type";

const initialState = {
  email: "",
  token: "",
  isLoggedIn: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_USER:
      const storageData = localStorage.getItem("kitct");
      const user = JSON.parse(storageData);
      state.email = user.email;
      state.token = user.token;
      state.isLoggedIn = true;
      return {
        ...state,
      };
    case LOGIN:
      state.email = action.user.email;
      state.token = action.user.token;
      state.isLoggedIn = true;

      localStorage.setItem("kitct", JSON.stringify(action.user));
      return {
        ...state,
      };
    case LOGOUT:
      state.email = "";
      state.token = "";
      state.isLoggedIn = false;
      localStorage.removeItem("kitct");
      localStorage.removeItem("persist:root");
      return {
        ...state,
      };
    default:
      return state;
  }
};
export default authReducer;
