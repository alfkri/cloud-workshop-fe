import {
  ADD_QR_ORDER_ITEM,
  DEC_QTY_QR_ORDER_ITEM,
  INC_QTY_QR_ORDER_ITEM,
  RESET_PRODUCTS_QR_ORDER_ITEM,
  SUBMIT_ORDER_QR,
} from "../actions/type";

const initialValue = {
  order: [],
  orderBrand: {
    idBrand: null,
    namaBrand: "",
  },
  totalPrice: 0,
  totalItems: 0,
};

export const qrOrderReducer = (state = initialValue, action) => {
  switch (action.type) {
    case RESET_PRODUCTS_QR_ORDER_ITEM:
      return {
        ...state,
        order: [],
        orderBrand: {},
        totalPrice: 0,
        totalItems: 0,
      };

    case ADD_QR_ORDER_ITEM:
      const productId = action.product.id_produk;
      if (
        state.order.findIndex(product => product.id_produk === productId) === -1
      ) {
        state.order.push(action.product);
        state.totalPrice += action.product.harga;
        state.totalItems++;
        state.orderBrand.idBrand = action.product.idBrand;
        state.orderBrand.namaBrand = action.product.namaBrand;
      }
      return { ...state };
    case INC_QTY_QR_ORDER_ITEM:
      const inc_productId = action.id_produk;
      state.totalItems++;
      const new_order = state.order.reduce((orderList, product) => {
        if (product.id_produk === inc_productId) {
          orderList.push({
            ...product,
            jumlah: product.jumlah + 1,
          });
          state.totalPrice += product.harga;
        } else {
          orderList.push(product);
        }
        return orderList;
      }, []);
      return { ...state, order: new_order };

    case DEC_QTY_QR_ORDER_ITEM:
      const decProductId = action.id_produk;
      state.totalItems--;
      const decOrder = state.order.reduce((orderList, product) => {
        if (product.id_produk === decProductId) {
          if (product.jumlah > 1) {
            orderList.push({
              ...product,
              jumlah: product.jumlah - 1,
            });
          }
          state.totalPrice -= product.harga;
        } else {
          orderList.push(product);
        }
        return orderList;
      }, []);
      if (decOrder.length <= 0) {
        state.orderBrand.idBrand = null;
        state.orderBrand.namaBrand = "";
      }
      return { ...state, order: decOrder };

    case SUBMIT_ORDER_QR:
      return { ...state, order: [], totalPrice: 0, totalItems: 0 };

    default:
      return state;
  }
};
