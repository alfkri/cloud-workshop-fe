import { combineReducers } from "redux";
import authReducer from "./auth";
import orderReducer from "./order";
import displayReducer from "./display";
import { qrOrderReducer } from "./qrOrder";

const rootReducer = combineReducers({
  auth: authReducer,
  orderList: orderReducer,
  display: displayReducer,
  qrOrder: qrOrderReducer,
});

export default rootReducer;
