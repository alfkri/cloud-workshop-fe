import { GET_ALL_DISPLAY, DISPLAY_ERROR } from "../actions/type";

const initialValue = {
  display: [],
  status: [],
  //   error: null,
};

const displayReducer = (state = initialValue, action) => {
  switch (action.type) {
    case GET_ALL_DISPLAY:
      return {
        ...state,
        display: [],
        // status: action.status,
      };
    case DISPLAY_ERROR:
      return {
        ...state,
        error: action.payload,
        // status: "FAIL",
      };
    default:
      return state;
  }
};

export default displayReducer;
