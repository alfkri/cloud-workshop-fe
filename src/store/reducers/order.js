import {
  ADD_PRODUCT,
  DEC_QTY,
  INC_QTY,
  GET_ALL_ORDER,
  SUBMIT_ORDER,
  LOAD_PRODUCTS,
} from "../actions/type";

const initialValue = {
  order: [],
  totalPrice: 0,
};

export const orderReducer = (state = initialValue, action) => {
  switch (action.type) {
    case LOAD_PRODUCTS:
      return { ...state, order: [], totalPrice: 0 };

    case SUBMIT_ORDER:
      return { ...state, order: [], totalPrice: 0 };

    case ADD_PRODUCT:
      const productId = action.product.id_produk;
      if (
        state.order.findIndex(product => product.id_produk === productId) === -1
      ) {
        state.order.push(action.product);
        state.totalPrice += action.product.harga;
      }
      return { ...state };

    case INC_QTY:
      const inc_productId = action.id_produk;
      const new_order = state.order.reduce((orderList, product) => {
        if (product.id_produk === inc_productId) {
          orderList.push({
            ...product,
            jumlah: product.jumlah + 1,
          });
          state.totalPrice += product.harga;
        } else {
          orderList.push(product);
        }
        return orderList;
      }, []);
      return { ...state, order: new_order };
    case DEC_QTY:
      const decProductId = action.id_produk;
      const decOrder = state.order.reduce((orderList, product) => {
        if (product.id_produk === decProductId) {
          if (product.jumlah > 1) {
            orderList.push({
              ...product,
              jumlah: product.jumlah - 1,
            });
          }
          state.totalPrice -= product.harga;
        } else {
          orderList.push(product);
        }
        return orderList;
      }, []);

      return { ...state, order: decOrder };

    default:
      return state;
  }
};

export default orderReducer;
