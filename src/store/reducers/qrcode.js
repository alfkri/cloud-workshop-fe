import { SUBMIT_QR, LOAD_QR } from "../actions/type";

const initialValue = {
  qrcode: [],
};

export const qrReducer = (state = initialValue, action) => {
  switch (action.type) {
    case LOAD_QR:
      return { ...state, qrcode: [] };

    case SUBMIT_QR:
      return { ...state, qrcode: [] };

    default:
      return state;
  }
};

export default qrReducer;
