// User
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const LOAD_USER = "LOAD_USER";

//Lokasi
export const GET_ALL_DISPLAY = "GET_ALL_DISPLAY";
export const DISPLAY_ERROR = "DISPLAY_ERROR";

// Kitchen
export const GET_ALL_KITCHEN = "GET_ALL_KITCHEN";

// POS
export const GET_ALL_ORDER = "GET_ALL_ORDER";
export const INC_QTY = "INC_QTY";
export const ADD_PRODUCT = "ADD_PRODUCT";
export const DEC_QTY = "DEC_QTY";
export const SUBMIT_ORDER = "SUBMIT_ORDER";
export const LOAD_PRODUCTS = "LOAD_PRODUCTS";

//QR Code
export const SUBMIT_QR = "SUBMIT_QR";
export const LOAD_QR = "LOAD_QR";

// QR Order
export const RESET_PRODUCTS_QR_ORDER_ITEM = "RESET_PRODUCTS_QR_ORDER_ITEM";
export const ADD_QR_ORDER_ITEM = "ADD_QR_ORDER_ITEM";
export const INC_QTY_QR_ORDER_ITEM = "INC_QTY_QR_ORDER_ITEM";
export const DEC_QTY_QR_ORDER_ITEM = "DEC_QTY_QR_ORDER_ITEM";
export const SUBMIT_ORDER_QR = "SUBMIT_ORDER_QR";
