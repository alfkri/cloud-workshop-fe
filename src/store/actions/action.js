import * as types from "./type";
import "react-toastify/dist/ReactToastify.min.css";
import {
  REMOVE_FROM_COMPARE_LIST,
  ADD_TO_COMPARE,
  DISPLAY_ERROR,
} from "./type";

// User
export const login = (user) => (dispatch) => {
  dispatch({
    type: types.LOGIN,
    user,
  });
};

export const logout = () => ({
  type: types.LOGOUT,
});

export const loadUser = () => ({
  type: types.LOAD_USER,
});

// Kitchen
export const getAllKitchen = () => ({
  type: types.GET_ALL_KITCHEN,
});

// POS
export const submitOrder = () => ({
  type: types.SUBMIT_ORDER,
});

export const getAllOrder = () => ({
  type: types.GET_ALL_ORDER,
});

export const loadProducts = () => ({
  type: types.LOAD_PRODUCTS,
});

export const addProduct = (product) => (dispatch) => {
  dispatch({
    type: types.ADD_PRODUCT,
    product,
  });
};

export const incrementQTYOrder = (id_produk) => (dispatch) => {
  dispatch({
    type: types.INC_QTY,
    id_produk,
  });
};

export const decrementQTYOrder = (id_produk) => (dispatch) => {
  dispatch({
    type: types.DEC_QTY,
    id_produk,
  });
};
//DISPLAY
export const getAllDisplay = () => ({
  type: types.GET_ALL_DISPLAY,
});
// export const getAllDisplay = () => async (dispatch) => {
//   try {
//     const res = await client.get("/pos/get_display");
//     const data = await res.json();
//     dispatch({
//       type: types.GET_ALL_DISPLAY,
//       display: data,
//     });
//   } catch (err) {
//     dispatch({
//       type: types.DISPLAY_ERROR,
//       // payload: err.response.data.msg,
//     });
//   }
// };

//QR CODE
export const submitQR = () => ({
  type: types.SUBMIT_ORDER,
});

export const loadQR = () => ({
  type: types.LOAD_PRODUCTS,
});

// QR ORDER
export const resetProductsQROrderItem = () => ({
  type: types.RESET_PRODUCTS_QR_ORDER_ITEM,
});

export const addQRItemOrder = (product) => (dispatch) => {
  dispatch({
    type: types.ADD_QR_ORDER_ITEM,
    product,
  });
};

export const incrementQRItemOrderQTY = (id_produk) => (dispatch) => {
  dispatch({
    type: types.INC_QTY_QR_ORDER_ITEM,
    id_produk,
  });
};

export const decrementQRItemOrderQTY = (id_produk) => (dispatch) => {
  dispatch({
    type: types.DEC_QTY_QR_ORDER_ITEM,
    id_produk,
  });
};

export const submitOrderByQR = () => ({
  type: types.SUBMIT_ORDER_QR,
});
