import { useState, useEffect, useRef } from "react";

export default function useComponentVisible(initialIsVisible) {
  const [isComponentVisible, setIsComponentVisible] =
    useState(initialIsVisible);
  const ref = useRef(null);
  const buttonTriggerRef = useRef(null);

  const handleHideDropdown = event => {
    if (event.key === "Escape") {
      setIsComponentVisible(false);
    }
  };

  const handleClickOutside = event => {
    // console.log(ref.current);
    // console.log(buttonTriggerRef.current);
    // console.log(event.target);
    // console.log(buttonTriggerRef.current === event.target);
    // console.log(buttonTriggerRef.current.contains(event.target));
    if (
      ref?.current &&
      !ref?.current?.contains(event.target) &&
      !buttonTriggerRef?.current?.contains(event.target)
    ) {
      setIsComponentVisible(false);
    } else if (buttonTriggerRef?.current?.contains(event.target)) {
      setIsComponentVisible(true);
    }
  };

  useEffect(() => {
    document.addEventListener("keydown", handleHideDropdown, true);
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("keydown", handleHideDropdown, true);
      document.removeEventListener("click", handleClickOutside, true);
    };
  });

  return { buttonTriggerRef, ref, isComponentVisible, setIsComponentVisible };
}
