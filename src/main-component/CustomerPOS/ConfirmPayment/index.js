import {
  Box,
  Grid,
  InputAdornment,
  OutlinedInput,
  Paper,
  Typography,
} from "@material-ui/core";

// import "../ProductPage/productpage.scss";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import { client } from "../../../lib/client";
import { toast } from "react-toastify";
import OrderItem from "../../../components/OrderManagement/OrderItem";
import { makeStyles } from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";
import ConfirmPaymentItem from "../../../components/ConfirmPayment/ConfirmPaymentItem";
import OrderDetailModal from "../../../components/QROrder/OrderDetailModal";
import { Search } from "@material-ui/icons";
import { clientDev } from "../../../lib/clientDev";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const ConfirmPayment = () => {
  const push = useNavigate();
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingNextPage, setIsLoadingNextPage] = useState(false);
  const [listData, setListData] = useState([]);
  const [pageCount, setPageCount] = useState(1);
  const [page, setPage] = useState(1);

  // Get Detail Payment Item
  const [isOpenOrderModal, setIsOpenOrderModal] = useState(false);
  const [isLoadingDetail, setIsLoadingDetail] = useState(false);
  const [dataOrder, setDataOrder] = useState([]);
  const [totalHarga, setTotalHarga] = useState(0);

  // Confirmation
  const [isProcessing, setIsProcessing] = useState(false);
  const [isDone, setIsDone] = useState(false);

  // Search Order
  const [searchInput, setSearchInput] = useState("");

  const inputChangeHandler = (event) => {
    const input = event.target.value;
    setSearchInput(input);
    searchOrder(input);
  };

  async function confirmationHandler(id) {
    setIsDone(false);
    setIsProcessing(true);
    const { status, error } = await client.put("/pos/update_order_paid", {
      id_order_customer_qr: id,
      status_pemesanan: 2,
    });

    if (status === 200) {
      toast.success("Pembayaran terkonfirmasi");
    } else if (error) {
      toast.error(error);
    }
    setIsProcessing(false);
    setIsDone(true);
    getListOrder(page);
  }

  const handleOpenOrderModal = (idOrder, totalHarga) => {
    setIsOpenOrderModal(true);
    setTotalHarga(totalHarga);
    getDetailOrder(idOrder);
  };

  const handleCloseOrderModal = () => {
    setIsOpenOrderModal(false);
    setDataOrder([]);
    setTotalHarga(0);
  };

  const handleChange = (event, value) => {
    setPage(value);
    getListOrder(value);
  };

  async function getListOrder(page) {
    setIsLoadingNextPage(true);
    const response = await client.getParams("/pos/get_order_waiting_for_paid", {
      page: page,
    });

    if (response.status === 200) {
      setListData(response.data?.data);
      if (
        Number.isInteger(Number(response.data?.row_count)) &&
        Number.isInteger(Number(response.data?.offset))
      ) {
        setPageCount(
          Math.ceil(
            Number(response.data?.row_count) / Number(response.data?.offset)
          )
        );
      }
    } else {
      toast.error(response.error);
    }
    setIsLoadingNextPage(false);
  }

  async function searchOrder(name) {
    const response = await client.getParams("/pos/get_order_waiting_for_paid", {
      search: name,
    });

    if (response.status === 200) {
      setListData(response.data?.data);
      if (
        Number.isInteger(Number(response.data?.row_count)) &&
        Number.isInteger(Number(response.data?.offset))
      ) {
        setPageCount(
          Math.ceil(
            Number(response.data?.row_count) / Number(response.data?.offset)
          )
        );
      }
    } else {
      toast.error(response.error);
    }
    setIsLoadingNextPage(false);
  }

  async function getDetailOrder(idOrder) {
    setIsLoadingDetail(true);
    const response = await client.getParams("/pos/get_order_detail", {
      id_order: idOrder,
    });

    if (response.status === 200) {
      setDataOrder(response.data.data);
    } else {
      toast.error(response.error);
    }
    setIsLoadingDetail(false);
  }

  useEffect(() => {
    getListOrder(1);
  }, []);

  if (isLoadingNextPage) return "Loading...";

  return (
    <Box py={2} px={4}>
      <OrderDetailModal
        handleClose={handleCloseOrderModal}
        isOpen={isOpenOrderModal}
        data={dataOrder}
        totalHarga={totalHarga}
        isLoading={isLoadingDetail}
      />
      <Box pb={3}>
        <Typography component="h1" variant="h4" className="titlePage">
          Daftar Pembayaran Belum Terkonfirmasi
        </Typography>
        <Box mt={2}>
          <OutlinedInput
            id="searchProduct"
            placeholder="Cari order"
            value={searchInput}
            fullWidth
            onChange={inputChangeHandler}
            startAdornment={
              <InputAdornment position="start">
                <Search />
              </InputAdornment>
            }
          />
        </Box>
      </Box>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="flex-end"
        my={3}
        gap={2}
      >
        <Typography>Page</Typography>
        <div>
          <Pagination count={pageCount} page={page} onChange={handleChange} />
        </div>
      </Box>
      <Box id="listProductManagement">
        {isLoadingNextPage ? (
          <Typography>Loading...</Typography>
        ) : (
          listData?.map((item) => (
            <Box mt={2} key={item.nomor_order}>
              {/* <Paper elevation={1}>
               
              </Paper> */}
              <Box p={2} sx={{ borderRadius: "20px" }} bgcolor="white">
                <ConfirmPaymentItem
                  key={item.nomor_order}
                  item={item}
                  handleOpen={handleOpenOrderModal}
                  isDone={isDone}
                  isProcessing={isProcessing}
                  onConfirm={confirmationHandler}
                />
              </Box>
            </Box>
          ))
        )}
      </Box>
      <Box display="flex" justifyContent="flex-end" mt={3}>
        <div className={classes.root}>
          <Pagination count={pageCount} page={page} onChange={handleChange} />
        </div>
      </Box>
    </Box>
  );
};

export default ConfirmPayment;
