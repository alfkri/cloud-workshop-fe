// {
//   "tipe_pembelian" : 3,
//   "service_code":"9000",
//   "id_customer_qr":1,
//   "list_produk"  : [
//       {
//           "id_produk" : 1,
//           "jumlah" : 3
//       },
//       {
//           "id_produk" : 2,
//           "jumlah" : 10
//       }
//   ]
// }

import {
  Badge,
  Box,
  Breadcrumbs,
  ClickAwayListener,
  Divider,
  Grid,
  InputAdornment,
  OutlinedInput,
  Tooltip,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { connect } from "react-redux";
import {
  submitOrderByQR,
  addQRItemOrder,
  incrementQRItemOrderQTY,
  decrementQRItemOrderQTY,
  resetProductsQROrderItem,
} from "../../../store/actions/action";

import Typography from "@material-ui/core/Typography";
import SearchIcon from "@material-ui/icons/Search";

import React from "react";
import ProductItem from "../../../components/QROrder/ProductItem";

import { client } from "../../../lib/client";
import { toast } from "react-toastify";

import { useEffect } from "react";
import { useState } from "react";
import useComponentVisible from "../../../hooks/useComponentVisible";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import UpdateBrandDialog from "../../../components/QROrder/UpdateBrandDialog";
import { ArrowBack } from "@material-ui/icons";
// import { client } from "../../../lib/client";

const useStyles = makeStyles((theme) => ({
  listProduct: {
    // backgroundColor: "red",
    minHeight: "100vh",
  },
  listOrder: {
    backgroundColor: "#2c3248",
    color: "#FFFFFF",
    minHeight: "100vh",
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  dividerBlack: {
    backgroundColor: "black",
  },
  listProductBox: {
    display: "flex",

    "& .titlePage": {
      [theme.breakpoints.down("md")]: {
        marginBottom: "20px",
      },
    },

    [theme.breakpoints.down("md")]: {
      display: "block",
    },
  },
  totalOrderBox: {
    display: "flex",

    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
  totalOrderPrice: {
    marginTop: "0.5rem",
  },
  productCard: {
    [theme.breakpoints.between(0, 300)]: {
      maxWidth: "100%",
      flexBasis: "100%",
    },
  },
  orderInfo: {
    "& *": {
      color: "white",
    },
  },
}));

const OrderPage = ({
  orderList,
  brandInfo,
  totalPrice,
  submitOrder,
  addQRItemOrder,
  incrementQRItemOrderQTY,
  decrementQRItemOrderQTY,
  resetProductsQROrderItem,
}) => {
  const classes = useStyles();
  const { idBrand, qr } = useParams();
  const push = useNavigate();
  const url = useLocation();
  const path = url?.pathname?.split("/").slice(2);
  const [isLoading, setIsLoading] = useState(true);

  const [listDataProduct, setListDataProduct] = useState([]);
  const [namaBrand, setNamaBrand] = useState("");
  const [searchInput, setSearchInput] = useState("");

  // METODE BAYAR
  const [listMetodeBayar, setListMetodeBayar] = useState([]);
  const [metodeBayarDipilih, setMetodeBayarDipilih] = useState(null);

  // Tooltip
  const [open, setOpen] = React.useState(false);

  // UPDATE BRAND HANDLER
  const [openUpdateBrandDialog, setOpenUpdateBrandDialog] = useState(false);
  const [addBrandConfirm, setAddBrandConfirm] = useState(false);
  const [itemToAdd, setItemToAdd] = useState(null);

  const handleClickOpenUpdateBrand = () => {
    setOpenUpdateBrandDialog(true);
  };

  const handleCloseUpdateBrand = (value) => {
    setOpenUpdateBrandDialog(false);
    if (value === 1) {
      resetProductsQROrderItem();
      addNewProduct(itemToAdd, true);
    }
  };

  const handleTooltipClose = () => {
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  const inputChangeHandler = (event) => {
    const input = event.target.value;
    setSearchInput(input);
    searchProducts(input);
    // keyboard.current.setInput(input);
  };

  const metodeBayarChandeHandler = (idMetode) => {
    setMetodeBayarDipilih(idMetode);
  };

  const checkBrandBeforeAdd = (item) => {
    setItemToAdd(item);
    if (idBrand === brandInfo?.idBrand || !brandInfo?.idBrand) {
      addNewProduct(item, true);
    } else {
      handleClickOpenUpdateBrand();
    }
  };

  const addNewProduct = (item, confirmedAction) => {
    if (confirmedAction) {
      addQRItemOrder({
        id_produk: item.id_produk,
        nama_produk: item.nama_produk,
        foto_produk: item.foto_produk,
        harga: item.harga,
        jumlah: 1,
        idBrand: idBrand,
        namaBrand: namaBrand,
      });
    }
  };

  const incrementQtyProduct = (idProduct) => {
    incrementQRItemOrderQTY(idProduct);
  };

  const decrementQtyProduct = (idProduct) => {
    decrementQRItemOrderQTY(idProduct);
  };

  async function getDataProduct() {
    const response = await client.getParams("/pos/get_produk_customer_qr", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setListDataProduct(response.data.data);
    } else {
      toast.error(response.error);
    }
    getDataBrand();
  }

  async function getDataBrand() {
    const response = await client.getParams("/pos/get_brand_customer_qr", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      const theBrand = response.data?.data?.filter(
        (brand) => brand.id_brand.toString() === idBrand.toString()
      );
      setNamaBrand(theBrand[0]?.nama_brand);
    } else {
      toast.error(response.error);
    }
  }

  async function getListMetodePembayaran() {
    const response = await client.get("/pos/get_pembayaran_pos");

    if (response.status === 200) {
      setListMetodeBayar(response.data?.data);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  async function searchProducts(name) {
    const response = await client.getParams("/pos/get_produk_customer_qr", {
      id_brand: idBrand,
      search: name,
    });

    if (response.status === 200) {
      setListDataProduct(response.data.data);
    } else {
      toast.error(response.error);
    }
    // setIsLoading(false);
  }

  useEffect(() => {
    getListMetodePembayaran();
    getDataProduct();
  }, []);

  if (isLoading) {
    return "Loading...";
  }

  return (
    <Box>
      {/* Add Menu Confirmation if The Selected Brand is Different */}
      <UpdateBrandDialog
        open={openUpdateBrandDialog}
        onClose={handleCloseUpdateBrand}
      />
      <Grid container id="kasir">
        <Grid item xs={12} className={classes.listProduct}>
          <Box p={2} display="flex" alignItems="center" sx={{ gap: "1rem" }}>
            <Box onClick={() => push(`/menu/${qr}`)} sx={{ cursor: "pointer" }}>
              <ArrowBack />
            </Box>
            <Typography
              component="h6"
              variant="body1"
              className={classes.boldText}
            >
              Restoran lainnya
            </Typography>
          </Box>
          <Box
            p={2}
            className={classes.listProductBox}
            justifyContent="space-between"
            alignItems="center"
          >
            <Typography component="h1" variant="h4" className="titlePage">
              {namaBrand}
            </Typography>
            <OutlinedInput
              id="searchProduct"
              // ref={buttonTriggerRef}
              placeholder="Cari produk"
              // value={values.amount}
              value={searchInput}
              fullWidth
              onChange={inputChangeHandler}
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              }
            />
          </Box>
          <Divider className={classes.dividerBlack} />
          <Box px={2} pt={2} pb={6} mb={4}>
            <Grid container spacing={2}>
              {listDataProduct.map((item) => (
                <Grid
                  item
                  xs={12}
                  sm={4}
                  md={3}
                  key={item.id_produk}
                  className={classes.productCard}
                >
                  <ProductItem
                    item={item}
                    onAdd={checkBrandBeforeAdd}
                    onInc={incrementQtyProduct}
                    onDec={decrementQtyProduct}
                  />
                </Grid>
              ))}
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

const mapStateToProps = (state) => {
  return {
    orderList: state.qrOrder.order,
    totalPrice: state.qrOrder.totalPrice,
    brandInfo: state.qrOrder.orderBrand,
  };
};
export default connect(mapStateToProps, {
  submitOrderByQR,
  addQRItemOrder,
  incrementQRItemOrderQTY,
  resetProductsQROrderItem,
  decrementQRItemOrderQTY,
})(OrderPage);
