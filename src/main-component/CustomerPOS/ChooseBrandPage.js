import { Box, Button, Grid, Paper, Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
// import { client } from "../../lib/client";
import BrandItemClickable from "../../components/Brand/BrandItemClickable";
import { Pagination } from "@material-ui/lab";
import { client } from "../../lib/client";
import { POS_MEDIA_DEV_URL } from "../../lib/server";
import { POS_MEDIA_URL } from "../../lib/server";

const ChooseBrandPage = () => {
  const url = useLocation();
  const { qr } = useParams();
  const path = url?.pathname?.split("/").slice(2);
  const title =
    path?.toString().charAt(0).toUpperCase() + path?.toString().slice(1);
  const [allBrand, setAllBrand] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingNextPage, setIsLoadingNextPage] = useState(false);
  const [pageCount, setPageCount] = useState(1);
  const [page, setPage] = useState(1);

  const handleChange = (event, value) => {
    setPage(value);
    getAllBrand(value);
  };

  async function getAllBrand(page) {
    setIsLoadingNextPage(true);
    // Get Customer Location
    const responseGetQR = await client.get(`/pos/get_customer_qrcode/${qr}/`);

    if (responseGetQR.status === 200) {
      const location = responseGetQR.data?.data[0]?.id_lokasi;
      if (location) {
        const response = await client.getParams("/pos/get_brand_customer_qr", {
          page: page,
          id_lokasi: location,
        });

        if (response.status === 200) {
          setAllBrand(response.data?.data);
          if (
            Number.isInteger(Number(response.data?.row_count)) &&
            Number.isInteger(Number(response.data?.offset))
          ) {
            setPageCount(
              Math.ceil(
                Number(response.data?.row_count) / Number(response.data?.offset)
              )
            );
          }
        } else {
          toast.error(response.error);
        }
      } else {
        toast.error("QR Code tidak valid atau sudah expired");
      }
    } else {
      toast.error("QR Code tidak valid atau sudah expired");
    }

    setIsLoading(false);
    setIsLoadingNextPage(false);
  }

  useEffect(() => {
    getAllBrand(1);
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Box p={2}>
      <Box pb={3}>
        <Typography component="h1" variant="h4" className="titlePage">
          Bisa Kitchen Order System
        </Typography>
        <Typography component="h6" variant="h6">
          Pilih Restoran
        </Typography>
      </Box>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="flex-end"
        mb={3}
        gap={2}
      >
        <Typography>Page</Typography>
        <div>
          <Pagination count={pageCount} page={page} onChange={handleChange} />
        </div>
      </Box>
      <Grid container spacing={2}>
        {isLoadingNextPage ? (
          <Typography>Loading...</Typography>
        ) : (
          allBrand.map(brand => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={brand.id_brand}>
              <BrandItemClickable item={brand} imgURL={POS_MEDIA_URL} />
            </Grid>
          ))
        )}
      </Grid>
    </Box>
  );
};

export default ChooseBrandPage;
