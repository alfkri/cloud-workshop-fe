import { Box, Button } from "@material-ui/core";
import { useEffect } from "react";
import { useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import ListOrderDialog from "../../../components/QROrder/ListOrderDialog";
import { submitOrderByQR } from "../../../store/actions/action";
import { client } from "../../../lib/client";

const LayoutCustomerPOS = ({ children, submitOrderByQR, orderList }) => {
  const [isLoading, setIsLoading] = useState(true);
  // METODE BAYAR
  const [listMetodeBayar, setListMetodeBayar] = useState([]);
  const [metodeBayarDipilih, setMetodeBayarDipilih] = useState(null);
  const [dataMetodeBayarDipilih, setDataMetodeBayarDipilih] = useState({});
  const [isSubmittingOrder, setIsSubmittingOrder] = useState(false);

  const metodeBayarChandeHandler = (metode) => {
    setDataMetodeBayarDipilih(metode);
    setMetodeBayarDipilih(metode?.id_metode_pembayaran_pos);
  };

  async function submitProductOrder(data) {
    setIsSubmittingOrder(true);
    if (listMetodeBayar.length > 0 && metodeBayarDipilih === null) {
      toast.error("Harap memilih metode pembayaran");
      setIsSubmittingOrder(false);
      return;
    }

    const dataOrder = {
      tipe_pembelian: 3,
      list_produk: orderList,
      service_code: 9000,
      id_qrcode: data?.qr,
    };
    const response = await client.post(
      "/pos/insert_order_customer_qr",
      dataOrder
    );
    if (response.status === 200) {
      toast.success("Order berhasil");
      setMetodeBayarDipilih(null);
      submitOrderByQR();
    } else {
      toast.error("Gagal memproses pesanan");
    }
    setIsSubmittingOrder(false);
  }

  async function getListMetodePembayaran() {
    const response = await client.get("/pos/get_pembayaran_pos");

    if (response.status === 200) {
      setListMetodeBayar(response.data?.data);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getListMetodePembayaran();
  }, []);

  if (isLoading) return "Loading...";

  return (
    <>
      <ListOrderDialog
        onChangeMetodeBayar={metodeBayarChandeHandler}
        listMetodeBayar={listMetodeBayar}
        metodeBayarDipilih={metodeBayarDipilih}
        dataMetodeBayarDipilih={dataMetodeBayarDipilih}
        onSubmit={submitProductOrder}
        isSubmitting={isSubmittingOrder}
      />
      <Box mb={6}>{children}</Box>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    orderList: state.qrOrder.order,
    totalPrice: state.qrOrder.totalPrice,
    brandInfo: state.qrOrder.orderBrand,
  };
};
export default connect(mapStateToProps, {
  submitOrderByQR,
})(LayoutCustomerPOS);
