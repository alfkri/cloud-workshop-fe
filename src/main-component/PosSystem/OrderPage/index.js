import { Box, Grid, Paper, Typography } from "@material-ui/core";

import "../ProductPage/productpage.scss";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import { client } from "../../../lib/client";
import { toast } from "react-toastify";
import OrderItem from "../../../components/OrderManagement/OrderItem";
import { makeStyles } from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const OrderPage = () => {
  const push = useNavigate();
  const classes = useStyles();
  const { idBrand } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingNextPage, setIsLoadingNextPage] = useState(false);
  const [listData, setListData] = useState([]);
  const [namaBrand, setNamaBrand] = useState("");
  const [pageCount, setPageCount] = useState(1);
  const [page, setPage] = useState(1);

  const handleChange = (event, value) => {
    setPage(value);
    getListDataProduct(value);
  };

  async function getListDataProduct(page) {
    setIsLoadingNextPage(true);
    const response = await client.getParams("/pos/get_order", {
      id_brand: idBrand,
      page: page,
    });

    if (response.status === 200) {
      setListData(response.data?.data);
      if (
        Number.isInteger(Number(response.data?.row_count)) &&
        Number.isInteger(Number(response.data?.offset))
      ) {
        setPageCount(
          Math.ceil(
            Number(response.data?.row_count) / Number(response.data?.offset)
          )
        );
      }
    } else {
      toast.error(response.error);
    }
    setIsLoadingNextPage(false);
  }

  async function getDataBrand() {
    const response = await client.getParams("/pos/get_brand", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setNamaBrand(response.data?.data[0]?.nama_brand);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getListDataProduct(1);
    getDataBrand();
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Box py={2} px={4}>
      <Box pb={3}>
        <Typography component="h1" variant="h4" className="titlePage">
          {namaBrand} Order Management
        </Typography>
      </Box>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="flex-end"
        my={3}
        gap={2}
      >
        <Typography>Page</Typography>
        <div>
          <Pagination count={pageCount} page={page} onChange={handleChange} />
        </div>
      </Box>
      <Paper>
        <Box sx={{ backgroundColor: "#2c3248" }} color="text.primary" p={2}>
          <Grid container className="titleList-light" spacing={2}>
            <Grid item xs={4}>
              Nomor Order
            </Grid>
            <Grid item xs={3}>
              Total Harga
            </Grid>
            <Grid item xs={2}>
              Metode Bayar
            </Grid>
            <Grid item xs={3}>
              Waktu Order
            </Grid>
          </Grid>
        </Box>
      </Paper>
      <Box id="listProductManagement">
        {isLoadingNextPage ? (
          <Typography>Loading...</Typography>
        ) : (
          listData?.map((item) => (
            <Box
              onClick={() => push(`detail/${item.id_order}`)}
              mt={2}
              sx={{ cursor: "pointer" }}
              key={item.nomor_order}
            >
              <Paper elevation={1}>
                <Box p={2}>
                  <OrderItem key={item.nomor_order} item={item} />
                </Box>
              </Paper>
            </Box>
          ))
        )}
      </Box>
      <Box display="flex" justifyContent="flex-end" mt={3}>
        <div className={classes.root}>
          <Pagination count={pageCount} page={page} onChange={handleChange} />
        </div>
      </Box>
    </Box>
  );
};

export default OrderPage;
