import { Button, Container, Divider } from "@material-ui/core";

import { Box, TextField, Typography } from "@material-ui/core";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useEffect } from "react";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { client } from "../../../lib/client";

import "./orderpage.scss";

const useStyles = makeStyles(theme => ({
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
}));

const OrderDetail = () => {
  const classes = useStyles();
  const { idOrder, idBrand } = useParams();
  const push = useNavigate();

  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);

  async function getDataProduct() {
    const response = await client.getParams("/pos/get_order_detail", {
      id_order: idOrder,
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setData(response.data.data);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getDataProduct();
  }, []);

  if (isLoading) {
    return "Loading...";
  }

  return (
    <Container maxWidth="sm" id="orderDetails">
      <Box pb={2} pt={4}>
        <Typography variant="h6" component="h2">
          Order Detail
        </Typography>
      </Box>
      <Divider className={classes.divider} />
      {data.map((order, index) => (
        <Box key={order.nama_produk}>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            py={2}
            id="orderItem"
          >
            <Box>
              <Typography
                component="p"
                variant="body1"
                className={classes.boldText}
              >
                {order.nama_produk}
              </Typography>
              <Typography component="p" variant="body2">
                Rp {order.harga_produk_satuan?.toLocaleString()}/unit
              </Typography>
              <Typography component="p" variant="body2">
                Jumlah: {order.jumlah}
              </Typography>
            </Box>
            <Box>
              <Typography component="p" variant="h6">
                Rp {order.harga_produk_total?.toLocaleString()}
              </Typography>
            </Box>
          </Box>
          {index !== order.length - 1 && <Divider />}
        </Box>
      ))}
      <Box mt={4}>
        <Button
          size="large"
          variant="contained"
          fullWidth
          onClick={() => push(`/pos-system/order/${idBrand}`)}
        >
          Kembali
        </Button>
      </Box>
    </Container>
  );
};

export default OrderDetail;
