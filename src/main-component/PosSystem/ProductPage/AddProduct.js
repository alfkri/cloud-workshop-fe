import React, { useRef, useState } from "react";
import Grid from "@material-ui/core/Grid";
import SimpleReactValidator from "simple-react-validator";
import { toast } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useNavigate, useParams } from "react-router-dom";

import { client } from "../../../lib/client";
import { Box, Typography } from "@material-ui/core";

const AddProduct = () => {
  const push = useNavigate();
  const { idBrand } = useParams();

  const [isLoading, setIsLoading] = useState(false);

  // Image Handling
  const [isUploadingImg, setIsUploadingImg] = useState(false);
  const fileInput = useRef(null);
  const [fileName, setFileName] = useState("");
  const [fileNameIsValid, setFileNameIsValid] = useState(true);
  const [fileTypeIsValid, setFileTypeIsValid] = useState(true);
  const [fileSizeIsValid, setFileSizeIsValid] = useState(true);
  const [img64String, setImg64String] = useState("");
  const [img64Error, setImg64Error] = useState(false);

  const [value, setValue] = useState({
    nama_produk: "",
    deskripsi_produk: "",
    harga: "",
  });

  const changeHandler = (e) => {
    setValue({ ...value, [e.target.name]: e.target.value });
    validator.showMessages();
  };

  const [validator] = React.useState(
    new SimpleReactValidator({
      className: "errorMessage",
    })
  );

  // Image Handling
  function convertToBase64(selectedFile) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(selectedFile);

      reader.onload = () => {
        // console.log("called: ", reader);
        setImg64Error(false);
        resolve(reader.result);
      };

      reader.onerror = (error) => {
        setImg64Error(true);
        reject(error);
      };
    });
  }

  // Image Handling
  async function handleFileUpload() {
    setIsUploadingImg(true);
    const listFile = fileInput?.current?.files;
    if (listFile && listFile.length > 0) {
      setFileNameIsValid(true);
      if (Number(listFile[0].size) <= 5000000) {
        setFileSizeIsValid(true);
      } else {
        setFileSizeIsValid(false);
      }
      if (
        listFile[0].type === "image/png" ||
        listFile[0].type === "image/jpg" ||
        listFile[0].type === "image/jpeg"
      ) {
        setFileTypeIsValid(true);
        let base64Img = await convertToBase64(listFile[0]);
        setImg64String(base64Img.split(",")[1]);
      } else {
        setFileTypeIsValid(false);
      }
      setFileName(listFile[0].name);
    } else {
      setFileName("");
      setFileNameIsValid(false);
    }
    setIsUploadingImg(false);
  }

  async function submitForm(e) {
    e.preventDefault();
    setIsLoading(true);

    if (
      validator.allValid() &&
      img64String !== "" &&
      !img64Error &&
      fileTypeIsValid &&
      fileSizeIsValid &&
      fileNameIsValid
    ) {
      const response = await client.post("/pos/insert_produk", {
        nama_produk: value.nama_produk,
        deskripsi_produk: value.deskripsi_produk,
        harga: value.harga,
        foto_produk: img64String,
        id_brand: idBrand,
      });

      if (response.status === 200) {
        toast.success("Produk berhasil ditambahkan");
        push(`/pos-system/produk/${idBrand}`);
      } else {
        toast.error(response.error);
      }
    } else {
      validator.showMessages();
      toast.error("Data tidak valid!");
    }
    setIsLoading(false);
  }

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12} sm={10} md={6} lg={5}>
        <Box className="loginWrapper">
          <Box id="addProductForm" className="loginForm">
            <h2>Tambah Produk</h2>
            <form onSubmit={submitForm}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <TextField
                    className="inputOutline"
                    fullWidth
                    value={value.nama_produk}
                    variant="outlined"
                    name="nama_produk"
                    label="Nama Produk"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onBlur={(e) => changeHandler(e)}
                    onChange={(e) => changeHandler(e)}
                  />
                  {validator.message(
                    "nama_produk",
                    value.nama_produk,
                    "required"
                  )}
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    className="inputOutline"
                    fullWidth
                    value={value.deskripsi_produk}
                    variant="outlined"
                    name="deskripsi_produk"
                    label="Deskripsi Produk"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onBlur={(e) => changeHandler(e)}
                    onChange={(e) => changeHandler(e)}
                  />
                  {validator.message(
                    "deskripsi_produk",
                    value.deskripsi_produk,
                    "required"
                  )}
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    className="inputOutline"
                    fullWidth
                    value={value.harga}
                    variant="outlined"
                    name="harga"
                    label="Harga Produk"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onBlur={(e) => changeHandler(e)}
                    onChange={(e) => changeHandler(e)}
                  />
                  {validator.message(
                    "harga",
                    value.harga,
                    "required|numeric|min:0,num"
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Box mb={2}>
                    <Typography component="p" variant="caption">
                      Foto Produk (.png/.jpg/.jpeg)(max: 5 MB)
                    </Typography>
                  </Box>
                  <Box
                    className="Field"
                    sx={{ position: "relative" }}
                    display="flex"
                    gap={{ xs: 2, sm: 0 }}
                    flexDirection={{ xs: "column-reverse", sm: "row" }}
                    flexWrap={{ xs: "wrap", sm: "nowrap" }}
                    alignItems={{ xs: "flex-start", sm: "center" }}
                  >
                    <Button
                      variant="contained"
                      component="label"
                      className="uploadButton"
                      sx={{
                        paddingTop: { xs: "3px", sm: "7px" },
                        paddingBottom: { xs: "3px", sm: "7px" },
                        marginBottom: { xs: "1rem", sm: 0 },
                        borderRadius: { xs: "5px", sm: "5px 0px 0px 5px" },
                        backgroundColor: "#F3F3F3",
                        color: "black",
                        fontSize: "1rem",
                        boxSizing: "border-box",
                        flexBasis: "content",
                      }}
                      disabled={isUploadingImg}
                    >
                      Choose Photo
                      <input
                        type="file"
                        onChange={handleFileUpload}
                        ref={fileInput}
                        hidden
                      />
                    </Button>
                    <Box
                      sx={{
                        paddingTop: "7px",
                        paddingBottom: "7px",
                        overflowX: "auto",
                      }}
                      flexGrow={1}
                      pl={3}
                      pr={{ xs: 3, sm: 0 }}
                      width={{ xs: "100%", sm: "auto" }}
                    >
                      <Typography>
                        {fileName.length === 0 ? "No File Chosen" : fileName}
                      </Typography>
                    </Box>
                  </Box>
                  {!fileNameIsValid && (
                    <Typography component="p" variant="caption" mt={1}>
                      File foto tidak valid
                    </Typography>
                  )}
                  {!fileTypeIsValid && (
                    <Typography
                      component="p"
                      variant="caption"
                      mt={1}
                      color="error"
                    >
                      File foto harus bertipe png/jpg/jpeg
                    </Typography>
                  )}
                  {!fileSizeIsValid && (
                    <Typography
                      component="p"
                      variant="caption"
                      mt={1}
                      color="error"
                    >
                      Ukuran foto maksimal 5 MB
                    </Typography>
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Grid className="formFooter">
                    {!isLoading && (
                      <Button fullWidth className="cBtnTheme" type="submit">
                        Tambah Produk
                      </Button>
                    )}
                    {isLoading && (
                      <Button fullWidth disabled>
                        <Typography component="p" color="textPrimary">
                          Loading...
                        </Typography>
                      </Button>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </form>
            <div className="shape-img">
              <i className="fi flaticon-honeycomb"></i>
            </div>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};

export default AddProduct;
