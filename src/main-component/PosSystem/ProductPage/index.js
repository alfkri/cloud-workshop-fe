import { Box, Button, Divider, Grid, Typography } from "@material-ui/core";
import ProductItem from "../../../components/ProductManagement/ProductItem";
import posProducts from "../../../api/posProducts";

import "./productpage.scss";
import { Add } from "@material-ui/icons";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import { client } from "../../../lib/client";
import { toast } from "react-toastify";

const ProductPage = () => {
  const push = useNavigate();
  const { idBrand } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [listData, setListData] = useState([]);
  const [namaBrand, setNamaBrand] = useState("");
  const [isDeleting, setIsDeleting] = useState(false);
  const [doneDeleting, setDoneDeleting] = useState(false);

  async function getListDataProduct() {
    const response = await client.getParams("/pos/get_produk", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setListData(response.data?.data);
    } else {
      toast.error(response.error);
    }
    getDataBrand();
  }

  async function getDataBrand() {
    const response = await client.getParams("/pos/get_brand", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setNamaBrand(response.data?.data[0]?.nama_brand);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  async function onDelete(id) {
    setIsDeleting(true);
    const { data, status, error } = await client.put("/pos/delete_produk", {
      id_produk: id,
      is_delete: 1,
    });

    if (status === 200) {
      toast.success("Produk berhasil dihapus");
    } else if (error) {
      toast.error(error);
    }
    setIsDeleting(false);
    setDoneDeleting(true);
    getListDataProduct();
  }

  useEffect(() => {
    getListDataProduct();
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Box py={2} px={4}>
      <Box pb={3}>
        <Typography component="h1" variant="h4" className="titlePage">
          {namaBrand} Product Management
        </Typography>
      </Box>
      <Box pb={4} display="flex" justifyContent="end">
        <Button
          variant="contained"
          color="primary"
          startIcon={<Add />}
          onClick={() => {
            push("tambah");
          }}
        >
          Tambah Produk
        </Button>
      </Box>
      <Grid container className="titleList" spacing={2}>
        <Grid item xs={2} sm={1}>
          Gambar
        </Grid>
        <Grid item xs={3} sm={4}>
          Nama Produk
        </Grid>
        <Grid item xs={3} sm={3}>
          Harga
        </Grid>
        <Grid item xs={4} sm={4}>
          Aksi
        </Grid>
      </Grid>
      <Box my={3}>
        <Divider />
      </Box>
      <Box id="listProductManagement">
        {listData?.map((item) => (
          <ProductItem
            key={item.id_produk}
            item={item}
            onDelete={onDelete}
            isDeleting={isDeleting}
            doneDeleting={doneDeleting}
          />
        ))}
      </Box>
    </Box>
  );
};

export default ProductPage;
