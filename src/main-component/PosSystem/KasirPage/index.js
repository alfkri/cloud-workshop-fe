import {
  Badge,
  Box,
  Breadcrumbs,
  ClickAwayListener,
  Divider,
  Grid,
  InputAdornment,
  OutlinedInput,
  Tooltip,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import SearchIcon from "@material-ui/icons/Search";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import InfoIcon from "@material-ui/icons/Info";

import "./kasir.scss";
import React from "react";
import ProductItem from "../../../components/Kasir/ProductItem";
import { connect } from "react-redux";
import OrderItem from "../../../components/Kasir/OrderItem";
import { client } from "../../../lib/client";
import { toast } from "react-toastify";
import { submitOrder, loadProducts } from "../../../store/actions/action";
import { useEffect } from "react";
import { useState } from "react";
import ScreenKeyboard from "../../../components/Kasir/ScreenKeyboard";
import { POS_MEDIA_URL } from "../../../lib/server";
import useComponentVisible from "../../../hooks/useComponentVisible";
import { Link, useLocation, useParams } from "react-router-dom";

import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  listProduct: {
    // backgroundColor: "red",
    minHeight: "100vh",
  },
  listOrder: {
    backgroundColor: "#2c3248",
    color: "#FFFFFF",
    minHeight: "100vh",
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  dividerBlack: {
    backgroundColor: "black",
  },
  listProductBox: {
    display: "flex",

    "& .titlePage": {
      [theme.breakpoints.down("md")]: {
        marginBottom: "20px",
      },
    },

    [theme.breakpoints.down("md")]: {
      display: "block",
    },
  },
  totalOrderBox: {
    display: "flex",

    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
  totalOrderPrice: {
    marginTop: "0.5rem",
  },
}));

const KasirPage = ({ order, totalPrice, submitOrder, loadProducts }) => {
  const classes = useStyles();
  const { idBrand } = useParams();
  const url = useLocation();
  const path = url?.pathname?.split("/").slice(2);
  const [isLoading, setIsLoading] = useState(true);
  const [isSubmittingOrder, setIsSubmittingOrder] = useState(false);
  const [listDataProduct, setListDataProduct] = useState([]);
  const [namaBrand, setNamaBrand] = useState("");
  const [searchInput, setSearchInput] = useState("");
  const { ref, buttonTriggerRef, isComponentVisible } =
    useComponentVisible(false);

  // METODE BAYAR
  const [listMetodeBayar, setListMetodeBayar] = useState([]);
  const [metodeBayarDipilih, setMetodeBayarDipilih] = useState(null);

  // Tooltip
  const [open, setOpen] = React.useState(false);

  const handleTooltipClose = () => {
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  const inputChangeHandler = (event) => {
    const input = event.target.value;
    setSearchInput(input);
    searchProducts(input);
    // keyboard.current.setInput(input);
  };

  const keyboardChangeHandler = (input) => {
    setSearchInput(input);
    searchProducts(input);
  };

  const metodeBayarChandeHandler = (idMetode) => {
    setMetodeBayarDipilih(idMetode);
  };

  async function submitProductOrder() {
    setIsSubmittingOrder(true);
    if (listMetodeBayar.length > 0 && metodeBayarDipilih === null) {
      toast.error("Harap memilih metode pembayaran");
      setIsSubmittingOrder(false);
      return;
    }
    const response = await client.post("/pos/insert_order", {
      tipe_pembelian: 1,
      list_produk: order,
      id_metode_pembayaran_pos: metodeBayarDipilih,
    });
    if (response.status === 200) {
      toast.success("Order berhasil");
      setMetodeBayarDipilih(null);
      submitOrder();
    } else {
      toast.error(response.error);
    }
    setIsSubmittingOrder(false);
  }

  async function getDataProduct() {
    const response = await client.getParams("/pos/get_produk", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setListDataProduct(response.data.data);
    } else {
      toast.error(response.error);
    }
    getDataBrand();
  }

  async function getDataBrand() {
    const response = await client.getParams("/pos/get_brand", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setNamaBrand(response.data?.data[0]?.nama_brand);
    } else {
      toast.error(response.error);
    }
  }

  async function getListMetodePembayaran() {
    const response = await client.get("/pos/get_pembayaran_pos");

    if (response.status === 200) {
      setListMetodeBayar(response.data?.data);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  async function searchProducts(name) {
    const response = await client.getParams("/pos/get_produk", {
      id_brand: idBrand,
      search: name,
    });

    if (response.status === 200) {
      setListDataProduct(response.data.data);
    } else {
      toast.error(response.error);
    }
    // setIsLoading(false);
  }

  useEffect(() => {
    loadProducts();
    getListMetodePembayaran();
    getDataProduct();
  }, []);

  if (isLoading) {
    return "Loading...";
  }

  return (
    <Box>
      <Grid container id="kasir">
        <Grid item xs={7} className={classes.listProduct}>
          <Box p={2}>
            <Breadcrumbs aria-label="breadcrumb">
              <Typography color="textSecondary" style={{ color: "black" }}>
                pos-system
              </Typography>
              {path.map((sub, ind) => (
                <Link
                  key={sub}
                  color={
                    ind === path.length - 1 ? "textPrimary" : "textSecondary"
                  }
                  to={`/pos-system/${path.slice(0, ind + 1).join("/")}`}
                >
                  {sub}
                </Link>
              ))}
            </Breadcrumbs>
          </Box>
          <Box
            p={2}
            className={classes.listProductBox}
            justifyContent="space-between"
            alignItems="center"
          >
            <Typography component="h1" variant="h4" className="titlePage">
              {namaBrand} Order System
            </Typography>
            <OutlinedInput
              id="searchProduct"
              ref={buttonTriggerRef}
              placeholder="Cari produk"
              // value={values.amount}
              value={searchInput}
              fullWidth
              onChange={inputChangeHandler}
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              }
            />
          </Box>
          <Divider className={classes.dividerBlack} />
          <Box p={2}>
            <Grid container spacing={2}>
              {listDataProduct.map((item) => (
                <Grid item xs={12} sm={6} md={4} key={item.id_produk}>
                  <ProductItem item={item} />
                </Grid>
              ))}
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={5} className={classes.listOrder}>
          <Box p={2} className="orderSection">
            <Box pb={2}>
              <Typography variant="h6" component="h2">
                Order List
              </Typography>
            </Box>
            <Divider className={classes.divider} />
            {order.map((order, index) => (
              <Box key={order.id_produk}>
                <OrderItem
                  item={order}
                  key={order.id_produk + order.nama_produk}
                />
                {index !== order.length - 1 && <Divider />}
              </Box>
            ))}
            <Divider className={classes.divider} />
            <Box py={2} display="flex" alignItems="center" sx={{ gap: "5px" }}>
              <Typography variant="h6" component="h2">
                Metode Pembayaran
              </Typography>
              <ClickAwayListener onClickAway={handleTooltipClose}>
                <div>
                  <Tooltip
                    PopperProps={{
                      disablePortal: true,
                    }}
                    onClose={handleTooltipClose}
                    open={open}
                    disableFocusListener
                    disableHoverListener
                    disableTouchListener
                    title="Atur metode pembayaran di menu Pembayaran"
                  >
                    <Button
                      onClick={handleTooltipOpen}
                      style={{ padding: 0, margin: 0, minWidth: 0 }}
                    >
                      <InfoIcon fontSize="small" color="secondary" />
                    </Button>
                  </Tooltip>
                </div>
              </ClickAwayListener>
            </Box>
            {order.length > 0 && listMetodeBayar.length > 0 && (
              <Box display="flex" sx={{ gap: "10px" }}>
                {listMetodeBayar.map((metode) => (
                  <Box
                    onClick={() =>
                      metodeBayarChandeHandler(metode.id_metode_pembayaran_pos)
                    }
                    key={metode.metode_pembayaran}
                    sx={{ cursor: "pointer" }}
                  >
                    <Badge
                      key={metode.metode_pembayaran}
                      badgeContent={
                        metode.id_metode_pembayaran_pos ===
                          metodeBayarDipilih && (
                          <CheckCircleIcon color="secondary" />
                        )
                      }
                    >
                      <Box
                        sx={{
                          borderRadius: "5px",
                          color: "white",
                          border: `1px solid ${
                            metode.id_metode_pembayaran_pos ===
                            metodeBayarDipilih
                              ? "red"
                              : "white"
                          }`,
                          backgroundColor: "transparent",
                          gap: "5px",
                        }}
                        pr={2}
                        display="flex"
                        alignItems="center"
                      >
                        <img
                          src={`${POS_MEDIA_URL}/foto_pembayaran/${metode.foto_pembayaran}`}
                          alt=""
                          style={{ borderRadius: "5px 0px 0px 5px" }}
                          height="32px"
                          width="auto"
                        />{" "}
                        {metode.metode_pembayaran}
                      </Box>
                    </Badge>
                  </Box>
                ))}
              </Box>
            )}

            <Box
              className={classes.totalOrderBox}
              justifyContent="space-between"
              py={2}
            >
              <Typography
                component="p"
                variant="h6"
                className={classes.boldText}
              >
                Total
              </Typography>
              <Typography
                component="p"
                variant="h6"
                className={clsx(classes.boldText, classes.totalOrderPrice)}
              >
                Rp {totalPrice?.toLocaleString()}
              </Typography>
            </Box>
            <Button
              variant="contained"
              color={order.length === 0 ? "default" : "primary"}
              className={classes.boldText}
              fullWidth
              onClick={submitProductOrder}
              disabled={order.length === 0 || isSubmittingOrder}
            >
              {isSubmittingOrder ? "Submitting..." : "Bayar"}
            </Button>
          </Box>
        </Grid>
      </Grid>
      <Box
        sx={{
          position: "fixed",
          // height: "20vh",
          right: 100,
          left: 100,
          bottom: 0,
          zIndex: 1201,
          // backgroundColor: "red",
        }}
      >
        <Box>
          {isComponentVisible && (
            <Box ref={ref}>
              <ScreenKeyboard
                onInputChange={keyboardChangeHandler}
                input={searchInput}
              />
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  );
};

const mapStateToProps = (state) => {
  return {
    order: state.orderList.order,
    totalPrice: state.orderList.totalPrice,
  };
};
export default connect(mapStateToProps, { submitOrder, loadProducts })(
  KasirPage
);
