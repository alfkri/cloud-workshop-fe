import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import CropFreeIcon from "@material-ui/icons/CropFree";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";

import DesktopWindowsIcon from "@material-ui/icons/DesktopWindows";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import ReceiptIcon from "@material-ui/icons/Receipt";
import AppsIcon from "@material-ui/icons/Apps";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Box, Breadcrumbs } from "@material-ui/core";
import { logout } from "../../../store/actions/action";
import { connect } from "react-redux";
import { useEffect } from "react";

const drawerWidth = 240;

const MENU = [
  {
    id: 0,
    title: "Kasir",
    icon: <DesktopWindowsIcon fontSize="large" />,
    url: "/pos-system/kasir",
  },
  {
    id: 1,
    title: "Produk",
    icon: <FastfoodIcon fontSize="large" />,
    url: "/pos-system/produk",
  },
  {
    id: 2,
    title: "Riwayat Order",
    icon: <ReceiptIcon fontSize="large" />,
    url: "/pos-system/order",
  },
  {
    id: 3,
    title: "Brand",
    icon: <AppsIcon fontSize="large" />,
    url: "/pos-system/brand",
  },
  {
    id: 4,
    title: "Pembayaran",
    icon: <AccountBalanceWalletIcon fontSize="large" />,
    url: "/pos-system/metode-bayar",
  },
  {
    id: 5,
    title: "QR Code Management",
    icon: <CropFreeIcon fontSize="large" />,
    url: "/pos-system/qr-code-management",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "#2c3248",
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButtonAppBar: {
    color: "#2c3248",
  },
  menuButton: {
    color: "#FFFFFF",
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
    backgroundColor: "#2c3248",
    color: "white !important",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: "#2c3248",
    color: "white !important",
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    backgroundColor: "#2c3248",
    color: "white !important",
    [theme.breakpoints.down("xs")]: {
      width: 0,
    },
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    // [theme.breakpoints.down("sm")]: {
    //   display: "none",
    // },
  },
  content: {
    flexGrow: 1,
  },
  icons: {
    color: "white !important",
  },
  iconsActive: {
    color: "#2c3248",
  },
  listItem: {
    marginTop: "1rem",
    marginBottom: "1rem",
  },
  activePage: {
    backgroundColor: "#FFFFFF",
    "&:hover": {
      backgroundColor: "#FFFFFF",
    },
  },
  title: {
    color: "#FFFFFF",
    fontWeight: 700,
  },
}));

const Sidebar = ({ children, logout, auth }) => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [active, setActive] = React.useState(0);
  const push = useNavigate();
  const url = useLocation();
  const path = url?.pathname?.split("/").slice(2);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (!auth.isLoggedIn) {
      push("/pos-system/login");
    }
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="h1"
            className={classes.title}
            noWrap
          >
            Point of Sale System
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbarIcon}>
          {open ? (
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "rtl" ? (
                <ChevronRightIcon className={classes.icons} />
              ) : (
                <ChevronLeftIcon className={classes.icons} />
              )}
            </IconButton>
          ) : (
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <ChevronRightIcon fontSize="large" />
              {/* <MenuIcon fontSize="large" /> */}
            </IconButton>
          )}
        </div>
        <Divider />
        <List>
          {MENU.map((item, index) => (
            <ListItem
              button
              key={item.title}
              className={clsx(classes.listItem, {
                [classes.activePage]: path[0] === item.url.split("/")[2],
              })}
              onClick={() => {
                push(item.url);
                setActive(item.id);
              }}
            >
              <ListItemIcon
                className={clsx({
                  [classes.iconsActive]: path[0] === item.url.split("/")[2],
                  [classes.icons]: path[0] !== item.url.split("/")[2],
                })}
              >
                {item.icon}
              </ListItemIcon>
              <ListItemText
                primary={item.title}
                className={clsx({
                  [classes.iconsActive]: path[0] === item.url.split("/")[2],
                  [classes.icons]: path[0] !== item.url.split("/")[2],
                })}
              />
            </ListItem>
          ))}
          <Box mt={4}>
            <ListItem
              button
              onClick={() => {
                logout();
                push("pos-system/login");
              }}
            >
              <ListItemIcon>
                <ExitToAppIcon className={classes.icons} fontSize="large" />
              </ListItemIcon>
              <ListItemText className={classes.icons} primary="Logout" />
            </ListItem>
          </Box>
        </List>
        {/* <Divider />
        <List>
          {["All mail", "Trash", "Spam"].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List> */}
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {path[0] !== "kasir" && (
          <Box p={2}>
            <Breadcrumbs aria-label="breadcrumb">
              <Typography>pos-system</Typography>
              {path.map((sub, ind) =>
                sub === "update" || sub === "detail" ? (
                  <Typography key={sub}>{sub}</Typography>
                ) : (
                  <Link
                    key={sub}
                    color={
                      ind === path.length - 1 ? "textPrimary" : "textSecondary"
                    }
                    to={`/pos-system/${path.slice(0, ind + 1).join("/")}`}
                  >
                    {sub}
                  </Link>
                )
              )}
            </Breadcrumbs>
          </Box>
        )}
        {children}
      </main>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps, { logout })(Sidebar);
