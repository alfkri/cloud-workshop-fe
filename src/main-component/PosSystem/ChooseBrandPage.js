import { Box, Button, Grid, Paper, Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { client } from "../../lib/client";
import BrandItemClickable from "../../components/Brand/BrandItemClickable";
import { Pagination } from "@material-ui/lab";

const ChooseBrandPage = () => {
  const push = useNavigate();
  const url = useLocation();
  const path = url?.pathname?.split("/").slice(2);
  const title =
    path?.toString().charAt(0).toUpperCase() + path?.toString().slice(1);
  const [allBrand, setAllBrand] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingNextPage, setIsLoadingNextPage] = useState(false);
  const [pageCount, setPageCount] = useState(1);
  const [page, setPage] = useState(1);

  const handleChange = (event, value) => {
    setPage(value);
    getAllBrand(value);
  };

  async function getAllBrand(page) {
    setIsLoadingNextPage(true);
    const response = await client.getParams("/pos/get_brand", {
      page: page,
    });

    if (response.status === 200) {
      setAllBrand(response.data?.data);
      if (
        Number.isInteger(Number(response.data?.row_count)) &&
        Number.isInteger(Number(response.data?.offset))
      ) {
        setPageCount(
          Math.ceil(
            Number(response.data?.row_count) / Number(response.data?.offset)
          )
        );
      }
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
    setIsLoadingNextPage(false);
  }

  useEffect(() => {
    getAllBrand(1);
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Box p={2}>
      <Box
        pb={3}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Box>
          <Typography component="h1" variant="h4" className="titlePage">
            POS {title} Management
          </Typography>
          <Typography component="h6" variant="h6">
            Pilih Brand
          </Typography>
        </Box>
        {title === "Kasir" && (
          <Button
            variant="contained"
            color="primary"
            size="large"
            onClick={() => push("/pos-system/confirm-payment")}
          >
            Konfirmasi Pembayaran
          </Button>
        )}
      </Box>
      {/* <Box my={1} display="flex" justifyContent="flex-end">
        <Button
          variant="contained"
          color="primary"
          onClick={() => push("/confirm-payment")}
        >
          Konfirmasi Pembayaran
        </Button>
      </Box> */}
      <Box
        display="flex"
        alignItems="center"
        justifyContent="flex-end"
        mb={3}
        gap={2}
      >
        <Typography>Page</Typography>
        <div>
          <Pagination count={pageCount} page={page} onChange={handleChange} />
        </div>
      </Box>
      <Grid container spacing={2}>
        {isLoadingNextPage ? (
          <Typography>Loading...</Typography>
        ) : (
          allBrand.map((brand) => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={brand.id_brand}>
              <BrandItemClickable item={brand} />
            </Grid>
          ))
        )}
      </Grid>
    </Box>
  );
};

export default ChooseBrandPage;
