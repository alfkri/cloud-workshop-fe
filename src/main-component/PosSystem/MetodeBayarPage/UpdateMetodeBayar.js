import React, { useState } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import SimpleReactValidator from "simple-react-validator";
import { toast } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { Link, useNavigate, useParams } from "react-router-dom";

import { client } from "../../../lib/client";
import { useEffect } from "react";
import { useRef } from "react";
import { Box, Typography } from "@material-ui/core";

const UpdateMetodePembayaran = () => {
  const push = useNavigate();
  const [isLoading, setIsLoading] = useState(true);
  const { idMetodePembayaran } = useParams();
  // Image Handling
  const [isUploadingImg, setIsUploadingImg] = useState(false);
  const fileInput = useRef(null);
  const [fileName, setFileName] = useState("");
  const [fileNameIsValid, setFileNameIsValid] = useState(true);
  const [fileTypeIsValid, setFileTypeIsValid] = useState(true);
  const [fileSizeIsValid, setFileSizeIsValid] = useState(true);
  const [inputNewImg, setInputNewImg] = useState(false);
  const [img64String, setImg64String] = useState("");
  const [img64Error, setImg64Error] = useState(false);

  const [value, setValue] = useState({
    id_metode_pembayaran_pos: idMetodePembayaran,
    metode_pembayaran: "",
  });

  const changeHandler = (e) => {
    setValue({ ...value, [e.target.name]: e.target.value });
    validator.showMessages();
  };

  const [validator] = React.useState(
    new SimpleReactValidator({
      className: "errorMessage",
    })
  );

  // Image Handling
  function convertToBase64(selectedFile) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(selectedFile);

      reader.onload = () => {
        // console.log("called: ", reader);
        setImg64Error(false);
        resolve(reader.result);
      };

      reader.onerror = (error) => {
        setImg64Error(true);
        reject(error);
      };
    });
  }

  // Image Handling
  async function handleFileUpload() {
    setIsUploadingImg(true);
    setInputNewImg(true);
    const listFile = fileInput?.current?.files;
    if (listFile && listFile.length > 0) {
      setFileNameIsValid(true);
      if (Number(listFile[0].size) <= 5000000) {
        setFileSizeIsValid(true);
      } else {
        setFileSizeIsValid(false);
      }
      if (
        listFile[0].type === "image/png" ||
        listFile[0].type === "image/jpg" ||
        listFile[0].type === "image/jpeg"
      ) {
        setFileTypeIsValid(true);
        let base64Img = await convertToBase64(listFile[0]);
        setImg64String(base64Img.split(",")[1]);
      } else {
        setFileTypeIsValid(false);
      }
      setFileName(listFile[0].name);
    } else {
      setFileName("");
      setFileNameIsValid(false);
    }
    setIsUploadingImg(false);
  }

  async function submitForm(e) {
    e.preventDefault();
    let imgIsValid = true;
    if (inputNewImg) {
      imgIsValid =
        fileNameIsValid &&
        fileSizeIsValid &&
        fileTypeIsValid &&
        img64String !== "" &&
        !img64Error;
    }
    if (validator.allValid() && fileName !== "" && imgIsValid) {
      let data = {
        id_metode_pembayaran_pos: value.id_metode_pembayaran_pos,
        metode_pembayaran: value.metode_pembayaran,
      };

      if (inputNewImg) {
        data = {
          id_metode_pembayaran_pos: value.id_metode_pembayaran_pos,
          metode_pembayaran: value.metode_pembayaran,
        };
      }
      const response = await client.put("/pos/update_pembayaran_pos", data);

      if (response.status === 200) {
        toast.success("Update metode pembayaran berhasil");
        push("/pos-system/metode-bayar");
      } else {
        toast.error(response.error);
      }
    } else {
      validator.showMessages();
      toast.error("Data tidak valid");
    }
  }

  async function getDataMetodeBayar() {
    const response = await client.getParams("/pos/get_pembayaran_pos", {
      id_metode_pembayaran_pos: idMetodePembayaran,
    });

    if (response.status === 200) {
      setValue({
        id_metode_pembayaran_pos:
          response.data?.data[0]?.id_metode_pembayaran_pos,
        metode_pembayaran: response.data?.data[0]?.metode_pembayaran,
      });
      setFileName(response.data?.data[0]?.foto_pembayaran);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getDataMetodeBayar();
  }, []);

  if (isLoading) {
    return "Loading...";
  }

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12} sm={10} md={6} lg={5}>
        <Box className="loginWrapper">
          <Box id="addProductForm" className="loginForm">
            <h4 style={{ textAlign: "center", marginBottom: "1.5rem" }}>
              Update Metode Pembayaran
            </h4>
            <form onSubmit={submitForm}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <TextField
                    className="inputOutline"
                    fullWidth
                    value={value.metode_pembayaran}
                    variant="outlined"
                    name="metode_pembayaran"
                    label="Nama Metode Pembayaran"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onBlur={(e) => changeHandler(e)}
                    onChange={(e) => changeHandler(e)}
                  />
                  {validator.message(
                    "metode_pembayaran",
                    value.metode_pembayaran,
                    "required"
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Box mb={2}>
                    <Typography component="p" variant="caption">
                      Gambar Metode Pembayaran (.png/.jpg/.jpeg)(max: 5 MB)
                    </Typography>
                  </Box>
                  <Box
                    className="Field"
                    sx={{ position: "relative" }}
                    display="flex"
                    gap={{ xs: 2, sm: 0 }}
                    flexDirection={{ xs: "column-reverse", sm: "row" }}
                    flexWrap={{ xs: "wrap", sm: "nowrap" }}
                    alignItems={{ xs: "flex-start", sm: "center" }}
                  >
                    <Button
                      variant="contained"
                      component="label"
                      className="uploadButton"
                      sx={{
                        paddingTop: { xs: "3px", sm: "7px" },
                        paddingBottom: { xs: "3px", sm: "7px" },
                        marginBottom: { xs: "1rem", sm: 0 },
                        borderRadius: { xs: "5px", sm: "5px 0px 0px 5px" },
                        backgroundColor: "#F3F3F3",
                        color: "black",
                        fontSize: "1rem",
                        boxSizing: "border-box",
                        flexBasis: "content",
                      }}
                      disabled={isUploadingImg}
                    >
                      Choose Photo
                      <input
                        type="file"
                        onChange={handleFileUpload}
                        ref={fileInput}
                        hidden
                      />
                    </Button>
                    <Box
                      sx={{
                        paddingTop: "7px",
                        paddingBottom: "7px",
                        overflowX: "auto",
                      }}
                      flexGrow={1}
                      pl={3}
                      pr={{ xs: 3, sm: 0 }}
                      width={{ xs: "100%", sm: "auto" }}
                    >
                      <Typography>
                        {fileName.length === 0 ? "No File Chosen" : fileName}
                      </Typography>
                    </Box>
                  </Box>
                  {!fileNameIsValid && (
                    <Typography component="p" variant="caption" mt={1}>
                      File foto tidak valid
                    </Typography>
                  )}
                  {!fileTypeIsValid && (
                    <Typography
                      component="p"
                      variant="caption"
                      mt={1}
                      color="error"
                    >
                      File foto harus bertipe png/jpg/jpeg
                    </Typography>
                  )}
                  {!fileSizeIsValid && (
                    <Typography
                      component="p"
                      variant="caption"
                      mt={1}
                      color="error"
                    >
                      Ukuran foto maksimal 5 MB
                    </Typography>
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Grid className="formFooter">
                    <Button fullWidth className="cBtnTheme" type="submit">
                      Update
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} style={{ marginTop: "1rem" }}>
                <Grid>
                  <Button
                    fullWidth
                    variant="outlined"
                    onClick={() => push("/pos-system/metode-bayar")}
                  >
                    Batal
                  </Button>
                </Grid>
              </Grid>
            </form>
            <div className="shape-img">
              <i className="fi flaticon-honeycomb"></i>
            </div>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};

export default UpdateMetodePembayaran;
