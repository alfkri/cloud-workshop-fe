import { Box, Button, Grid, Paper, Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { Pagination } from "@material-ui/lab";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import MetodeBayarItem from "../../../components/MetodeBayar/MetodeBayarItem";
import { client } from "../../../lib/client";

const MetodeBayarPage = () => {
  const push = useNavigate();
  const [listMetodeBayar, setListMetodeBayar] = useState([]);
  const [isLoadingNextPage, setIsLoadingNextPage] = useState(false);
  const [pageCount, setPageCount] = useState(1);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(true);
  const [isDeleting, setIsDeleting] = useState(false);
  const [doneDeleting, setDoneDeleting] = useState(false);

  const handleChange = (event, value) => {
    setPage(value);
    getAllMetodeBayar(value);
  };

  async function getAllMetodeBayar(page) {
    setIsLoadingNextPage(true);
    const response = await client.getParams("/pos/get_pembayaran_pos", {
      page: page,
    });

    if (response.status === 200) {
      setListMetodeBayar(response.data?.data);
      if (
        Number.isInteger(Number(response.data?.row_count)) &&
        Number.isInteger(Number(response.data?.offset))
      ) {
        setPageCount(
          Math.ceil(
            Number(response.data?.row_count) / Number(response.data?.offset)
          )
        );
      }
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
    setIsLoadingNextPage(false);
  }

  async function onDelete(id) {
    setIsDeleting(true);
    const { data, status, error } = await client.put(
      "/pos/update_pembayaran_pos",
      {
        id_metode_pembayaran_pos: id,
        is_delete: 1,
      }
    );

    if (status === 200) {
      toast.success("Metode pembayaran berhasil dihapus");
    } else if (error) {
      toast.error(error);
    }
    setIsDeleting(false);
    setDoneDeleting(true);
    getAllMetodeBayar();
  }

  useEffect(() => {
    getAllMetodeBayar(1);
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Box p={2} px={4}>
      <Box pb={3}>
        <Typography component="h1" variant="h4" className="titlePage">
          POS Metode Pembayaran Management
        </Typography>
      </Box>
      <Box pb={4} display="flex" justifyContent="end">
        <Button
          variant="contained"
          color="primary"
          startIcon={<Add />}
          onClick={() => {
            push("/pos-system/metode-bayar/tambah");
          }}
        >
          Tambah Metode Pembayaran
        </Button>
      </Box>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="flex-end"
        mb={3}
        gap={2}
      >
        <Typography>Page</Typography>
        <div>
          <Pagination count={pageCount} page={page} onChange={handleChange} />
        </div>
      </Box>
      <Grid container spacing={2}>
        {isLoadingNextPage ? (
          <Typography>Loading...</Typography>
        ) : (
          listMetodeBayar.map((metode) => (
            <Grid item xs={12} md={4} key={metode.metode_pembayaran}>
              <MetodeBayarItem
                item={metode}
                onDelete={onDelete}
                isDeleting={isDeleting}
                doneDeleting={doneDeleting}
              />
            </Grid>
          ))
        )}
      </Grid>
    </Box>
  );
};

export default MetodeBayarPage;
