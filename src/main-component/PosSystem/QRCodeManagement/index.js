import React, { useRef, useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import SimpleReactValidator from "simple-react-validator";
import { toast } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useNavigate, useParams } from "react-router-dom";
import { connect } from "react-redux";
import { submitQR, loadQR } from "../../../store/actions/action";
import QRCodeItem from "../../../components/QRCode/QRCodeItem";

import { client } from "../../../lib/client";
import { Box, Typography, Divider } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import clsx from "clsx";
import QRCodeModal from "../../../components/QRModal/QRCodeModal";

const useStyles = makeStyles(theme => ({
  listOrder: {
    backgroundColor: "#2c3248",
    color: "#FFFFFF",
    minHeight: "100vh",
  },
  divider: {
    height: "2px !important",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  dividerBlack: {
    backgroundColor: "black",
  },
  listProductBox: {
    display: "flex",

    "& .titlePage": {
      [theme.breakpoints.down("md")]: {
        marginBottom: "20px",
      },
    },

    [theme.breakpoints.down("md")]: {
      display: "block",
    },
  },
  totalOrderBox: {
    display: "flex",

    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
  totalOrderPrice: {
    marginTop: "0.5rem",
  },
}));

const QRPage = () => {
  const push = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const [listData, setListData] = useState([]);

  // const { ref, buttonTriggerRef, isComponentVisible } =
  //   useComponentVisible(false);

  // POST Order
  const [isOpenQRModal, setIsOpenQRModal] = useState(false);
  const [dataQR, setDataQR] = useState([]);

  const [value, setValue] = useState({
    id_lokasi: "",
    nama: "",
    no_telpon: "",
    email: "",
    no_table: "",
  });

  const changeHandler = e => {
    setValue({ ...value, [e.target.name]: e.target.value });
    validator.showMessages();
  };

  const [validator] = React.useState(
    new SimpleReactValidator({
      className: "errorMessage",
    })
  );

  async function submitForm(e) {
    e.preventDefault();
    setIsLoading(true);

    if (validator.allValid()) {
      const response = await client.post("/pos/insert_customer_qrcode", {
        nama: value.nama,
        no_telpon: value.no_telpon,
        email: value.email,
        no_table: value.no_table,
      });

      if (response.status === 200) {
        getQRCode(response.data.description.id_qrcode);
        toast.success("QR Code berhasil ditambahkan");
      } else {
        toast.error(response.error);
      }
    } else {
      validator.showMessages();
      toast.error("Data tidak valid!");
    }
    setIsLoading(false);
  }

  async function getQRCode(id_qrcode) {
    const response = await client.getParams(
      `/pos/get_customer_qrcode/${id_qrcode}/`
    );

    if (response.status === 200) {
      setDataQR(response.data.data);
      // setIsOpenQRModal(true);
    } else {
      toast.error(response.error);
    }
  }

  const handleCloseQRModal = () => {
    setIsOpenQRModal(false);
  };

  useEffect(() => {
    // getQRCode();
  });

  return (
    <Box>
      <QRCodeModal
        handleClose={handleCloseQRModal}
        isOpen={isOpenQRModal}
        data={dataQR}
      />
      <Grid container justifyContent="center">
        <Grid item xs={12} sm={10} md={6} lg={6}>
          <Box className="loginWrapper">
            <Box id="addProductForm" className="loginForm">
              <h2>Tambah QR Code</h2>
              <form onSubmit={submitForm}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      className="inputOutline"
                      fullWidth
                      value={value.nama}
                      variant="outlined"
                      name="nama"
                      label="Nama"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onBlur={e => changeHandler(e)}
                      onChange={e => changeHandler(e)}
                    />
                    {validator.message("nama", value.nama, "required")}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      className="inputOutline"
                      fullWidth
                      value={value.no_telpon}
                      variant="outlined"
                      name="no_telpon"
                      label="No Telepon"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onBlur={e => changeHandler(e)}
                      onChange={e => changeHandler(e)}
                    />
                    {validator.message(
                      "no_telpon",
                      value.no_telpon,
                      "required|numeric|min:0,num"
                    )}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      className="inputOutline"
                      fullWidth
                      value={value.email}
                      variant="outlined"
                      name="email"
                      label="Email"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onBlur={e => changeHandler(e)}
                      onChange={e => changeHandler(e)}
                    />
                    {validator.message("email", value.email, "required")}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      className="inputOutline"
                      fullWidth
                      value={value.no_table}
                      variant="outlined"
                      name="no_table"
                      label="No Meja"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onBlur={e => changeHandler(e)}
                      onChange={e => changeHandler(e)}
                    />
                    {validator.message(
                      "no_table",
                      value.no_table,
                      "required|numeric|min:0,num"
                    )}
                  </Grid>
                  <Grid item xs={12}>
                    <Grid className="formFooter">
                      {!isLoading && (
                        <Button fullWidth className="cBtnTheme" type="submit">
                          Buat QR Code
                        </Button>
                      )}
                      {isLoading && (
                        <Button fullWidth disabled>
                          <Typography component="p" color="textPrimary">
                            Loading...
                          </Typography>
                        </Button>
                      )}
                    </Grid>
                  </Grid>
                </Grid>
              </form>
              <div className="shape-img">
                <i className="fi flaticon-honeycomb"></i>
              </div>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={10} md={6} lg={6}>
          <Box py={5} px={4}>
            {/* <Box pb={3}>
              <Typography
                component="h1"
                variant="h4"
                className="titlePage text-center"
              >
                Scan Me!
              </Typography>
            </Box> */}
            <Box pb={4} display="flex" justifyContent="center"></Box>
            <Grid
              container
              className="titleList"
              justifyContent="center"
              spacing={2}
            >
              <Grid item xs={10} sm={10}>
                {/* <h3 className="text-center">Scan Me!</h3> */}
              </Grid>
              {/* <Grid item xs={3} sm={4}>
                Nama
              </Grid> */}
            </Grid>
            <Box my={3}>
              <Divider />
            </Box>
            <Box
              id="addProductForm"
              className="loginForm"
              justifyContent="center"
            >
              {dataQR?.map(item => (
                <QRCodeItem key={item.id_qrcode} item={item} />
              ))}
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default QRPage;
