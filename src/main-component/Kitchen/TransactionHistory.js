import { Box, Typography } from "@material-ui/core";
import { Fragment, useState, useEffect } from "react";
import Logo from "../../images/logo2.png";
import Navbar from "../../components/Navbar";
import PageTitle from "../../components/pagetitle/PageTitle";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import ItemCard from "../../components/Kitchen/TransactionHistory/ItemCard";
import { client } from "../../lib/client";
import { toast } from "react-toastify";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";

const TransactionHistory = ({ auth }) => {
  const push = useNavigate();
  const [isLoading, setIsLoading] = useState(true);
  const [transactions, setTransactions] = useState([]);

  async function getTransactionData() {
    const {
      data: transactionData,
      error,
      status,
    } = await client.get("/dapur/get_riwayat_transaksi");
    if (status === 200) {
      setTransactions(transactionData.data);
    } else {
      toast.error(error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    if (auth.isLoggedIn) {
      getTransactionData();
    } else {
      push("/login");
    }
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle
        pageTitle={"Transaction History"}
        pagesub={"Transaction History"}
      />
      <Box p={4}>
        <Box>
          <Typography component="h1" variant="h5">
            Riwayat Transaksi
          </Typography>
        </Box>
        {transactions.map(item => (
          <Box mb={2} key={item.nomor_invoice}>
            <ItemCard data={item} />
          </Box>
        ))}
      </Box>
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps)(TransactionHistory);
