import React, { Fragment } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import Navbar from "../../components/Navbar";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo2.png";
import Alat from "../../components/Alat/Alat";

const AlatPage = () => {
  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle pageTitle={"Sewa Alat"} pagesub={" Produk / Alat"} />
      <Alat />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

export default AlatPage;
