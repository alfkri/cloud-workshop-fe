import React, { Fragment } from "react";
import BlogSection from "../../components/BlogSection";
import Footer from "../../components/footer";
import FunFact from "../../components/FunFact";
import Hero from "../../components/hero";
import Navbar from "../../components/Navbar";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo.png";
import WhyChoose from "../ServiceSinglePage/WhyChoose";
import Shape1 from "../../images/nshape1.png";
import Shape2 from "../../images/nshape2.png";

import DrMekanik from "../../images/brand/logo_drmekanik.png";
import Gramatikal from "../../images/brand/logo_gramatikal.png";
import Endog from "../../images/brand/endog.png";
import ChickenParty from "../../images/brand/chicken_party.png";
import Karee from "../../images/brand/karee.png";
import Kava1 from "../../images/brand/kava.png";
import PamelaBakery from "../../images/brand/pamela_bakery.png";
import Tandus from "../../images/brand/tandus.png";

import { Box } from "@material-ui/core";

const HomePage = () => {
  return (
    <Fragment>
      <Navbar topbarBlock={"wpo-header-style-2"} Logo={Logo} />
      <Hero />
      {/* <section
        className="wpo-fun-fact-section-s2 brand-section section-padding"
        style={{ background: "#f3f8fc" }}
      >
        <h1>
          Daftar <span>Brand</span>
        </h1>
        <Box
          display="flex"
          flexWrap="wrap"
          gap={3}
          justifyContent="space-around"
          mt={5}
        >
          <img className="mb-2" src={DrMekanik} alt="" />
          <img className="mb-2" src={Gramatikal} alt="" />
          <img className="mb-2" src={Karee} alt="" />
          <img className="mb-2" src={Kava1} alt="" />
          <img className="mb-2" src={PamelaBakery} alt="" />
          <img className="mb-2" src={Tandus} alt="" />
        </Box>
      </section> */}
      <FunFact fClass={"wpo-fun-fact-section-s2 section-padding"} />
      {/* <section className="service-single-section section-padding wpo-newslatter-section">
        <div className="container">
          <div className="row">
            <div className="col col-12">
              <div className="service-single-content">
                <WhyChoose />
              </div>
            </div>
          </div>
        </div>
        <div className="n-shape">
          <img src={Shape1} alt="" />
        </div>
        <div className="n-shape2">
          <img src={Shape2} alt="" />
        </div>
      </section> */}
      <BlogSection />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

export default HomePage;
