import React from "react";
import { Routes, Route } from "react-router-dom";
import Homepage from "../HomePage";
import AboutPage from "../AboutPage/AboutPage";
import SparepartPage from "../SparepartPage/SparepartPage";
import AlatPage from "../AlatPage/AlatPage";
import BengkelPage from "../BengkelPage/BengkelPage";
import ProdukPage from "../ProdukPage/ProdukPage";

import CheckoutPage from "../CheckoutPage";
import RoomSinglePage from "../RoomSinglePage";
import LoginPage from "../LoginPage";
import SignUpPage from "../SignUpPage";

import TransactionHistory from "../Kitchen/TransactionHistory";
import KioskPosRoute from "./kioskPos";
import PosSystemRoute from "./posSystem";
import KDSSystemRoute from "./kdsSystem";
import OrderPage from "../CustomerPOS/OrderPage";
import ChooseBrandPage from "../PosSystem/ChooseBrandPage";
import LayoutCustomerPOS from "../CustomerPOS/LayoutCustomerPOS";
import CustomerPosRoute from "./customerPos";
import ServicePage from "../ServicePage/ServicePage";

const AllRoute = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="home" element={<Homepage />} />
        <Route path="about" element={<AboutPage />} />
        <Route path="produk/sparepart" element={<SparepartPage />} />
        <Route path="produk/alat" element={<AlatPage />} />
        <Route path="produk/bengkel" element={<BengkelPage />} />
        <Route path="produk/service" element={<ServicePage />} />
        <Route path="produk" element={<ProdukPage />} />
        <Route path="kitchen/:id/:type" element={<RoomSinglePage />} />
        <Route path="login" element={<LoginPage />} />
        <Route path="register" element={<SignUpPage />} />
        <Route path="checkout/:idHarga" element={<CheckoutPage />} />
        <Route path="user/transaction" element={<TransactionHistory />} />
      </Routes>
      <KioskPosRoute />
      <PosSystemRoute />
      <KDSSystemRoute />
      <CustomerPosRoute />
    </>
  );
};

export default AllRoute;
