import { Route, Routes, useLocation } from "react-router-dom";
import OrderDetailKDS from "../KDSSystem/OrderDetail";
import LoginKDS from "../KDSSystem/LoginKDS";
import ListDisplay from "../KDSSystem/ListDisplay";
import MenuDisplay from "../KDSSystem/MenuDisplay";

const KDSSystemRoute = () => {
  return (
    <Routes>
      <Route path="kds-system/login" element={<LoginKDS />} />
      <Route path="kds-system/list-display" element={<ListDisplay />} />
      <Route
        path="kds-system/menu-display/:id/:namaDisplay"
        element={<MenuDisplay />}
      />
      <Route path="kds-system/order-detail/:id" element={<OrderDetailKDS />} />
    </Routes>
  );
};

export default KDSSystemRoute;
