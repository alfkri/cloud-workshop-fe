import { Route, Routes, useLocation } from "react-router-dom";
import ChooseBrandPage from "../CustomerPOS/ChooseBrandPage";
import LayoutCustomerPOS from "../CustomerPOS/LayoutCustomerPOS";
import OrderPage from "../CustomerPOS/OrderPage";

const CustomerPosRoute = () => {
  const url = useLocation();
  const path = url.pathname.split("/");
  return (
    <>
      {path[1] === "menu" && (
        <Routes>
          <Route
            path="menu/:qr"
            element={
              <LayoutCustomerPOS>
                <ChooseBrandPage />
              </LayoutCustomerPOS>
            }
          />
          <Route
            path="menu/:qr/:idBrand"
            element={
              <LayoutCustomerPOS>
                <OrderPage />
              </LayoutCustomerPOS>
            }
          />
        </Routes>
      )}
    </>
  );
};

export default CustomerPosRoute;
