import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import ChooseBrandPage from "../KioskSystem/ChooseBrandPage";
import Kasir from "../KioskSystem/Kasir";
import LoginPage from "../KioskSystem/LoginPage";
import SystemLayout from "../KioskSystem/SystemLayout";

const KioskPosRoute = () => {
  const url = useLocation();
  const path = url.pathname.split("/");
  return (
    <>
      {path[1] === "kiosk-pos-system" && path[2] !== "login" ? (
        <SystemLayout>
          <Routes>
            <Route
              path="kiosk-pos-system/kasir"
              element={<ChooseBrandPage />}
            />
            <Route path="kiosk-pos-system/kasir/:idBrand" element={<Kasir />} />
            <Route
              path="kiosk-pos-system"
              element={<Navigate replace to="/kiosk-pos-system/kasir" />}
            />
          </Routes>
        </SystemLayout>
      ) : (
        <Routes>
          <Route path="/kiosk-pos-system/login" element={<LoginPage />} />
        </Routes>
      )}
    </>
  );
};

export default KioskPosRoute;
