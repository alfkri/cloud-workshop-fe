import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import PosLoginPage from "../PosSystem/PosLoginPage";
import KasirPage from "../PosSystem/KasirPage";
import ProductPage from "../PosSystem/ProductPage";
import UpdateProduct from "../PosSystem/ProductPage/UpdateProduct";
import AddProduct from "../PosSystem/ProductPage/AddProduct";
import OrderPage from "../PosSystem/OrderPage";
import OrderDetail from "../PosSystem/OrderPage/OrderDetail";
import BrandPage from "../PosSystem/BrandPage";
import UpdateBrand from "../PosSystem/BrandPage/UpdateBrand";
import AddBrand from "../PosSystem/BrandPage/AddBrand";
import ChooseBrandPage from "../PosSystem/ChooseBrandPage";
import LayoutPosSystem from "../PosSystem/LayoutPosSystem";
import MetodeBayarPage from "../PosSystem/MetodeBayarPage";
import AddMetodeBayar from "../PosSystem/MetodeBayarPage/AddMetodeBayar";
import UpdateMetodePembayaran from "../PosSystem/MetodeBayarPage/UpdateMetodeBayar";

import ConfirmPayment from "../CustomerPOS/ConfirmPayment";
import QRPage from "../PosSystem/QRCodeManagement";

const PosSystemRoute = () => {
  const url = useLocation();
  const path = url.pathname.split("/");
  return (
    <>
      {path[1] === "pos-system" && path[2] !== "login" ? (
        <LayoutPosSystem>
          <Routes>
            <Route path="pos-system/kasir" element={<ChooseBrandPage />} />
            <Route path="pos-system/kasir/:idBrand" element={<KasirPage />} />
            <Route path="pos-system/brand" element={<BrandPage />} />
            <Route path="pos-system/brand/tambah" element={<AddBrand />} />
            <Route
              path="pos-system/brand/update"
              element={<Navigate replace to="/pos-system/brand" />}
            />
            <Route
              path="pos-system/brand/update/:idBrand"
              element={<UpdateBrand />}
            />
            <Route path="pos-system/produk" element={<ChooseBrandPage />} />
            <Route
              path="pos-system/produk/:idBrand"
              element={<ProductPage />}
            />
            <Route
              path="pos-system/produk/:idBrand/tambah"
              element={<AddProduct />}
            />
            <Route
              path="pos-system/produk/:idBrand/update"
              element={<Navigate replace to="/pos-system/produk/:idBrand" />}
            />
            <Route
              path="pos-system/produk/:idBrand/update/:idProduk"
              element={<UpdateProduct />}
            />
            <Route path="pos-system/order" element={<ChooseBrandPage />} />
            <Route path="pos-system/order/:idBrand" element={<OrderPage />} />
            <Route
              path="pos-system/order/:idBrand/detail/:idOrder"
              element={<OrderDetail />}
            />
            <Route
              path="pos-system/metode-bayar"
              element={<MetodeBayarPage />}
            />
            <Route
              path="pos-system/metode-bayar/tambah"
              element={<AddMetodeBayar />}
            />
            <Route
              path="pos-system/metode-bayar/update/:idMetodePembayaran"
              element={<UpdateMetodePembayaran />}
            />
            <Route
              path="pos-system/confirm-payment"
              element={<ConfirmPayment />}
            />
            <Route path="pos-system/qr-code-management" element={<QRPage />} />

           
          </Routes>
        </LayoutPosSystem>
      ) : (
        <Routes>
          <Route path="pos-system/login" element={<PosLoginPage />} />
        </Routes>
      )}
    </>
  );
};

export default PosSystemRoute;
