import React, { Fragment } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import Navbar from "../../components/Navbar";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo2.png";
import Produk from "../../components/Produk/Produk";

const ProdukPage = () => {
  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle pageTitle={"Produk Kami"} pagesub={" Produk"} />
      <Produk />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

export default ProdukPage;
