import React, { Fragment } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import Navbar from "../../components/Navbar";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo2.png";
import About2 from "../../components/about2/about2";
import RoomDetails from "../RoomSinglePage/RoomDetails";

const AboutPage = () => {
  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle pageTitle={"Tentang Kami"} pagesub={"About"} />
      <About2 />
      {/* <RoomDetails /> */}
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

export default AboutPage;
