import { Box, Button, Divider, Grid, Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import React, { Fragment } from "react";
import { useNavigate } from "react-router-dom";
import ListDisplay from "../../../components/ListDisplay";
import Navbar from "../../../components/Navbar1";
import SectionTitle from "../../../components/SectionTitle";

const HomePage3 = () => {
  const push = useNavigate();

  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-4"} />
      <div className="pt-5">
        <div className="container pt-5">
          <SectionTitle MainTitle={"List Display"} className="pb-0" />
          <ListDisplay lClass={"section-padding"} />
          {/* <ListOrder lClass={"section-padding"} /> */}
        </div>
      </div>
    </Fragment>
  );
};

export default HomePage3;
