import React, { Fragment } from "react";
// import PageTitle from "../../components/pagetitle/PageTitle";
import SectionTitle from "../../../components/SectionTitle";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { toast } from "react-toastify";
import Navbar from "../../../components/Navbar1";
import { client } from "../../../lib/client";
import { Box, Button, Divider, Grid, Typography } from "@material-ui/core";
import { ArrowLeft } from "@material-ui/icons";
import { POS_MEDIA_URL } from "../../../lib/server";

const OrderDetail = () => {
  const { id } = useParams();
  const push = useNavigate();

  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);

  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  async function getDetailOrder() {
    const response = await client.getParams("/pos/get_order_detail", {
      id_order: id,
    });

    if (response.status === 200) {
      setData(response.data.data);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getDetailOrder();
  }, []);

  setTimeout(function () {
    window.location.reload();
  }, 5000);

  if (isLoading) {
    return "Loading..";
  }

  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-4"} />
      <div className="pt-5">
        <div className="container pt-5">
          {/* <Button
            variant="contained"
            color="primary"
            startIcon={<ArrowLeft />}
            onClick={() => {
              push("/kds-system/menu-display");
            }}
          >
            Back
          </Button> */}
          <SectionTitle MainTitle={"Detail Order"} link={"/location"} />
          <div className="destination-wrap">
            <div className="row ">
              {data.map((order, index) => (
                <div className="col-lg-8 col-md-6 col-12 " key={order.id_order}>
                  <div className="destination-item shadow">
                    <div className="destination-img">
                      <img
                        src={`${POS_MEDIA_URL}/foto_produk/${order.foto_produk}`}
                        alt=""
                      />
                    </div>
                    <div className="destination-content mx-3 my-3 pb-1">
                      <h3>
                        {/* <Link
                          onClick={ClickHandler}
                          to={`/order-detail/${order.id_order}`}
                        > */}
                        {order.nama_produk}
                        {/* </Link> */}
                      </h3>
                      <div className="destination-bottom">
                        <h6>
                          Jumlah: <strong>{order.jumlah}</strong>
                        </h6>
                        <p>
                          Harga Produk: Rp{" "}
                          <strong>
                            {order.harga_produk_satuan?.toLocaleString()}
                          </strong>
                        </p>
                        <p>
                          Total Harga Produk: Rp{" "}
                          <strong>
                            {order.harga_produk_total?.toLocaleString()}
                          </strong>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default OrderDetail;
