import React, { Fragment, useRef } from "react";
// import PageTitle from "../../../components/pagetitle";
import SectionTitle from "../../../components/SectionTitle";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { toast } from "react-toastify";
import Navbar from "../../../components/Navbar1";
import { client } from "../../../lib/client";
import { POS_MEDIA_URL } from "../../../lib/server";
import NotificationSound from "./notification.mp3";

const MenuDisplay = () => {
  const push = useNavigate();
  const { id, namaDisplay } = useParams();

  const ref = useRef();
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [listData, setListData] = useState([]);
  const [value, setValue] = useState({
    id_order: "",
    status_order: "",
  });

  const audioPlayer = useRef(null);

  function playAudio() {
    audioPlayer.current.play();
  }

  const changeHandler = (e) => {
    setValue({ ...value, [e.target.name]: e.target.value });
  };

  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  async function submitChange(e, idOrder) {
    e.preventDefault();
    const response = await client.put("/pos/update_order", {
      // id_order: value.id_order,
      // status_order: value.status_order,
      id_order: idOrder,
      status_order: 2,
    });

    if (response.status === 200) {
      // toast.success("update status order berhasil");
      getDataOrder();
    } else {
      toast.error(response.error);
    }
  }

  // Tambah parameter idOrder di sini
  async function submitReady(e, idOrder) {
    e.preventDefault();
    const response = await client.put("/pos/update_order", {
      // id_order: value.id_order,
      // status_order: value.status_order,
      id_order: idOrder,
      status_order: 3,
    });

    if (response.status === 200) {
      // toast.success("update status order berhasil");
      // push(window.location.reload());
      getDataOrder();
    } else {
      toast.error(response.error);
    }
  }
  async function submitFinish(e, idOrder) {
    e.preventDefault();
    const response = await client.put("/pos/update_order", {
      // id_order: value.id_order,
      // status_order: value.status_order,
      id_order: idOrder,
      status_order: 4,
    });

    if (response.status === 200) {
      getDataOrder();
    } else {
      toast.error(response.error);
    }
  }
  async function getListDisplay() {
    const response = await client.get("/pos/get_display", {
      id_display: id,
    });
    if (response.status === 200) {
      if (response.status === 200) {
        setListData(response.data.data);
      } else {
        toast.error(response.error);
      }
      setIsLoading(false);
    }
  }

  async function getListDisplay() {
    const response = await client.get("/pos/get_display", {
      id_display: id,
      nama_display: namaDisplay,
    });
    if (response.status === 200) {
      if (response.status === 200) {
        setListData(response.data.data);
      } else {
        toast.error(response.error);
      }
      setIsLoading(false);
    }
  }

  async function getDataOrder() {
    const response = await client.getParams(
      "/pos/get_order_for_display_complete",
      {
        id_display: id,
      }
    );

    if (response.status === 200) {
      setData(response.data.data);
      setValue({
        id_display: response.data?.data[0]?.id_display,
        id_order: response.data?.data[0]?.id_order,
        status_order: response.data?.data[0]?.status_order,
        nomor_order: response.data?.data[0]?.nomor_order,
        waktu_order: response.data?.data[0]?.waktu_order,
      });
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  async function getDataOrderNew() {
    const response = await client.getParams(
      "/pos/get_order_for_display_complete",
      {
        id_display: id,
      }
    );

    if (response.status === 200) {
      setData(response.data.data);
      setValue({
        id_display: response.data?.data[0]?.id_display,
        id_order: response.data?.data[0]?.id_order,
        status_order: response.data?.data[0]?.status_order,
        nomor_order: response.data?.data[0]?.nomor_order,
        waktu_order: response.data?.data[0]?.waktu_order,
      });
      if (response.data.data.length > data.length) {
        playAudio();
        toast.success(
          `Ada ${response.data.data.length - data.length} Order Baru`
        );
      }
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getListDisplay();
    getDataOrder();
    // getDetailOrder();
  }, []);

  // Refetch order data every 10 sec
  useEffect(() => {
    ref.current = setInterval(getDataOrderNew, 10000);

    return () => {
      if (ref.current) {
        clearInterval(ref.current);
      }
    };
  });

  // setTimeout(function () {
  //   // window.location.reload();
  //   getDataOrderNew();
  // }, 10000);
  if (isLoading) {
    return "Loading..";
  }

  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-4"} />
      <div className="pt-5">
        <div className="container pt-5">
          <SectionTitle
            MainTitle={"List Order - " + namaDisplay}
            link={"/location"}
          />
          <div className={`wpo-destination-area`}>
            <div className="container mt-3">
              <div className="destination-wrap">
                <div className="row">
                  <audio ref={audioPlayer} src={NotificationSound} />
                  {data.map((display, index) => (
                    <div
                      className="col-lg-12 col-md-12 col-12"
                      key={display.id_display}
                    >
                      <div className="destination-item shadow">
                        <div className="destination-content m-2 pb-1">
                          <div className="row mb-2">
                            <div className="col-lg-6 col-md-12">
                              {display.status_order === 1 ? (
                                <span className="btn btn-lg btn-secondary ">
                                  <strong>Waiting</strong>
                                </span>
                              ) : display.status_order === 2 ? (
                                <span className="btn btn-lg btn-warning">
                                  Preparing
                                </span>
                              ) : display.status_order === 3 ? (
                                <span className="btn btn-lg btn-success">
                                  Ready
                                </span>
                              ) : (
                                <span className="btn btn-lg btn-primary">
                                  Finish
                                </span>
                              )}

                              <h4 className="mt-2">{display.nomor_order}</h4>
                              <div className="destination-bottom">
                                <p
                                  className="text-muted"
                                  style={{ fontSize: "12px" }}
                                >
                                  {display.waktu_order}
                                </p>
                              </div>
                              <div className="destination-bottom mt-3 justify-content-center">
                                <div className="row mb-3">
                                  <div className="col-lg-4 col-md-4 col-sm-12">
                                    <form
                                      onSubmit={(e) =>
                                        submitChange(e, display.id_order)
                                      }
                                    >
                                      <input
                                        hidden
                                        type="number"
                                        name="id_order"
                                        value={display.id_order}
                                        onChange={(e) => changeHandler(e)}
                                      ></input>
                                      {/* <input
                                        value="1"
                                        type="number"
                                        name="status_order"
                                        onChange={(e) => changeHandler(e)}
                                        hidden
                                      ></input> */}
                                      <button
                                        type="submit"
                                        className="btn btn-warning m-1"
                                        style={{ width: 100 }}
                                        value="Preparing"
                                      >
                                        Preparing
                                      </button>
                                    </form>
                                  </div>
                                  <div className="col-lg-4 col-md-4 col-sm-12">
                                    {/* Tambah argument idOrder. Jadi tiap button itu, ada data id order yg berkaitan */}
                                    <form
                                      onSubmit={(e) =>
                                        submitReady(e, display.id_order)
                                      }
                                    >
                                      <input
                                        hidden
                                        type="number"
                                        name="id_order"
                                        value={display.id_order}
                                        onChange={(e) => changeHandler(e)}
                                      ></input>
                                      {/* <input
                                        value="1"
                                        type="number"
                                        name="status_order"
                                        onChange={(e) => changeHandler(e)}
                                        hidden
                                      ></input> */}
                                      <button
                                        type="submit"
                                        className="btn btn-success m-1"
                                        style={{ width: 100 }}
                                        // href="/kds-system/login"
                                        value="Ready"
                                      >
                                        Ready
                                      </button>
                                    </form>
                                  </div>
                                  <div className="col-lg-4 col-md-4 col-sm-12">
                                    <form
                                      onSubmit={(e) =>
                                        submitFinish(e, display.id_order)
                                      }
                                    >
                                      <input
                                        hidden
                                        type="number"
                                        name="id_order"
                                        value={display.id_order}
                                        onChange={(e) => changeHandler(e)}
                                      ></input>
                                      {/* <input
                                        type="number"
                                        name="status_order"
                                        onChange={(e) => changeHandler(e)}
                                        hidden
                                      ></input> */}
                                      <button
                                        type="submit"
                                        className="btn btn-primary m-1"
                                        style={{ width: 100 }}
                                        // href="/kds-system/login"
                                        value="Finish"
                                      >
                                        Finish
                                      </button>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-5 col-md-11">
                              {display.order_detail.map((display, index) => (
                                <div className="row">
                                  <div className="col-lg-2">
                                    <div className="img mt-2 text-center">
                                      <img
                                        width="auto"
                                        height="40px"
                                        src={`${POS_MEDIA_URL}/foto_produk/${display.foto_produk}`}
                                        alt=""
                                      />
                                    </div>
                                  </div>
                                  <div className="col-lg-5 mt-3">
                                    <h4>{display.nama_produk}</h4>
                                  </div>
                                  <div className="col-lg-5 mt-3">
                                    <h5 style={{ fontSize: "18px" }}>
                                      {display.jumlah} items
                                    </h5>
                                  </div>
                                </div>
                              ))}
                            </div>
                            <div className="col-lg-1 col-md-1 mb-3">
                              {display.status_order === 1 ? (
                                <div
                                  className="btn-lg bg-secondary "
                                  style={{ width: "100%", height: "100%" }}
                                ></div>
                              ) : display.status_order === 2 ? (
                                <div
                                  className="btn-lg bg-warning "
                                  style={{ width: "100%", height: "100%" }}
                                ></div>
                              ) : display.status_order === 3 ? (
                                <div
                                  className="btn-lg bg-success "
                                  style={{ width: "100%", height: "100%" }}
                                ></div>
                              ) : (
                                <div
                                  className="btn-lg bg-primary"
                                  style={{ width: "100%", height: "100%" }}
                                ></div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default MenuDisplay;
