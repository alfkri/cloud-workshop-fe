import React, { Fragment } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import Navbar from "../../components/Navbar";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo2.png";
import Sparepart from "../../components/Sparepart/Sparepart";

const SparepartPage = () => {
  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle pageTitle={"Beli Sparepart"} pagesub={" Produk / Sparepart"} />
      <Sparepart />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

export default SparepartPage;
