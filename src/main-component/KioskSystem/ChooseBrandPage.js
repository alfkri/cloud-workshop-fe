import { Box, Button, Grid, Paper, Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { client } from "../../lib/client";
import BrandItemClickable from "../../components/Brand/BrandItemClickable";

const ChooseBrandPage = () => {
  const url = useLocation();
  const path = url?.pathname?.split("/").slice(2);
  const title =
    path?.toString().charAt(0).toUpperCase() + path?.toString().slice(1);
  const [allBrand, setAllBrand] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  async function getAllBrand() {
    const response = await client.get("/pos/get_brand");

    if (response.status === 200) {
      setAllBrand(response.data?.data);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getAllBrand();
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Box p={2}>
      <Box pb={3}>
        <Typography component="h1" variant="h4" className="titlePage">
          POS {title} Management
        </Typography>
        <Typography component="h6" variant="h6">
          Pilih Brand
        </Typography>
      </Box>
      <Grid container spacing={2}>
        {allBrand.map(brand => (
          <Grid item xs={12} sm={6} md={4} lg={3} key={brand.id_brand}>
            <BrandItemClickable item={brand} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default ChooseBrandPage;
