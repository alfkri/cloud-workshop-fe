import React, { useState } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import SimpleReactValidator from "simple-react-validator";
import { toast } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { Link, useNavigate } from "react-router-dom";

import { login } from "../../../store/actions/action";

// import "./style.scss";
import { client } from "../../../lib/client";
import { useEffect } from "react";

const LoginPage = ({ login, auth }) => {
  const push = useNavigate();
  const [isProcessing, setIsProcessing] = useState(false);

  const [value, setValue] = useState({
    email: "",
    password: "",
    remember: false,
  });

  const changeHandler = e => {
    setValue({ ...value, [e.target.name]: e.target.value });
    validator.showMessages();
  };

  const rememberHandler = () => {
    setValue({ ...value, remember: !value.remember });
  };

  const [validator] = React.useState(
    new SimpleReactValidator({
      className: "errorMessage",
    })
  );

  async function submitForm(e) {
    e.preventDefault();
    setIsProcessing(true);
    if (validator.allValid()) {
      const response = await client.post("/login_admin_lokasi", {
        email: value.email,
        password: value.password,
      });

      if (response.status === 200) {
        toast.success("Login berhasil");
        const user = {
          email: value.email,
          token: response.data.access_token,
        };
        login(user);
        setValue({
          email: "",
          password: "",
          remember: false,
        });
        validator.hideMessages();
        push("/kiosk-pos-system/kasir");
      } else {
        toast.error(response.error);
      }

      // const userRegex = /^user+.*/gm;
      // const email = value.email;

      // if (email.match(userRegex)) {
      //   toast.success("You successfully Login on Parador !");
      //   push("/home");
      // }
    } else {
      validator.showMessages();
      toast.error("Empty field is not allowed!");
    }
    setIsProcessing(false);
  }

  useEffect(() => {
    if (auth.isLoggedIn) {
      push("/kiosk-pos-system/kasir");
    }
  }, []);

  return (
    <Grid className="loginWrapper">
      <Grid className="loginForm">
        <h2>Sign In Admin Lokasi</h2>
        <p>
          Sign in to your <strong>admin lokasi</strong> account
        </p>
        <form onSubmit={submitForm}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                className="inputOutline"
                fullWidth
                placeholder="E-mail"
                value={value.email}
                variant="outlined"
                name="email"
                label="E-mail"
                InputLabelProps={{
                  shrink: true,
                }}
                onBlur={e => changeHandler(e)}
                onChange={e => changeHandler(e)}
              />
              {validator.message("email", value.email, "required|email")}
            </Grid>
            <Grid item xs={12}>
              <TextField
                className="inputOutline"
                fullWidth
                placeholder="Password"
                value={value.password}
                variant="outlined"
                name="password"
                type="password"
                label="Password"
                InputLabelProps={{
                  shrink: true,
                }}
                onBlur={e => changeHandler(e)}
                onChange={e => changeHandler(e)}
              />
              {validator.message("password", value.password, "required")}
            </Grid>
            {!isProcessing && (
              <Grid item xs={12}>
                <Grid className="formFooter">
                  <Button fullWidth className="cBtnTheme" type="submit">
                    Login
                  </Button>
                </Grid>
              </Grid>
            )}
            {isProcessing && (
              <Grid item xs={12}>
                <Grid>
                  <Button fullWidth variant="outlined" disabled>
                    Loading...
                  </Button>
                </Grid>
              </Grid>
            )}
          </Grid>
        </form>
        <div className="shape-img">
          <i className="fi flaticon-honeycomb"></i>
        </div>
      </Grid>
    </Grid>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps, { login })(LoginPage);
