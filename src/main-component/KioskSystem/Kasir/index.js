import {
  Box,
  Breadcrumbs,
  Divider,
  Grid,
  InputAdornment,
  Modal,
  OutlinedInput,
  TextField,
} from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import AddBoxIcon from "@material-ui/icons/AddBox";
import IndeterminateCheckBoxIcon from "@material-ui/icons/IndeterminateCheckBox";
import SearchIcon from "@material-ui/icons/Search";

import React from "react";
import posProducts from "../../../api/posProducts";
import ProductItem from "../../../components/Kasir/ProductItem";
import { connect } from "react-redux";
import OrderItem from "../../../components/Kasir/OrderItem";
import { client } from "../../../lib/client";
import { toast } from "react-toastify";
import { submitOrder, loadProducts } from "../../../store/actions/action";
import { useEffect } from "react";
import { useState } from "react";
import ScreenKeyboard from "../../../components/Kasir/ScreenKeyboard";
import useComponentVisible from "../../../hooks/useComponentVisible";
import { Link, useLocation, useParams } from "react-router-dom";

// Rekomendasi
import "./menuRec.css";
import MenuItem from "../../../components/MenuItem/MenuItem";
import Slider from "react-slick";
import PrevArrow from "../../../components/Carousel/prevArrow";
import NextArrow from "../../../components/Carousel/nextArrow";

import clsx from "clsx";
import OrderDetailModal from "../../../components/Kiosk/modal/OrderDetailModal";

const useStyles = makeStyles((theme) => ({
  listProduct: {
    // backgroundColor: "red",
    minHeight: "100vh",
  },
  listOrder: {
    backgroundColor: "#2c3248",
    color: "#FFFFFF",
    minHeight: "100vh",
  },
  listMenu: {
    paddingTop: "2vh",
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  boldText: {
    fontWeight: 700,
  },
  input: {
    width: "50px",
  },
  divider: {
    height: "2px !important",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  dividerBlack: {
    backgroundColor: "black",
  },
  listProductBox: {
    display: "flex",

    "& .titlePage": {
      [theme.breakpoints.down("md")]: {
        marginBottom: "20px",
      },
    },

    [theme.breakpoints.down("md")]: {
      display: "block",
    },
  },
  totalOrderBox: {
    display: "flex",

    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
  totalOrderPrice: {
    marginTop: "0.5rem",
  },
}));

const settings = {
  infinite: true,
  autoplay: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  swiping: true,
  dynamic: true,
  nextArrow: <NextArrow />,
  prevArrow: <PrevArrow />,
};

function shuffleArray(array) {
  let i = array.length - 1;
  for (; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

const KasirPage = ({ order, totalPrice, submitOrder, loadProducts }) => {
  const classes = useStyles();
  const { idBrand } = useParams();
  const url = useLocation();
  const path = url?.pathname?.split("/").slice(2);
  const [isLoading, setIsLoading] = useState(true);

  const [listDataProduct, setListDataProduct] = useState([]);
  const [listDataMenu, setListDataMenu] = useState([]);

  const [namaBrand, setNamaBrand] = useState("");
  const [searchInput, setSearchInput] = useState("");
  const { ref, buttonTriggerRef, isComponentVisible } =
    useComponentVisible(false);

  // POST Order
  const [isOpenOrderModal, setIsOpenOrderModal] = useState(false);
  const [dataOrder, setDataOrder] = useState([]);
  const [totalHargaOrder, setTotalHargaOrder] = useState(0);

  const inputChangeHandler = (event) => {
    const input = event.target.value;
    setSearchInput(input);
    searchProducts(input);
    // keyboard.current.setInput(input);
  };

  const keyboardChangeHandler = (input) => {
    setSearchInput(input);
    searchProducts(input);
  };

  const handleCloseOrderModal = () => {
    setIsOpenOrderModal(false);
  };

  async function submitProductOrder() {
    const response = await client.post("/pos/insert_order", {
      tipe_pembelian: 1,
      list_produk: order,
    });
    if (response.status === 200) {
      getDetailOrder(response.data.data.id_order);
      setTotalHargaOrder(response.data.data.total_harga);
      toast.success("Order berhasil");
      submitOrder();
    } else {
      toast.error(response.error);
    }
  }

  async function getDetailOrder(idOrder) {
    const response = await client.getParams("/pos/get_order_detail", {
      id_order: idOrder,
    });

    if (response.status === 200) {
      setDataOrder(response.data.data);
      setIsOpenOrderModal(true);
    } else {
      toast.error(response.error);
    }
    // getDataBrand();
  }

  async function getDataProduct() {
    const response = await client.getParams("/pos/get_produk", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setListDataProduct(response.data.data);
    } else {
      toast.error(response.error);
    }
    getDataBrand();
  }

  async function getDataMenu() {
    const response = await client.getParams("/pos/get_produk", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setListDataMenu(response.data.data);
    } else {
      toast.error(response.error);
    }
    getDataBrand();
  }

  async function getDataBrand() {
    const response = await client.getParams("/pos/get_brand", {
      id_brand: idBrand,
    });

    if (response.status === 200) {
      setNamaBrand(response.data?.data[0]?.nama_brand);
    } else {
      toast.error(response.error);
    }
    setIsLoading(false);
  }

  async function searchProducts(name) {
    const response = await client.getParams("/pos/get_produk", {
      id_brand: idBrand,
      search: name,
    });

    if (response.status === 200) {
      setListDataProduct(response.data.data);
    } else {
      toast.error(response.error);
    }
    // setIsLoading(false);
  }

  useEffect(() => {
    loadProducts();
    getDataProduct();
    getDataMenu();
  }, []);

  if (isLoading) {
    return "Loading...";
  }

  const shuffledMenu = shuffleArray(listDataMenu);

  return (
    <Box>
      <OrderDetailModal
        handleClose={handleCloseOrderModal}
        isOpen={isOpenOrderModal}
        data={dataOrder}
        totalHarga={totalHargaOrder}
      />
      <Grid container id="kasir">
        <Grid item xs={7} className={classes.listProduct}>
          <Box p={2}>
            <Breadcrumbs aria-label="breadcrumb">
              <Typography color="textSecondary" style={{ color: "black" }}>
                kiosk order system
              </Typography>
              {path.map((sub, ind) => (
                <Link
                  key={sub}
                  color={
                    ind === path.length - 1 ? "textPrimary" : "textSecondary"
                  }
                  to={`/kiosk-pos-system/${path.slice(0, ind + 1).join("/")}`}
                >
                  {sub}
                </Link>
              ))}
            </Breadcrumbs>
          </Box>
          <Box
            p={2}
            className={classes.listProductBox}
            justifyContent="space-between"
            alignItems="center"
          >
            <Typography component="h1" variant="h4" className="titlePage">
              {namaBrand} Order System
            </Typography>
            <OutlinedInput
              id="searchProduct"
              ref={buttonTriggerRef}
              placeholder="Cari produk"
              // value={values.amount}
              value={searchInput}
              fullWidth
              onChange={inputChangeHandler}
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              }
            />
          </Box>
          <Divider className={classes.dividerBlack} />
          <Box p={2}>
            {/* LIST PRODUK */}
            <Grid container spacing={2}>
              {listDataProduct.slice(0, 6).map((item) => (
                <Grid item xs={12} sm={6} md={4} key={item.id_produk}>
                  <ProductItem item={item} />
                </Grid>
              ))}
            </Grid>

            {/* LIST REKOMENDASI */}
            <Box p={2}>
              <Grid className="delivery-collections">
                <div className="max-width">
                  <Typography
                    component="h2"
                    variant="h5"
                    className={classes.boldText}
                  >
                    Rekomendasi Menu
                  </Typography>
                  <Slider {...settings} className={classes.listMenu}>
                    {shuffledMenu.slice(0, 5).map((item) => (
                      <Grid item xs={12} sm={12} md={6} key={item.id_produk}>
                        <MenuItem item={item} />
                      </Grid>
                    ))}
                  </Slider>
                </div>
              </Grid>
            </Box>
            {/* END LIST REKOMENDASI */}

            {/* LIST PRODUK */}
            <Grid container spacing={2}>
              {listDataProduct.slice(6, 20).map((item) => (
                <Grid item xs={12} sm={6} md={4} key={item.id_produk}>
                  <ProductItem item={item} />
                </Grid>
              ))}
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={5} className={classes.listOrder}>
          <Box p={2} className="orderSection">
            <Box pb={2}>
              <Typography variant="h6" component="h2">
                Order List
              </Typography>
            </Box>
            <Divider className={classes.divider} />
            {order.map((order, index) => (
              <Box key={order.id_produk}>
                <OrderItem
                  item={order}
                  key={order.id_produk + order.nama_produk}
                />
                {index !== order.length - 1 && <Divider />}
              </Box>
            ))}
            <Divider className={classes.divider} />
            <Box
              className={classes.totalOrderBox}
              justifyContent="space-between"
              py={2}
            >
              <Typography
                component="p"
                variant="h6"
                className={classes.boldText}
              >
                Total
              </Typography>
              <Typography
                component="p"
                variant="h6"
                className={clsx(classes.boldText, classes.totalOrderPrice)}
              >
                Rp {totalPrice?.toLocaleString()}
              </Typography>
            </Box>
            <Button
              variant="contained"
              color={order.length === 0 ? "default" : "primary"}
              className={classes.boldText}
              fullWidth
              onClick={submitProductOrder}
              disabled={order.length === 0}
            >
              Bayar
            </Button>
          </Box>
        </Grid>
      </Grid>
      <Box
        sx={{
          position: "fixed",
          // height: "20vh",
          right: 100,
          left: 100,
          bottom: 0,
          zIndex: 1201,
          // backgroundColor: "red",
        }}
      >
        <Box>
          {isComponentVisible && (
            <Box ref={ref}>
              <ScreenKeyboard
                onInputChange={keyboardChangeHandler}
                input={searchInput}
              />
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  );
};

const mapStateToProps = (state) => {
  return {
    order: state.orderList.order,
    totalPrice: state.orderList.totalPrice,
  };
};
export default connect(mapStateToProps, { submitOrder, loadProducts })(
  KasirPage
);
