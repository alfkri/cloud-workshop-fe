import { Box } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import Services from "../../api/service";

const WhyChoose = props => {
  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  return (
    <div className="why-choose-section">
      <h2>
        Mengapa Harus Bergabung Bersama <span>Bisa Kitchen</span>?
      </h2>
      <div className="feature-grids clearfix">
        <Box display="flex" flexWrap="wrap">
          {Services.map((service, sitem) => (
            <div className="grid" key={sitem}>
              <div className="icon">
                <i className={`fi ${service.fIcon}`}></i>
              </div>
              <div className="hover-icon">
                <i className={`fi ${service.fIcon}`}></i>
              </div>
              <h3>
                <Link
                  onClick={ClickHandler}
                  to={`/service-single/${service.id}`}
                >
                  {service.title}
                </Link>
              </h3>
              <p>{service.description}</p>
            </div>
          ))}
        </Box>
      </div>
    </div>
  );
};

export default WhyChoose;
