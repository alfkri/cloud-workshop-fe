import React, { Fragment } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import Navbar from "../../components/Navbar";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo2.png";
import Bengkel from "../../components/Bengkel/Bengkel";

const BengkelPage = () => {
  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle pageTitle={"Sewa Bengkel"} pagesub={" Produk / Bengkel"} />
      <Bengkel />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

export default BengkelPage;
