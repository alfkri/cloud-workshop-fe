import React, { Fragment } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import Navbar from "../../components/Navbar";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo2.png";
import Service from "../../components/ServiceKendaraan/Service";

const ServicePage = () => {
  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle
        pageTitle={"Service Kendaraan"}
        pagesub={" Produk / Service"}
      />
      <Service />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

export default ServicePage;
