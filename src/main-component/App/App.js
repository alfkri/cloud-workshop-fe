import React from "react";
import AllRoute from "../router";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../sass/style.scss";
import { useLocation } from "react-router-dom";
import LayoutPosSystem from "../PosSystem/LayoutPosSystem";

const App = () => {
  return (
    <div className="App" id="scrool">
      <div className="App">
        <AllRoute />
        <ToastContainer />
      </div>
    </div>
  );
};

export default App;
