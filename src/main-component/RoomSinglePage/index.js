import React, { Fragment } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import { useParams } from "react-router-dom";
import Navbar from "../../components/Navbar";
import Scrollbar from "../../components/scrollbar";
import { connect } from "react-redux";
import Footer from "../../components/footer";
import Logo from "../../images/logo2.png";
import KitchenDetails from "./KitchenDetails";

const RoomSinglePage = props => {
  const { type } = useParams();
  const titlePage = type.split("-").join(" ").split("_").join(", ");
  //   const [product, setProduct] = useState({});

  //   useEffect(() => {
  //     setProduct(Allproduct.filter(Allproduct => Allproduct.type === type));
  //   }, []);

  //   const item = product[0];

  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle pageTitle={titlePage} pagesub={"Sewa Kitchen"} />
      <div className="room-details-section">
        {/* {item ? ( */}
        <div className="room-details-inner">
          <KitchenDetails />
        </div>
      </div>
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {
    products: state.data.products,
  };
};

export default connect(mapStateToProps)(RoomSinglePage);
