import React from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import simg1 from "../../images/room/img-7.jpg";
import simg2 from "../../images/room/img-8.jpg";

import CookingSet from "../../images/facilities/kitchenSet.jpg";
import Others from "../../images/facilities/others.jpg";
import PricingSection from "../../components/PricingSection";

const KitchenDetails = props => {
  const { type, id } = useParams();
  const available = id === "1" ? true : false;
  const titlePage = type.split("-").join(" ").split("_").join(", ");
  const SubmitHandler = e => {
    e.preventDefault();
  };

  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  return (
    <div className="Room-details-area section-padding">
      <div className="container">
        <div className="row">
          <div className="col-12 text-align-left">
            <div className="room-description">
              <div className="kitchen-title">
                <h2>Description</h2>
              </div>
              <p className="p-wrap">
                Bisa Kitchen merupakan Cloud Kitchen atau Dapur Bersama untuk
                membantu ekspansi bisnis kuliner secara mudah dengan
                mengoperasikan dapur yang fokus ke layanan food delivery. Bisa
                Kitchen menyediakan fasilitas dan layanan sangat lengkap yang
                siap membantu mengembangkan Bisnis Kuliner Anda menjadi lebih
                mudah dan cepat.
              </p>
              <p>
                Bersama Bisa Kitchen, kami akan membantu Anda untuk mendapatkan
                dapur bersama sesuai kebutuhan dan siap pakai, dengan beragam
                layanan yang bisa membuat bisnis kuliner Anda berkembang lebih
                pesat. Salah satu lokasi kitchen kami adalah di {titlePage}.
              </p>
            </div>
            <div className="room-details-service">
              <div className="row">
                <div className="room-details-item">
                  <div className="row">
                    <div className="col-md-5 col-sm-5 mb-5">
                      <div className="room-d-text">
                        <div className="kitchen-title">
                          <h2>Fasilitas Dapur</h2>
                        </div>
                        <ul>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Kompor 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Meja Dapur 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Wajan 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Frying Pan 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Panci 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Spatula 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Saringan 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Penjepit 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Gunting 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Talenan 1 pcs
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="#">
                              Chiller & Freezer Sharing
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-md-7 col-sm-7">
                      <div className="room-d-img">
                        <img height="600px" src={CookingSet} alt="" />
                      </div>
                    </div>
                    <div className="col-md-7 col-sm-7">
                      <div className="room-d-img">
                        <img src={Others} alt="" />
                      </div>
                    </div>
                    <div className="col-md-5 col-sm-5">
                      <div className="room-d-text2">
                        <div className="kitchen-title">
                          <h2>Kelebihan Lainnya</h2>
                        </div>
                        <ul>
                          <li>
                            <Link onClick={ClickHandler} to="/room-single/1">
                              Pendaftaran Brand Anda di Seluruh Food Delivery
                              Channel (GoFood, GrabFood, Shopee Food)
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="/room-single/1">
                              1 (satu) Stand Dapur Bisa Digunakan Untuk Beberapa
                              Brand yang Anda miliki
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="/room-single/1">
                              Area Dapur yang Selalu Dijaga Kebersihannya
                            </Link>
                          </li>
                          <li>
                            <Link onClick={ClickHandler} to="/room-single/1">
                              Runner Bersama yang Akan Naik-Turun Mengambil
                              Pesanan
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="pricing-area">
              <div className="kitchen-title">
                <h2>Pricing Plans</h2>
              </div>
              <PricingSection available={available} />
              {/* <div className="pricing-table">
                <table className="table-responsive pricing-wrap">
                  <thead>
                    <tr>
                      <th>Mon</th>
                      <th>Tue</th>
                      <th>Wed</th>
                      <th>Thu</th>
                      <th>Fri</th>
                      <th>Sat</th>
                      <th>Sun</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>$250</td>
                      <td>$250</td>
                      <td>$250</td>
                      <td>$250</td>
                      <td>$250</td>
                      <td>$250</td>
                      <td>$250</td>
                    </tr>
                  </tbody>
                </table>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default KitchenDetails;
