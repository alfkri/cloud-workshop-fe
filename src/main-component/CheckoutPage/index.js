import React, { Fragment, useEffect, useState } from "react";
import PageTitle from "../../components/pagetitle/PageTitle";
import Navbar from "../../components/Navbar";
import { useNavigate, useParams } from "react-router-dom";
import CheckoutSection from "../../components/CheckoutSection";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import { connect } from "react-redux";
import Logo from "../../images/logo2.png";
import { client } from "../../lib/client";
import { logout } from "../../store/actions/action";
import { toast } from "react-toastify";

const CheckoutPage = ({ auth, logout }) => {
  const { idHarga } = useParams();
  const push = useNavigate();
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  async function getItemData() {
    const response = await client.getParams("/dapur/get_harga_dapur", {
      id_dpr_harga: idHarga,
    });

    if (response.status === 200) {
      setData(response.data.data[0]);
    } else if (response.status === 401) {
      logout();
      toast.error("Silahkan login kembali");
      push("/login");
    } else {
      toast.error("Paket tidak tersedia");
      push("/home");
    }
    setIsLoading(false);
  }
  useEffect(() => {
    if (auth.isLoggedIn) {
      getItemData();
    } else {
      push("/login");
    }
  }, []);

  if (isLoading) return "Loading...";

  return (
    <Fragment>
      <Navbar hclass={"wpo-header-style-2"} Logo={Logo} />
      <PageTitle pageTitle={"Checkout"} pagesub={"Checkout"} />
      <CheckoutSection data={data} />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};
const mapStateToProps = state => {
  return {
    auth: state.auth,
    symbol: state.data.symbol,
  };
};

export default connect(mapStateToProps, { logout })(CheckoutPage);
